﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnColorPickManager : MonoBehaviour
{
    public static Color currentColor;
    public List<Button> buttons;

    // Start is called before the first frame update
    void Start()
    {
        currentColor = buttons[0].transform.GetChild(0).GetComponent<Image>().color;
        foreach (Button btn in buttons)
        {
            btn.onClick.AddListener(()=> {
                currentColor = btn.transform.GetChild(0).GetComponent<Image>().color;
            });
        }
    }
}

