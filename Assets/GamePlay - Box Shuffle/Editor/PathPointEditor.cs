﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ParabolicSwitch))]
public class PathPointEditor : Editor
{
    ParabolicSwitch parabolicSwitch;

    public override void OnInspectorGUI()
    {
        parabolicSwitch = (ParabolicSwitch)target;

        if (GUILayout.Button("ReConstruct Points"))
        {
            parabolicSwitch.ReConstructPoints();
        }
        if (GUILayout.Button("Clear Points"))
        {
            parabolicSwitch.ClearPoints();
        }

        base.OnInspectorGUI();
    }
}
