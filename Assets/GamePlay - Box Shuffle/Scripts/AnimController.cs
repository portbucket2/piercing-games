﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimController : MonoBehaviour
{
    public SwitchSequenceManager sequenceManager;

    public void CloseBoxes()
    {
        sequenceManager.CloseBoxes();
    }

    public void SetJewelleryContainerParent()
    {
        sequenceManager.SetJewelleryParentAndShuffle();
    }
}
