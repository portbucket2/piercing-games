﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxShuffleLevelManager : MonoBehaviour
{
    public GameObject[] levels;
    public int levelIndex;

    public int maxStarPerLevel = 3;

    private GameObject currentLevel;
    private int levelStarCount;

    public TouchDetect touchDetect;

    public UIManager uiManager;

    // Start is called before the first frame update
    void Start()
    {
        BaseLoader.GetIterationIndex(ref levelIndex);
        StartLevel(levelIndex);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void StartLevel(int level)
    {
        uiManager.gameStateMessage.text = "";
        touchDetect.enabled = false;
        currentLevel = Instantiate(levels[level]);
        currentLevel.GetComponent<SwitchSequenceManager>().Initialize(this);
        levelStarCount = maxStarPerLevel;
    }

    public void LevelSucceeded()
    {
        uiManager.gameStateMessage.text = uiManager.findSuccessfulMsg;
        touchDetect.enabled = false;
        Debug.Log("Level Completed__With Star Count =   " + levelStarCount);

        FRIA.Centralizer.Add_DelayedMonoAct(this, () =>
        {
            BaseLoader.RequestCompletion(true);
            BaseLoader.ReportStarCount(levelStarCount);
        }, 1.5f);
    }

    public void LevelFailed()
    {
        uiManager.gameStateMessage.text = uiManager.findFailedMsg;
        touchDetect.enabled = false;
        if (levelStarCount > 0)
            levelStarCount--;
        Debug.Log("Level Failed__With Current Star Count =   " + levelStarCount);
        //NoNeedToCall As of now : BaseLoader.RequestCompletion(false);
        Invoke("RestartLevel", 2f);
    }

    void RestartLevel()
    {
        uiManager.gameStateMessage.text = "";
        Destroy(currentLevel);
        touchDetect.enabled = false;
        currentLevel = Instantiate(levels[levelIndex]);
        currentLevel.GetComponent<SwitchSequenceManager>().Initialize(this);
    }
}
