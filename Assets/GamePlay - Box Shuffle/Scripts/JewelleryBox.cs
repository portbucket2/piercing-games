﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JewelleryBox : MonoBehaviour
{
    public bool ContainsJewellery { get; set; } = false;

    private void Start()
    {
        ContainsJewellery = false;
    }
}
