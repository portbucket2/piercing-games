﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.Rendering;

public class ParabolicSwitch : MonoBehaviour
{
    public float coefficient4aFactor = 16;
    public float curveResolution = 30;

    public GameObject testObject;
    List<GameObject> testObjects = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SwitchBoxes(Transform box1, Transform box2, float switchingSpeedFactor, Action OnComplete, bool changeSwitchDirection = false)
    {
        StartCoroutine(Move(box1, box2, CalculatePathPoints(box1.position, box2.position, changeSwitchDirection), CalculatePathPoints(box2.position, box1.position, changeSwitchDirection), switchingSpeedFactor, OnComplete));
    }

    IEnumerator Move(Transform box1, Transform box2, List<Vector3> box1PathPoints, List<Vector3> box2PathPoints, 
        float switchSpeedFactor, Action OnCompletionOfSwitch)
    {
        int currentBox1Point = 0;
        int currenBox2Point = 0;

        float startingDistance = Vector3.Distance(box1.position, box2.position);

        while (currentBox1Point < box1PathPoints.Count - 1)
        {
            box1.position = Vector3.MoveTowards(box1.position, box1PathPoints[currentBox1Point + 1], switchSpeedFactor * startingDistance * Time.deltaTime);
            if (Vector3.Distance(box1.position, box1PathPoints[currentBox1Point + 1]) < switchSpeedFactor * startingDistance * Time.deltaTime)
            {
                box1.position = box1PathPoints[currentBox1Point + 1];
                currentBox1Point++;
            }

            box2.position = Vector3.MoveTowards(box2.position, box2PathPoints[currenBox2Point + 1], switchSpeedFactor * startingDistance * Time.deltaTime);
            if (Vector3.Distance(box2.position, box2PathPoints[currenBox2Point + 1]) < switchSpeedFactor * startingDistance * Time.deltaTime)
            {
                box2.position = box2PathPoints[currenBox2Point + 1];
                currenBox2Point++;
            }

            yield return null;
        }

        OnCompletionOfSwitch?.Invoke();
    }

    List<Vector3> CalculatePathPoints(Vector3 startPoint, Vector3 endPoint, bool changePathDireciton = false, bool showPath = false, bool showDebug = false)
    {
        List<Vector3> pathPoints = new List<Vector3>();

        Vector3 connectingVector = endPoint - startPoint;
        Vector3 midPointOfConnectingLine = startPoint + connectingVector.normalized * (connectingVector.magnitude / 2);

        Vector3 xAxisOfParabola = connectingVector.normalized;

        float xAxisParabolaAngle = 0f;
        if (connectingVector.x >= 0f && connectingVector.z >= 0f)   //1st quadrant
            xAxisParabolaAngle = Mathf.Acos(xAxisOfParabola.x);
        else if (connectingVector.x < 0f && connectingVector.z >= 0f)   //2ns quadrant
            xAxisParabolaAngle = Mathf.Acos(xAxisOfParabola.x);
        else if (connectingVector.x < 0f && connectingVector.z < 0f)    //3rd quadrant
            xAxisParabolaAngle = Mathf.Acos(xAxisOfParabola.x) + 90f * Mathf.Deg2Rad;
        else    //4th quadrant
            xAxisParabolaAngle = 360f * Mathf.Deg2Rad - Mathf.Acos(xAxisOfParabola.x);

        float yAxisParabolaAngle = xAxisParabolaAngle + 90f * Mathf.Deg2Rad;
        Vector3 yAxisOfParabola = new Vector3(Mathf.Cos(yAxisParabolaAngle), 0f, Mathf.Sin(yAxisParabolaAngle));

        if (showDebug)
        {
            Debug.Log(xAxisOfParabola.ToString("F3"));
            Debug.Log(xAxisParabolaAngle * Mathf.Rad2Deg);
            Debug.Log(yAxisParabolaAngle * Mathf.Rad2Deg);
            Debug.Log(yAxisOfParabola.ToString("F5"));
            Debug.DrawLine(startPoint, endPoint, Color.red, 100f);
            Debug.DrawLine(midPointOfConnectingLine, yAxisOfParabola * 20f, Color.green, 100f); 
        }

        float modCoefficient4a = changePathDireciton ? -coefficient4aFactor * connectingVector.magnitude :
            coefficient4aFactor * connectingVector.magnitude;
        float segmentLength = connectingVector.magnitude / (curveResolution - 1);
        float x_Left = -(connectingVector.magnitude / 2);

        float vertexZOffset = 0 - ((x_Left * x_Left) / modCoefficient4a);

        for (int i = 0; i < curveResolution; i++)
        {
            float x = x_Left + segmentLength * i;
            float z = ((x * x) / modCoefficient4a) + vertexZOffset;

            Vector3 actualPathPoint = midPointOfConnectingLine + xAxisOfParabola * x + yAxisOfParabola * z;

            pathPoints.Add(actualPathPoint);
            if (showPath)
            {
                testObjects.Add(Instantiate(testObject, actualPathPoint, Quaternion.identity));
            }
        }

        return pathPoints;
    }

    public void ReConstructPoints()
    {
        SwitchSequenceManager ssm = GetComponent<SwitchSequenceManager>();
        CalculatePathPoints(ssm.levelData.boxes[0].position, ssm.levelData.boxes[1].position, showPath: true);
        CalculatePathPoints(ssm.levelData.boxes[1].position, ssm.levelData.boxes[0].position, showPath: true);
        CalculatePathPoints(ssm.levelData.boxes[1].position, ssm.levelData.boxes[2].position, showPath: true);
        CalculatePathPoints(ssm.levelData.boxes[2].position, ssm.levelData.boxes[1].position, showPath: true);
        CalculatePathPoints(ssm.levelData.boxes[0].position, ssm.levelData.boxes[2].position, showPath: true);
        CalculatePathPoints(ssm.levelData.boxes[2].position, ssm.levelData.boxes[0].position, showPath: true);
    }

    public void ClearPoints()
    {
        foreach (GameObject obj in testObjects)
        {
            DestroyImmediate(obj);
        }
    }
}
