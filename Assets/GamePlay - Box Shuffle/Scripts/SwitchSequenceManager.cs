﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchSequenceManager : MonoBehaviour
{
    [System.Serializable]
    public struct LevelData
    {
        public Transform[] boxes;
        public SwitchSequence[] switchSequences;
        [Space(10.0f)]
        public float switchSpeedFactor;
        public float switchDelay;
        [Space(10.0f)]
        public Transform jewellery;
        public Transform jewelleryBox;
    }

    [System.Serializable]
    public struct SwitchSequence
    {
        public int box1;
        public int box2;
        public bool changeDirection;
        public bool switchComplete { get; set; }
    }

    public LevelData levelData;

    [Space(20.0f)]
    public ParabolicSwitch switchControl;

    //public float switchStartDelay = 2f;

    private string jewelleryAnimationTrigger = "MIDDLE";

    private BoxShuffleLevelManager levelManager;

    public void Initialize(BoxShuffleLevelManager manager)
    {
        levelManager = manager;
    }

    // Start is called before the first frame update
    void Start()
    {
        levelData.jewellery.GetComponent<Animator>().SetTrigger(jewelleryAnimationTrigger);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Shuffle()
    {
        //yield return new WaitForSeconds(switchStartDelay);

        levelManager.uiManager.startShufflePrompt.SetActive(true);
        yield return new WaitUntil(() => Input.GetMouseButtonDown(0));
        levelManager.uiManager.startShufflePrompt.SetActive(false);

        int switchesPerformed = 0;

        while (switchesPerformed < levelData.switchSequences.Length)
        {
            Transform box1 = levelData.boxes[levelData.switchSequences[switchesPerformed].box1];
            Transform box2 = levelData.boxes[levelData.switchSequences[switchesPerformed].box2];
            float switchSpeed = levelData.switchSpeedFactor;
            levelData.switchSequences[switchesPerformed].switchComplete = false;

            switchControl.SwitchBoxes(box1, box2, switchSpeed,
                () => levelData.switchSequences[switchesPerformed].switchComplete = true, levelData.switchSequences[switchesPerformed].changeDirection);

            yield return new WaitUntil(() => levelData.switchSequences[switchesPerformed].switchComplete);

            yield return new WaitForSeconds(levelData.switchDelay);
            switchesPerformed++;
        }

        levelManager.touchDetect.enabled = true;

        levelManager.uiManager.gameStateMessage.text = levelManager.uiManager.findJewelleryMsg;
    }

    public void CloseBoxes()
    {
        foreach (var box in levelData.boxes)
        {
            box.GetComponent<Animator>().SetTrigger("CLOSE");
        }
    }

    public void SetJewelleryParentAndShuffle()
    {
        levelData.jewelleryBox.GetComponent<JewelleryBox>().ContainsJewellery = true;
        StartCoroutine(Shuffle());
    }
}
