﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchDetect : MonoBehaviour
{
    public BoxShuffleLevelManager levelManager;
    public string jewelleryCheckName = "Jewellery";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray mouseRay = GenerateMouseRay();
            RaycastHit hit;

            if (Physics.Raycast(mouseRay.origin, mouseRay.direction, out hit))
            {
                hit.transform.GetComponent<Animator>().SetTrigger("OPEN");

                if (hit.transform.GetComponent<JewelleryBox>().ContainsJewellery)
                {
                    levelManager.LevelSucceeded();
                    return;
                }
                else
                {
                    levelManager.LevelFailed();
                    return;
                }
            }
        }
    }

    Ray GenerateMouseRay()
    {
        Vector3 mousePosFar = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.farClipPlane);

        Vector3 mousePosNear = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane);

        Vector3 mouseWorldPosFar = Camera.main.ScreenToWorldPoint(mousePosFar);

        Vector3 mouseWorldPosNear = Camera.main.ScreenToWorldPoint(mousePosNear);

        Ray mouseRay = new Ray(mouseWorldPosNear, mouseWorldPosFar - mouseWorldPosNear);
        return mouseRay;
    }
}
