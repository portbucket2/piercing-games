﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GPMan_GemPolish : MonoBehaviour
{

    public List<GemPolisher> itemList;

    public int iteration = 0;
    // Start is called before the first frame update
    public Image progressBar;
    void Awake()
    {
        BaseLoader.GetIterationIndex(ref iteration);

        GameObject go = Instantiate(itemList[iteration].gameObject);
        GemPolisher gp = go.GetComponent<GemPolisher>();
        gp.progressBar = progressBar;
        progressBar.fillAmount = 0;
    }

}
