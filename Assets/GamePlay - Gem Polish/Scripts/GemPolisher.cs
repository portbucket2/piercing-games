﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class GemPolisher : MonoBehaviour
{

    public static GemPolisher instance;

    public MeshFilter filter;
    public Mesh mesh;

    public float rad =  0.5f;
    public float solidRatio =  0.5f;

    public float expectedProgress;
    public float currentProgress;

    public Image progressBar;
    private void Start()
    {
        instance = this;

        CameraAndToolVisibilityController.onRayHittingInteractableLayer += OnRayRecieve;
        mesh = Instantiate(filter.mesh);
        filter.mesh = mesh;
        vertices = mesh.vertices;
        N = vertices.Length;
        vcolors = new Color[N];
        for (int i = 0; i < N; i++) vcolors[i] = new Color(1,1,1,1);
        mesh.colors = vcolors;

        expectedProgress = N;
        currentProgress = 0;
    }
    int N;
    Vector3[] vertices;
    Color[] vcolors;

    private void OnDestroy()
    {
        CameraAndToolVisibilityController.onRayHittingInteractableLayer -= OnRayRecieve;

    }

    bool completionCalled = false;
    public void OnRayRecieve(RaycastHit rch, VertexOpMode opmode, Ray ray)
    {
        VertexOperation_Sphere_Thin( filter.transform.InverseTransformPoint(rch.point));
        float p = currentProgress /( expectedProgress * 0.95f);
        if (progressBar) progressBar.fillAmount = p;
        if (p>=1 && !completionCalled)
        {
            completionCalled = true;
            Centralizer.Add_DelayedMonoAct(this, ()=> {
                BaseLoader.RequestCompletion(true);
                
            },1.2f);
        }
    }


    public void VertexOperation_Sphere_Thin(Vector3 rchPoint)
    { 
        float d;
        float tempProg;
        Vector3 tempVec;
        float currentR2 = rad * rad;
        float currentR1 = currentR2*solidRatio*solidRatio ;
        int c1=0, c2=0;
        for (int v = 0; v < vertices.Length; v++)
        {
            tempVec.x = vertices[v].x - rchPoint.x;
            tempVec.y = vertices[v].y - rchPoint.y;
            tempVec.z = vertices[v].z - rchPoint.z;
            d = tempVec.x * tempVec.x + tempVec.y * tempVec.y + tempVec.z * tempVec.z;
            if (d < currentR2)
            {
                float vprog = vcolors[v].a;
                if (vprog > 0)
                {
                    if (d > currentR1)
                    {
                        tempProg = (currentR2 - d) / (currentR2 - currentR1);
                        if (tempProg < vprog)
                        {
                            currentProgress += vprog - tempProg;
                            vprog = tempProg;
                            //Debug.Log("C");
                            c1++;
                        }
                    }
                    else if (vprog > 0)
                    {
                        c2++;
                        currentProgress += vprog;
                        vprog = 0;
                    }
                }
                vcolors[v].a = vprog;
            }

        }
        mesh.colors = vcolors;
    }


#if UNITY_EDITOR
    public void Build()
    {
        Debug.Log("Building");
        MeshCollider mc = filter.GetComponent<MeshCollider>();
        if (!mc)
        {
            mc = filter.gameObject.AddComponent<MeshCollider>();
        }
        mc.gameObject.SetLayer(CameraAndToolVisibilityController.interactableLayer);
    }
#endif
}

#if UNITY_EDITOR
[CustomEditor(typeof(GemPolisher))]
public class GemPlisherEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GemPolisher gp = target as GemPolisher;
        if (GUILayout.Button("Build"))
        {
            gp.Build();
            EditorFix.SetObjectDirty(gp.gameObject);
        }
    }

}
#endif