﻿using GemToss;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GemToss
{
    public class Basket : MonoBehaviour
    {
        public LevelTossData tossData;

        public TossController.GemType gemType;

        // Start is called before the first frame update
        void Start()
        {

        }


        private void OnTriggerEnter(Collider other)
        {
            TossController tc = other.GetComponent<TossController>();
            if (tc != null && !tc.collidedForResult)
            {
                if (this.gemType == tc.gemType)
                {
                    Debug.Log("Successful Throw");
                    tossData.SuccessfulThrow();
                    tc.collidedForResult = true;
                }
                else
                {
                    Debug.Log("Failed Throw");
                    tossData.DeductLife(true);
                    tc.collidedForResult = true;
                }
            }
        }
    } 
}
