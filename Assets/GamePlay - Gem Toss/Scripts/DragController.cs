﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GemToss
{
    public class DragController : MonoBehaviour
    {
        public LayerMask inputLayer;
        TossController tossController;
        Vector2 dragStartPos;

        public float minSwipeLengthToToss;
        public float swipeDetectThreshold;
        public float swipeTimeLimit;
        float timer;

        Vector2 swipe;
        bool tossed = false;
        bool dropped = false;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray mouseRay = GenerateMouseRay();
                RaycastHit hit;

                if (Physics.Raycast(mouseRay.origin, mouseRay.direction, out hit))
                {
                    if (((1 << hit.transform.gameObject.layer) & inputLayer) == 0)
                        return;
                    tossController = hit.transform.GetComponent<TossController>();
                    dragStartPos = Input.mousePosition;
                    timer = 0f;
                    tossed = false;
                    dropped = false;
                    swipe = Vector2.zero;
                }
            }
            else if (Input.GetMouseButton(0) && tossController)
            {
                timer += Time.deltaTime;
                swipe = (Vector2)Input.mousePosition - dragStartPos;
                swipe = new Vector2(swipe.x / Screen.width, swipe.y / Screen.height);
                Vector2 swipeDirection = swipe.normalized;

                if (swipe.magnitude > minSwipeLengthToToss && timer <= swipeTimeLimit && !tossed && tossController.onTable)
                {
                    tossController.Toss(ConvertDirectionToXZPlane(swipeDirection));
                    tossed = true;
                }
                
                if (timer > swipeTimeLimit && !dropped && !tossed && swipe.magnitude > swipeDetectThreshold)
                {
                    tossController.LiftUp();
                    dropped = true;
                }
            }
            else if (Input.GetMouseButtonUp(0) && tossController && !dropped && !tossed && swipe.magnitude > swipeDetectThreshold)
            {
                tossController.LiftUp();
                dropped = true;
                tossController = null;
            }
        }

        Vector3 ConvertDirectionToXZPlane(Vector2 screenVector)
        {
            float screenXAngle = Mathf.Acos(screenVector.x);
            Vector3 directionInXZPlane = new Vector3(Mathf.Cos(screenXAngle), 0f, Mathf.Sin(screenXAngle));
            return directionInXZPlane;
        }

        Ray GenerateMouseRay()
        {
            Vector3 mousePosFar = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.farClipPlane);

            Vector3 mousePosNear = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane);

            Vector3 mouseWorldPosFar = Camera.main.ScreenToWorldPoint(mousePosFar);

            Vector3 mouseWorldPosNear = Camera.main.ScreenToWorldPoint(mousePosNear);

            Ray mouseRay = new Ray(mouseWorldPosNear, mouseWorldPosFar - mouseWorldPosNear);
            return mouseRay;
        }
    } 
}
