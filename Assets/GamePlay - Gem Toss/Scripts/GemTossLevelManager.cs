﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GemToss
{
    public class GemTossLevelManager : MonoBehaviour
    {
        public GameObject[] levels;
        public int levelIndex;

        public int maxStarPerLevel = 3;

        private GameObject currentLevel;
        private int levelStarCount;

        public GemTossUiManager uiManager;

        private const string firstTimePlayCheck = "FirstTimePlayed";

        // Start is called before the first frame update
        void Start()
        {
            BaseLoader.GetIterationIndex(ref levelIndex);
            StartLevel(levelIndex);
            if (FirstTimeGamePlayed())
            {
                uiManager.ShowSwipeTutorial(); 
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        void StartLevel(int level)
        {
            uiManager.gameStateMessage.text = "";
            currentLevel = Instantiate(levels[level]);
            currentLevel.GetComponent<LevelTossData>().InitializeLevel(this);
            levelStarCount = maxStarPerLevel;
        }

        public void LevelSucceeded()
        {
            Invoke("LevelCompleteMessage", 2.2f);
            Debug.Log("Level Completed__With Star Count =   " + levelStarCount);

            FRIA.Centralizer.Add_DelayedMonoAct(this, () =>
            {
                BaseLoader.RequestCompletion(true);
                BaseLoader.ReportStarCount(levelStarCount);
            }, 4f);
        }

        public void LevelFailed()
        {
            Invoke("LevelFailedMessage", 2.2f);
            Debug.Log("Level Failed__With Current Star Count =   " + levelStarCount);
            //NoNeedToCall As of now : BaseLoader.RequestCompletion(false);
            Invoke("RestartLevel", 4f);
        }

        void RestartLevel()
        {
            uiManager.gameStateMessage.text = "";
            Destroy(currentLevel);
            currentLevel = Instantiate(levels[levelIndex]);
            currentLevel.GetComponent<LevelTossData>().InitializeLevel(this);
            levelStarCount = maxStarPerLevel;
        }

        public void DeductStar()
        {
            levelStarCount--;
        }

        public void LevelCompleteMessage()
        {
            uiManager.ShowMessage(uiManager.levelWon);
        }

        public void LevelFailedMessage()
        {
            uiManager.ShowMessage(uiManager.levelFailed);
        }

        public bool FirstTimeGamePlayed()
        {
            if (PlayerPrefs.GetInt(firstTimePlayCheck, 0) != 0)
                return false;
            return true;
        }

        public void SwipeDoneSuccessfully()
        {
            PlayerPrefs.SetInt(firstTimePlayCheck, 1);
        }    
    } 
}
