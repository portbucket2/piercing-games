﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GemToss
{
    public class GemTossUiManager : MonoBehaviour
    {
        public Text gameStateMessage;
        public TextMeshProUGUI livesCount;
        public TextMeshProUGUI gemInBasketCount;

        public string wrongBasket;
        public string rightBasket;
        public string thrownOutside;

        public string levelWon;
        public string levelFailed;

        public GameObject swipeTutorial;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void UpdateCountUI(int livesRemaining, int gemsInBasket, int totalGemsInLevel)
        {
            livesCount.text = "x" + livesRemaining.ToString();
            gemInBasketCount.text = gemsInBasket.ToString() + "/" + totalGemsInLevel.ToString();
        }

        public void ShowMessage(string message)
        {
            gameStateMessage.text = message;
            StartCoroutine(DisappearMessage());
        }

        public void ShowSwipeTutorial()
        {
            swipeTutorial.SetActive(true);
        }

        public void HideSwipeTutorial()
        {
            swipeTutorial.SetActive(false);
        }

        IEnumerator DisappearMessage()
        {
            yield return new WaitForSeconds(2f);
            gameStateMessage.text = "";
        }
    } 
}
