﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GemToss
{
    public class LevelTossData : MonoBehaviour
    {
        public Transform cleanGemBasket;
        public Transform dirtyGemBasket;
        public Transform throwPosition;

        public GameObject[] gems;
        private int gemsThrown;

        public int startingLives;
        private int remainingLives;

        public bool testingThrow;


        private GemTossLevelManager levelManager;
        private int successfulThrows = 0;

        public void InitializeLevel(GemTossLevelManager levelManager)
        {
            this.levelManager = levelManager;
            remainingLives = startingLives;
        }

        // Start is called before the first frame update
        void Start()
        {
            levelManager.uiManager.UpdateCountUI(startingLives, 0, gems.Length);
            gemsThrown = 0;
            successfulThrows = 0;
            Instantiate(gems[0], throwPosition.position, Quaternion.identity, transform).GetComponent<TossController>().InitializeGem(this);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SuccessfulThrow()
        {
            successfulThrows++;
            levelManager.uiManager.UpdateCountUI(remainingLives, successfulThrows, gems.Length);

            if (gemsThrown < gems.Length)
            {
                BringNewGem(); 
            }
            
            levelManager.uiManager.ShowMessage(levelManager.uiManager.rightBasket);
            if (gemsThrown >= gems.Length)
            {
                levelManager.LevelSucceeded();
            }
        }

        public void DeductLife(bool wrongBasket = true)
        {
            if (wrongBasket)
            {
                levelManager.uiManager.ShowMessage(levelManager.uiManager.wrongBasket); 
            }
            else
            {
                levelManager.uiManager.ShowMessage(levelManager.uiManager.thrownOutside);
            }

            remainingLives--;
            levelManager.DeductStar();
            levelManager.uiManager.UpdateCountUI(remainingLives, successfulThrows, gems.Length);

            if (gemsThrown < gems.Length && remainingLives > 0)
            {
                BringNewGem();
            }

            if (remainingLives <= 0)
            {
                levelManager.LevelFailed();
            }
            else if (gemsThrown >= gems.Length)
            {
                levelManager.LevelSucceeded();
            }
        }

        public void GemThrown()
        {
            gemsThrown++;
        }

        public void BringNewGem()
        {
            Instantiate(gems[gemsThrown], throwPosition.position, Quaternion.identity, transform).GetComponent<TossController>().InitializeGem(this);
        }

        public void ShowTutorial()
        {
            if (levelManager.FirstTimeGamePlayed())
                levelManager.uiManager.ShowSwipeTutorial();
        }

        public void HideTutorial()
        {
            levelManager.uiManager.HideSwipeTutorial();
            levelManager.SwipeDoneSuccessfully();
        }    
    } 
}
