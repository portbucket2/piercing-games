﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace GemToss
{
    public class TossController : MonoBehaviour
    {
        LevelTossData tossData;

        public enum GemType
        {
            Clean,
            Dirty
        }
        public GemType gemType;

        public float liftUpDistance = 1f;
        bool droppped = false;

        public float projectileAngle = 60f;
        public Rigidbody gemRigidbody;
        public float customGravityAcc = 50f;

        Vector3 throwPosition;
        bool tossed = false;
        public bool collidedForResult { get; set; } = false;
        public bool onTable { get; set; } = true;

        bool useCustomGravity = false;

        public void InitializeGem(LevelTossData tossData)
        {
            this.tossData = tossData;
        }

        // Start is called before the first frame update
        void Start()
        {
            gemRigidbody.useGravity = false;
            tossed = false;
            collidedForResult = false;
            throwPosition = transform.position;
        }

        // Update is called once per frame
        void Update()
        {
            if (transform.position.y <= 1.5f && tossData.testingThrow)
            {
                transform.position = throwPosition;
                useCustomGravity = false;
                gemRigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
            }
        }

        void FixedUpdate()
        {
            if (useCustomGravity)
            {
                gemRigidbody.AddForce(customGravityAcc * Vector3.down, ForceMode.Acceleration);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (tossed && collision.gameObject.GetComponent<Basket>() == null && !collidedForResult)
            {
                collidedForResult = true;
                Debug.Log("Failed Throw");
                tossData.DeductLife(false);
            }
            else if (collision.gameObject.name == "Table")
            {
                onTable = true;
            }
        }

        public void LiftUp()
        {
            StopAllCoroutines();
            onTable = false;
            droppped = false;
            useCustomGravity = false;
            gemRigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
            StartCoroutine(LiftUpLerp());
        }

        IEnumerator LiftUpLerp()
        {
            float startTime = Time.realtimeSinceStartup;
            float liftUpTime = 1f;

            Vector3 startPos = transform.position;
            Vector3 liftUpPosition = transform.position + Vector3.up * liftUpDistance;

            while (transform.position != liftUpPosition && !droppped)
            {
                transform.position = Vector3.Lerp(startPos, liftUpPosition,
                    Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / liftUpTime));

                yield return null;
            }

            Drop();
        }

        public void Toss(Vector3 directionInXZPlane)
        {
            Vector3 bucketsMidPoint = new Vector3(0f, 0f, tossData.cleanGemBasket.position.z);
            Vector3 gemPoint = new Vector3(transform.position.x, 0f, transform.position.z);
            float distance = Vector3.Distance(gemPoint, bucketsMidPoint);
            float xAngle = Mathf.Acos(directionInXZPlane.x) > 90f ? 180f - Mathf.Acos(directionInXZPlane.x) : Mathf.Acos(directionInXZPlane.x);
            float rangeOfProjectile = distance / Mathf.Sin(xAngle);

            float velocityOfProjectile = Mathf.Sqrt((rangeOfProjectile * customGravityAcc) /
                (Mathf.Sin(Mathf.Deg2Rad * 2 * projectileAngle)));


            float directionOfProjectileX = Mathf.Cos(Mathf.Deg2Rad * projectileAngle) * Mathf.Cos(xAngle);
            float directionOfProjectileY = Mathf.Sin(Mathf.Deg2Rad * projectileAngle);
            float directionOfProjectileZ = Mathf.Cos(Mathf.Deg2Rad * projectileAngle) * Mathf.Sin(xAngle);
            Vector3 directionOfProjectile = new Vector3(directionOfProjectileX,
                directionOfProjectileY, directionOfProjectileZ);

            useCustomGravity = true;
            gemRigidbody.constraints = RigidbodyConstraints.None;
            gemRigidbody.velocity = velocityOfProjectile * directionOfProjectile;

            tossed = true;
            onTable = false;
            tossData.GemThrown();

            tossData.HideTutorial();
        }

        void Drop()
        {
            droppped = true;
            useCustomGravity = true;
        }

        Vector3 FindIntersectionPointOfTwoVectors(Vector3 vector1stPoint, Vector3 vector1stDirection,
            Vector3 vector2ndPoint, Vector3 vector2ndDirection)
        {
            float s = (vector2ndPoint.x * vector1stDirection.y + vector1stDirection.x * vector2ndPoint.x -
                        vector1stPoint.x * vector1stDirection.y + vector1stDirection.x * vector2ndPoint.y) /
                        (vector1stDirection.x * vector2ndDirection.y - vector2ndDirection.x * vector1stDirection.y);

            Vector3 intersectionPoint = vector2ndPoint + s * vector2ndDirection;

            return intersectionPoint;
        }
    } 
}
