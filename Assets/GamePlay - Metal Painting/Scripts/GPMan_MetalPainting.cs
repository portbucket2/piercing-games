﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GPMan_MetalPainting : MonoBehaviour
{

    public List<MetalPainter> itemList;

    public int iteration = 0;
    // Start is called before the first frame update
    public Image progressBar;
    void Awake()
    {
        BaseLoader.GetIterationIndex(ref iteration);

        GameObject go = Instantiate(itemList[iteration].gameObject);
        MetalPainter mp = go.GetComponent<MetalPainter>();
        mp.progressBar = progressBar;
        progressBar.fillAmount = 0;
    }

}
