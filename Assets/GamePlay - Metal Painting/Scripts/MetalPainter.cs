﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class MetalPainter : MonoBehaviour
{

    public static MetalPainter instance;
    public Image progressBar;
    public float progMult = 1.20f;
    public List<PaintMesh> paintMeshes;
    public float rad;
    public float vRad = 0.04f;
    public float speedMult;

    [System.Serializable]
    public class PaintMesh
    {
        public int N;
        public MeshRenderer renderer;
        public MeshFilter filter;
        public Collider col;


        public Material mat;
        public Texture2D tex;
        public Mesh m;
        public int vCount;
        public float progress;
        Vector3[] vertices;
        float[] progVerts;

        Color[] pixBuf;
        public void Build()
        {
            filter = renderer.GetComponent<MeshFilter>();
            col = renderer.GetComponent<Collider>();
            if (col == null)
            {
                Debug.LogWarning("Assigning new collider");
                col = renderer.gameObject.AddComponent<MeshCollider>();
            }
            col.gameObject.SetLayer(CameraAndToolVisibilityController.interactableLayer);           

        }

        public void Init()
        {
            mat = renderer.material;
            m = filter.mesh;
            vCount = filter.mesh.vertexCount;
            vertices = filter.mesh.vertices;
            progVerts = new float[vCount];
            //m = renderer.GetComponent<MeshFilter>().mesh;
            progress = 0;
            tex = new Texture2D(N, N);


            pixBuf = new Color[N * N];
            for (int i = 0; i < pixBuf.Length; i++)
            {
                pixBuf[i] = new Color(0, 0, 0, 0);
            }
            tex.SetPixels(pixBuf);
            tex.Apply();
            mat.SetTexture("_MainTex",tex);
        }

        float d;
        float f;
        float dt;
        Color cOld;
        public void OnRay(RaycastHit rch, Color c , float r,float speedMult, float vRad)
        {
            dt = Time.deltaTime;
            Vector2 uv = rch.textureCoord * (N - 1);
            for (int u = 0; u < N; u++)
            {
                for (int v = 0; v < N; v++)
                {
                    float distSqr = (u - uv.x) * (u - uv.x) + (v - uv.y) * (v - uv.y);
                    if (distSqr < r * r)
                    {
                        d = Mathf.Sqrt(distSqr);
                        f = speedMult*dt*((r-d) / r);
                        cOld = pixBuf[v * N + u];
                        if (cOld.a != 0)
                        {
                            cOld.r = Mathf.Lerp(cOld.r, c.r, f);
                            cOld.g = Mathf.Lerp(cOld.g, c.g, f);
                            cOld.b = Mathf.Lerp(cOld.b, c.b, f);
                            cOld.a = Mathf.Clamp01(cOld.a+f);
                            //Debug.Log(cOld.a);
                        }
                        else
                        {
                            cOld = c;
                            cOld.a = f;
                        }

                        pixBuf[v * N + u] = cOld;
                    }
                }
            }
            tex.SetPixels(pixBuf);
            tex.Apply();

            Vector3 hitpoint = filter.transform.InverseTransformPoint(rch.point);
            progress = 0;
            for (int i = 0; i < vCount; i++)
            {
                d = Vector3.SqrMagnitude(hitpoint - vertices[i]);
                if (d < vRad*vRad)
                {
                    //d = Mathf.Sqrt(d);
                    progVerts[i] = Mathf.Clamp01(progVerts[i] + 1);
                }
                progress += progVerts[i];
            }
        }
    }

    float totalN;
    private void Start()
    {
        instance = this;

        CameraAndToolVisibilityController.onRayHittingInteractableLayer += OnRayRecieve;
        totalN = 0;
        foreach (var item in paintMeshes)
        {
            item.Init();
            totalN += item.vCount*item.N;
        }
    }
    private void OnDestroy()
    {
        CameraAndToolVisibilityController.onRayHittingInteractableLayer -= OnRayRecieve;
    }

    public void OnRayRecieve(RaycastHit rch, VertexOpMode opmode, Ray ray)
    {
        foreach (var item in paintMeshes)
        {
            if (item.col == rch.collider)
            {
                item.OnRay(rch,BtnColorPickManager.currentColor,rad,speedMult,vRad);
            }
        }


        float prog = 0;
        foreach (var item in paintMeshes)
        {
            prog += item.progress*item.N;
        }
        prog *= (progMult / totalN);
        Debug.Log(totalN);
        progressBar.fillAmount = prog;
        if (!completionCalled && prog >= 1)
        {
            Centralizer.Add_DelayedMonoAct(this,()=> 
            {
                BaseLoader.ReportStarCount(3);
                BaseLoader.RequestCompletion(true);
            },1.5f);
            completionCalled = true;
        }
    }
    bool completionCalled = false;


#if UNITY_EDITOR
    public void Build()
    {
        foreach (PaintMesh item in paintMeshes)
        {
            item.Build();
        }
    }
#endif
}



#if UNITY_EDITOR
[CustomEditor(typeof(MetalPainter))]
public class MetalPainterEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        MetalPainter mp = target as MetalPainter;
        if (GUILayout.Button("Build"))
        {
            mp.Build();
            EditorFix.SetObjectDirty(mp.gameObject);
        }
    }

}
#endif