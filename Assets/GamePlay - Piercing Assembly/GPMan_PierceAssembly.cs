﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPMan_PierceAssembly : MonoBehaviour
{

    public List<PierceingAssembler> itemList;
    public Camera cam;

    public int iteration = 0;
    // Start is called before the first frame update
    void Awake()
    {
        BaseLoader.GetIterationIndex(ref iteration);

        GameObject go = Instantiate(itemList[iteration].gameObject);
        PierceingAssembler pa = go.GetComponent<PierceingAssembler>();
        pa.Init(cam);
    }

}
