﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class PierceingAssembler : MonoBehaviour
{
    public Transform referenceRoot;
    public Transform editableRoot;
    // Start is called before the first frame update
    public Dictionary<Transform, PiercingAssemblablePiece> assemblyList = new Dictionary<Transform, PiercingAssemblablePiece>();

    DragReporter dragR;
    public Camera cam;
    public LayerMask editLayerMask;
    public LayerMask editPlaneLayerMask;
    public float snapThreshold = 0.1f;

    public int assembledCount;
    public int toAssembleCount;

    public Transform rotationAxis;
    public float rotateSpeed;

    bool initialized = false;
    private void Start()
    {
        if (!initialized)
        {
            Init(Camera.main);
        }
    }
    public void Init(Camera cam)
    {
        initialized = true;
        this.cam = cam;
        assembledCount = 0;
        toAssembleCount = referenceRoot.childCount;
        for (int i = 0; i < toAssembleCount; i++)
        {
            PiercingAssemblablePiece pap = new PiercingAssemblablePiece();
            pap.editable = editableRoot.GetChild(i);
            pap.editable.gameObject.AddComponent<BoxCollider>();
            pap.target = referenceRoot.GetChild(i);
            assemblyList.Add(pap.editable,pap);
        }
        dragR = GetComponent<DragReporter>();
        dragR.OnDragStart += OnDragStart;
        referenceRoot.gameObject.SetLayer("RendTex0");
        editableRoot.gameObject.SetLayer("Editable");

    }
    void OnDragStart(Drag drag)
    {
        Ray r= cam.ScreenPointToRay(Input.mousePosition) ;
        RaycastHit rch;
        if (Physics.Raycast(r,out rch,100,editLayerMask))
        {
            RaycastHit r1;
            Physics.Raycast(r, out r1, 100, editPlaneLayerMask);
            dragData = new DragData();
            dragData.item = rch.transform;
            dragData.drag = drag;
            dragData.offset = rch.transform.position - r1.point;
            drag.onDrag += OnDrag;
        }
    }
    private void OnDrag(Vector2 travel)
    {
        Transform tr = dragData.item;
        Vector3 offset = dragData.offset;
        Ray r2 = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit rc2;
        if (Physics.Raycast(r2, out rc2, 100, editPlaneLayerMask))
        {
            tr.position = rc2.point + offset;
            float diff = (tr.position - assemblyList[tr].target.position).magnitude;
            if (diff < snapThreshold)
            {
                tr.position = assemblyList[tr].target.position;
                Destroy(tr.GetComponent<BoxCollider>());
                dragData.drag.onDrag -= OnDrag;
                assembledCount++;
                if (assembledCount == toAssembleCount)
                {
                    referenceRoot.gameObject.SetActive(false);
                    editableRoot.SetParent(rotationAxis);

                    Centralizer.Add_DelayedMonoAct(this,()=> {
                        BaseLoader.RequestCompletion(true);
                    },1.35f);
                }
            }
        }
    }

    private void Update()
    {
        if (assembledCount == toAssembleCount)
        {
            rotationAxis.Rotate(rotationAxis.forward,rotateSpeed*Time.deltaTime,Space.World);
        }
    }
    DragData dragData;
    
    public class DragData
    {
        public Transform item;
        public Drag drag;
        public Vector3 offset;
    }
}
[System.Serializable]
public class PiercingAssemblablePiece
{
    public Transform editable;
    public Transform target;
}
