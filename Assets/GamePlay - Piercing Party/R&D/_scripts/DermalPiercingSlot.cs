﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DermalPiercingSlot : MonoBehaviour
{
    public bool pierceFromTop;
    public string positionName;
    public PiercingPositions myPosition;
    public CameraOrientation myOrientation;
    public Animator currentPiercingAnimator;
    public GameObject neddle;
    public Transform neddleBottom;
    public ParticleSystem targetPointer;
    public FakeBoneObject fakeBone;
    public RingHolder currentRingHolder;

    private float targetPointOffset = 0.1f;

    private string anim1;
    private string anim2;

    private BonnieFier bonnieFier;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(3.5f);
    WaitForSeconds WAITTWO = new WaitForSeconds(1f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(2f);
    void Start()
    {
        anim1 = "ear1";
        anim2 = "ear2";
        HideNeddle();
    }
    public void PiercingStart(BonnieFier bonn, RingHolder _ring)
    {
        //RingHolder gg = Instantiate(_ring, this.transform);
        //currentRingHolder = gg.GetComponent<RingHolder>();
        //bonnieFier = bonn;
        //StartCoroutine(PiercingRoutine());
    }

    
    void SelectPiercingScenerio()
    {
        //isEarPiercing = dollyCam.isPosOnSide;
        if (myPosition == PiercingPositions.EAR)
        {

            anim1 = "ear1";
            anim2 = "ear2";
        }
        else if (myPosition == PiercingPositions.NOSE)
        {
            anim1 = "nose1";
            anim2 = "nose2";
        }
        else if (myPosition == PiercingPositions.LIPS)
        {
            anim1 = "lip1";
            anim2 = "lip2";
        }
        else if (myPosition == PiercingPositions.BROW)
        {
            anim1 = "brow1";
            anim2 = "brow2";
        }
        else if (myPosition == PiercingPositions.BULL)
        {
            anim1 = "bull1";
            anim2 = "bull2";
        }
        //Debug.Log("Piercing Position: " + myPosition);
    }

    public void PiercedNeddlePosition()
    {
        currentPiercingAnimator.enabled = true;
        currentPiercingAnimator.SetTrigger("in");
    }
    public void AwayNeddlePosition()
    {
        currentPiercingAnimator.enabled = true;
        currentPiercingAnimator.SetTrigger("away");
    }
    public void SetRingObject(RingHolder _ring)
    {
        RingHolder gg = Instantiate(_ring, this.transform);
        currentRingHolder = gg.GetComponent<RingHolder>();
        // set current moveable object
        //currentRingHolder.MakeVisible();
        fakeBone.sceneController.inputController.SetCurrentMoveableObject(currentRingHolder.pin.GetComponent<MoveableObject>());
        currentRingHolder.SetPinPostionToStartPiercing();
    }
    public void SetSavedRingObject(RingHolder _ring)
    {
        if (currentRingHolder == null)
        {
            RingHolder gg = Instantiate(_ring, this.transform);
            currentRingHolder = gg.GetComponent<RingHolder>();
        }
    }
    public void SetJewelMoveable()
    {
        fakeBone.sceneController.inputController.SetCurrentMoveableObject(currentRingHolder.jewel.GetComponent<MoveableObject>(), true);
        currentRingHolder.SetJewelPostionToStartPiercing();
    }
    public void TargetParticleOff()
    {
        targetPointer.Stop();
        targetPointer.gameObject.SetActive(false);
    }
    public void TargetParticleOn()
    {
        targetPointer.gameObject.SetActive(true);
        targetPointer.Play();
    }
    public void HideNeddle()
    {
        Vector3 dir = (fakeBone.sceneController.dollyCamera.transform.position - transform.position).normalized;
        neddle.transform.position = (transform.position - transform.up * 0.05f) + (dir * 0.1f);
        neddle.SetActive(false);
        currentPiercingAnimator.enabled = false;
    }
    public void ShowNeddle()
    {
        neddle.SetActive(true);
        Vector3 dir = (fakeBone.sceneController.dollyCamera.transform.position - transform.position).normalized;
        neddle.transform.position = (transform.position - Vector3.up * 0.2f) + (dir * 0.2f);
        currentPiercingAnimator.enabled = false;
        neddle.GetComponent<MoveableObject>().ScaleAnimationStart();
    }
    public void PiercingFixed()
    {
        neddle.SetActive(false);
        currentPiercingAnimator.enabled = false;
    }
    public void ResetSlot()
    {
        //neddle.SetActive(true);
        if (currentRingHolder)
        {
            Destroy(currentRingHolder.gameObject);
        }
    }
    public Transform GetTargetParticle()
    {
        return targetPointer.transform;
    }
    public void ShowTargetParticle(Vector3 cameraPos)
    {
        Vector3 dir = (cameraPos - transform.position).normalized;
        //Debug.DrawRay(transform.position, dir, Color.green,20f);
        targetPointer.transform.position = transform.position + dir * targetPointOffset;
        TargetParticleOn();
    }
}
