﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeBoneObject : MonoBehaviour
{
    public string positionName;
    public PiercingPositions myPosition;
    public CameraOrientation myOrientation;
    public GameObject indicator;
    public float speed;
    public float movement;
    public DermalPiercingSlot piercingSlot;
    public SceneController sceneController;
    public CustomerModel customerModel;
    public BonnieFier bonnieFier;


    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(1f);

    private Vector3 movementVector;
    Vector3 initialPosition;
    void Start()
    {
        indicator.SetActive(false);
        //Debug.DrawRay(transform.position,transform.up,Color.green, 50f);
        //initialPosition = transform.localPosition;
        sceneController = SceneController.GetController();
    }
    public void SkinPiercingMovement()
    {
        sceneController.PlayPopParticle(this.transform.position);
        //StopAllCoroutines();
        //StartCoroutine(SkinPierceRoutine(_reverse));
        

    }

    IEnumerator SkinPierceRoutine(bool reverse)
    {
        float step = 0;
        initialPosition = transform.localPosition;
        movementVector = transform.up.normalized;
        movementVector *= movement;
        Vector3 targetUp;
        Vector3 targetDown;
        if (reverse)
        {
            targetUp = transform.position - movementVector;
            targetDown = transform.position + movementVector / 2f;
        }
        else
        {
            targetUp = transform.position + movementVector;
            targetDown = transform.position - movementVector / 2f;
        }
        while (step <1)
        {
            transform.position = Vector3.Lerp(transform.position, targetUp, step);
            step += Time.deltaTime * speed;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        step = 0;
        while (step < 1)
        {
            transform.position = Vector3.Lerp(transform.position, targetDown, step);
            step += Time.deltaTime * speed;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        step = 0;
        while (step < 1)
        {
            transform.position = Vector3.Lerp(transform.position, initialPosition, step);
            step += Time.deltaTime * speed;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        transform.localPosition = initialPosition;
    }
    public DermalPiercingSlot GetPiercingSlot()
    {
        return piercingSlot;
    }

    public void ResetToPositionForNeedle()
    {
        StopAllCoroutines();
        StartCoroutine(ResetToPositionNeedleRoutine());
    }
    IEnumerator ResetToPositionNeedleRoutine()
    {
        transform.localPosition = initialPosition;
        piercingSlot.PiercedNeddlePosition();
        yield return WAITONE;
        sceneController.uiController.EnablePiercingSelectionPanel();
        sceneController.SetInputPiercingStage(PiercingStages.RingSelect);
        //sceneController.inputController.DisableInput();

        //piercingSlot.SetRingObject(sceneController.ringHolders[sceneController.GetRingNumber()]);
    }
    public void ResetToPositionForPin()
    {
        transform.localPosition = initialPosition;
        piercingSlot.AwayNeddlePosition();
        piercingSlot.SetJewelMoveable();
        sceneController.SetInputPiercingStage(PiercingStages.FloatingJewel);
        
    }
    public void HideNeedle()
    {
        piercingSlot.HideNeddle();
        piercingSlot.TargetParticleOff();
    }
    public void ShowTargetPoint()
    {
        piercingSlot.ShowTargetParticle(sceneController.dollyCamera.transform.position);
        
    }
    public void ShowNeddle()
    {
        piercingSlot.ShowNeddle();
    }
    public Animator GetModelAnimator()
    {
        return customerModel.GetModelAnimator();
    }
    public BonnieFier GetBonniefier()
    {
        return bonnieFier;
    }
    public PiercingPositions GetPiercingPosition()
    {
        return myPosition;
    }
    public CameraOrientation GetCameraOrientation()
    {
        //Debug.Log("Piercing Orientation: " + myOrientation);
        return myOrientation;
    }
    public void SetFakeBoneObject()
    {
        initialPosition = transform.localPosition; ;
    }
}
