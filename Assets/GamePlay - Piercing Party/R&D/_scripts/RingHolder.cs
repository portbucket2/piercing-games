﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class RingHolder : MonoBehaviour
{
    public int index;
    public Transform jewel;
    public Transform pin;
    public float moveSpeed = 3;
    private Vector3 initialPositionJewel;
    private Vector3 initialPositionPin;
    private Vector3 farPositionJewel;
    private Vector3 farPositionPin;

    private Vector3 initialRotationPin;
    //private Vector3 initialRotationJewel;
    //private Vector3 secondRotationPin;
    //private Vector3 secondRotationJewel;

    private FakeBoneObject boneObject;
    private float jewelDistance = 0f;
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(0.1f);
    SceneController sceneController;
    void Awake()
    {
        initialPositionJewel = jewel.localPosition;
        initialPositionPin = pin.localPosition;
        farPositionJewel = initialPositionJewel + (Vector3.up/100f)*2f ;
        farPositionPin = initialPositionPin + (Vector3.down/100f)*2f ;
        initialRotationPin = pin.localEulerAngles;

    }
    void Start()
    {
        sceneController = SceneController.GetController();
        jewelDistance = Vector3.Distance(jewel.position,pin.position);
        

    }
    void MovePinJewelFar()
    {
        pin.localPosition = farPositionPin;
        jewel.localPosition = farPositionJewel;
    }
    void MakeInvisible()
    {
        //jewel.GetComponent<MeshRenderer>().enabled = false;
        pin.GetComponent<MeshRenderer>().enabled = false;
    }
    public void MakeVisible()
    {
        //jewel.GetComponent<MeshRenderer>().enabled = true;
        pin.GetComponent<MeshRenderer>().enabled = true;
    }
    
    public void MoveJewelToPosition()
    {
        MakeVisible();
        StartCoroutine(MoveRoutine(jewel, jewel.localPosition, initialPositionJewel));
    }

    IEnumerator MoveRoutine(Transform tran, Vector3 start, Vector3 end)
    {
        float step = 0;
        while (step < 1) 
        {
            tran.localPosition = Vector3.Lerp(start, end, step);
            step += Time.deltaTime * moveSpeed;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        tran.localPosition = end;
        //boneObject.SkinPiercingMovement(_revers);
    }

    public void MovePinToInitialPosition()
    {
        //MakeVisible();
        pin.SetParent(transform);
        //pin.localPosition = initialPositionPin;
        StartCoroutine(MoveRoutine(pin, pin.localPosition, initialPositionPin));

        StartCoroutine(RotateRoutine(pin, pin.localEulerAngles, initialRotationPin));
    }
    public void MoveJewelToInitialPosition()
    {
        //jewel.localPosition = initialPositionJewel;
        MoveJewelToPosition();
    }
    public float GetJewelDistance()
    {
        return jewelDistance;
    }
    public void SetPinPostionToStartPiercing()
    {
        MovePinJewelFar();
        MakeInvisible();
        MakeVisible();


        sceneController = SceneController.GetController();
        Vector3 dir =((sceneController.dollyCamera.transform.position) - transform.position);
        //Debug.DrawRay(transform.position,dir,Color.red,20f);
        pin.position = transform.position + (Vector3.down * 0.2f) + (dir * 0.2f);
        //StartCoroutine(MoveRoutine(pin, pin.localPosition, transform.localPosition + (Vector3.down * 0 + dir * 0.002f)));
        StartCoroutine(StartScaleAnimation());
    }
    IEnumerator StartScaleAnimation()
    {
        yield return WAITONE;
        pin.GetComponent<MoveableObject>().ScaleAnimationStart();
    }
    public void SetJewelPostionToStartPiercing()
    {
        sceneController = SceneController.GetController();
        Vector3 dir = ((sceneController.dollyCamera.transform.position) - transform.position);
        //Debug.DrawRay(transform.position, dir, Color.red, 20f);
        jewel.position = transform.position + (Vector3.down * 0.2f) + (dir * 0.2f);
        //StartCoroutine(MoveRoutine(jewel, jewel.localPosition, transform.localPosition + (Vector3.down * 0 + dir * 0.002f)));
    }

    IEnumerator RotateRoutine(Transform tran, Vector3 start, Vector3 end)
    {
        float step = 0;
        while (step < 1)
        {
            tran.localEulerAngles = Vector3.Lerp(start, end, step);
            step += Time.deltaTime * 3f;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        tran.localEulerAngles = end;
        //boneObject.SkinPiercingMovement(_revers);
    }
}
