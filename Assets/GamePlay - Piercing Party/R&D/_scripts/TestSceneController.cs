﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TestSceneController : MonoBehaviour
{
    public Camera mainCamera;
    public ParticleSystem popParticle;
    public float popParticlePositionOffset;
    public Button completeButton;

    public BonnieFier currentBonniefier;
    public Transform directionalLight;
    public CameraDollyController dollyCam;
    public TouchInputController inputController;

    public List<FakeBoneObject> boneObjects;

    public List<RingHolder> ringHolders;
    //later nedded
    //public List<BonnieFier> bonniefiers;
    [Header("Ring Button Panel")]
    public GameObject selectRingPanel;
    public Button ringButton0;
    public Button ringButton1;
    public Button ringButton2;
    public Button ringButton3;
    public Button ringButton4;
    public Button ringButton5;
    public Button ringButton6;
    public Button ringButton7;
    [Header("Position Button Panel")]
    public GameObject selectPositionPanel;
    public Button positionTopLeft;
    public Button positionTopRight;
    public Button positionMiddle;
    public Button positionBottom;
    public Button positionLip;
    public Button positionNoseLeft;
    public Button positionNoseCenter;
    public Button positionEyebrow;

    private int boneNumber = 0;
    private int ringNumber = 0;

    void Start()
    {
        ButtonCollection();
        AllBoneOff();
    }

    public void ResetScene()
    {
        SceneManager.LoadScene("testScene");
    }
    void BonnieFie()
    {
        currentBonniefier.SetBonnifier(boneObjects[boneNumber]);
        currentBonniefier.StartBonnifing(ringHolders[ringNumber]);
        completeButton.gameObject.SetActive(false);
    }

    void ButtonCollection() {
        completeButton.onClick.AddListener(delegate {
            ResetScene();
        });

        ringButton0.onClick.AddListener(delegate {
            ringNumber = 0;
            selectRingPanel.SetActive(false);
        });
        ringButton1.onClick.AddListener(delegate {
            ringNumber = 1;
            selectRingPanel.SetActive(false);
        });
        ringButton2.onClick.AddListener(delegate {
            ringNumber = 2;
            selectRingPanel.SetActive(false);
        });
        ringButton3.onClick.AddListener(delegate {
            ringNumber = 3;
            selectRingPanel.SetActive(false);
        });
        ringButton4.onClick.AddListener(delegate {
            ringNumber = 4;
            selectRingPanel.SetActive(false);
        });
        ringButton5.onClick.AddListener(delegate {
            ringNumber = 5;
            selectRingPanel.SetActive(false);
        });
        ringButton6.onClick.AddListener(delegate {
            ringNumber = 6;
            selectRingPanel.SetActive(false);
        });
        ringButton7.onClick.AddListener(delegate {
            ringNumber = 7;
            selectRingPanel.SetActive(false);
        });


        positionTopRight.onClick.AddListener(delegate {
            boneNumber = 0;
            selectPositionPanel.SetActive(false);
            ZoomToPosition();
        });
        positionTopLeft.onClick.AddListener(delegate {
            boneNumber = 1;
            selectPositionPanel.SetActive(false);
            ZoomToPosition();
        });        
        positionMiddle.onClick.AddListener(delegate {
            boneNumber = 2;
            selectPositionPanel.SetActive(false);
            ZoomToPosition();
        });
        positionBottom.onClick.AddListener(delegate {
            boneNumber = 3;
            selectPositionPanel.SetActive(false);
            ZoomToPosition();
        });
        positionLip.onClick.AddListener(delegate {
            boneNumber = 8;
            selectPositionPanel.SetActive(false);
            ZoomToPosition();
        });
        positionNoseLeft.onClick.AddListener(delegate {
            boneNumber = 5;
            selectPositionPanel.SetActive(false);
            ZoomToPosition();
        });
        positionNoseCenter.onClick.AddListener(delegate {
            boneNumber = 6;
            selectPositionPanel.SetActive(false);
            ZoomToPosition();
        });
        positionEyebrow.onClick.AddListener(delegate {
            boneNumber = 7;
            selectPositionPanel.SetActive(false);
            ZoomToPosition();
        });
    }
    void AllBoneOff()
    {
        for (int i = 0; i < boneObjects.Count; i++)
        {
            boneObjects[i].HideNeedle();
            boneObjects[i].gameObject.SetActive(false);
        }
    }

    public void PlayPopParticle(Vector3 position) {
        Vector3 dir = (mainCamera.transform.position - position).normalized;
        popParticle.transform.position = position + (dir * popParticlePositionOffset);
        popParticle.Play();
    }
    public void SetLightSide() {
        directionalLight.eulerAngles = new Vector3(directionalLight.eulerAngles.x,-73f,directionalLight.eulerAngles.z);
    }
    public void SetLightFront()
    {
        directionalLight.eulerAngles = new Vector3(directionalLight.eulerAngles.x, 0f, directionalLight.eulerAngles.z);
    }
    void ZoomToPosition() {
        AllBoneOff();
        boneObjects[boneNumber].gameObject.SetActive(true);
        boneObjects[boneNumber].ShowTargetPoint();
        currentBonniefier.SetBonnifier(boneObjects[boneNumber]);
        currentBonniefier.GetVertices();
        inputController.SetCurrentPiercingSlot(boneObjects[boneNumber].piercingSlot);
        //dollyCam.ZoomToGamePlay(boneObjects[boneNumber]);
        //if (boneObjects[boneNumber].position == PiercingPositions.EAR)
        //{
        //    SetLightSide();
        //}
        //else
        //{
        //    SetLightFront();
        //}
    }
    public void TestBonniefy()
    {
        //boneNumber = 0;
        //ringNumber = 0;
        BonnieFie();
    }
    public void SetInputPiercingStage(PiercingStages stage)
    {
        inputController.SetPiercingStage(stage);
    }
    public int GetBoneNumber() { return boneNumber; }
    public int GetRingNumber() { return ringNumber; }
    public void ShowLevelComplete()
    {
        completeButton.gameObject.SetActive(true);
    }
}
