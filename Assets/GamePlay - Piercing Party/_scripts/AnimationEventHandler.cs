﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventHandler : MonoBehaviour
{
    public MoveInPingPong pingPong;
    Animator animator;
    //SceneController sceneController;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        //sceneController = SceneController.GetController();
    }

    public void Move()
    {
        pingPong.MovePingPong();
    }
    public void MoveBack()
    {
        pingPong.MoveBack();
    }
    public void Stop()
    {
        pingPong.StopMoving();
    }
    
}
