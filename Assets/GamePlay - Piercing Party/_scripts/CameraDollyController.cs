﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDollyController : MonoBehaviour
{
    //public bool isPosOnSide = true;
    public Camera roomCam;
    public Transform targetObject;
    public Transform targetObjectIdle;

    public Vector3 positionOffsetSideLeft;
    public Vector3 positionOffsetSideRight;

    public Vector3 positionOffsetFrontRight;
    public Vector3 positionOffsetFrontLeft;
    public Vector3 positionOffsetFrontCenter;
    public Vector3 positionOffsetBellyButton;
    public float cameraMoveTime = 1f;
    public CustomerController customer;
    public Transform floatingPlane;

    //[Header("fixed camera positions:")]
    //public Vector3 earCam;
    //public Vector3 noseCam;
    //public Vector3 lipsCam;
    //public Vector3 browCam;
    //public Vector3 bellyCam;
    //public Vector3 bullCam;
    private Vector3 currentOffset;
    private Transform cameraTransform;
    private Vector3 startPosition;
    private WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();

    private SceneController sceneController;
    private CameraOrientation cameraOrientation;
    private PiercingPositions currentPiercingPosition;
    private void Awake()
    {
        cameraTransform = roomCam.GetComponent<Transform>();
        startPosition = transform.position;
    }
    void Start()
    {
        sceneController = SceneController.GetController();
    }

    public void ZoomToGamePlay(bool isManualLevel, int _level, FakeBoneObject boneObject = null)
    {
        StopAllCoroutines();
        if (customer)
        {
            targetObject = customer.GetCurrentFakeBone().transform;
            NextLevelShit(customer.GetCurrentFakeBone().GetPiercingPosition(), customer.GetCurrentFakeBone().GetCameraOrientation());
        }
        else
        {
            targetObject = boneObject.transform;
        }

        //cameraOrientation = customer.GetCurrentPiercingSlot().GetCameraOrientation();

        StartCoroutine(ZoomRoutine(isManualLevel, _level, customer));
    }
    IEnumerator ZoomRoutine(bool isManualLevel, int level, bool isTest = false)
    {
        sceneController.inputController.DisableInput();
        Vector3 pos;

        pos = currentOffset;
        

        if(!isTest)
        {
            //pos = GetCameraZoomPosition(targetObject.GetComponent<FakeBoneObject>().myPosition);
        }
        else
        {
            //ring selection panel on here
            //sceneController.uiController.StartPiercingButtonStatus(true);
            sceneController.StartPiercingGameplay(isManualLevel, level);
            sceneController.ChangeStatusOfHingeJoint(false);
            
            // turn on gameplay here
        }
        floatingPlane.gameObject.SetActive(true);
        floatingPlane.position = targetObject.position;
        floatingPlane.LookAt(transform.position);
        float timeSpan = cameraMoveTime;
        float timeElapsed = 0;
        Vector3 startPos = transform.position;
        while (timeElapsed < timeSpan)
        {
            transform.position = Vector3.Lerp(startPos, pos,Mathf.Pow((timeElapsed/timeSpan),0.28f ));
            timeElapsed += Time.deltaTime ;
            cameraTransform.LookAt(targetObject);
            sceneController.PositionTargetPointer();
            yield return null;
        }
        yield return ENDOFFRAME;
        transform.position = pos;
        cameraTransform.LookAt(targetObject);
        //floatingPlane.gameObject.SetActive(true);
        floatingPlane.position = targetObject.position;
        floatingPlane.LookAt(transform.position);
        sceneController.inputController.EnableInput();
        sceneController.PositionTargetPointer();
        sceneController.PositionNeddle();
    }
    public void ZoomOutOfGamePlay()
    {
        floatingPlane.gameObject.SetActive(false);
        StopAllCoroutines();
        StartCoroutine(ZoomOutRoutine());
    }
    IEnumerator ZoomOutRoutine()
    {
        float step = 0f;
        Vector3 pos = startPosition;
        //while (Vector3.Distance(transform.position, pos) > 0.1f)
        while (step < 1)
        {
            transform.position = Vector3.Lerp(transform.position, pos, step);
            step += Time.deltaTime;
            cameraTransform.LookAt(targetObjectIdle);
            yield return null;
        }
        yield return ENDOFFRAME;
        transform.position = pos;
        cameraTransform.LookAt(targetObjectIdle);
        sceneController.ChangeStatusOfHingeJoint(true);
    }
    public void NextLevelShit(PiercingPositions poses, CameraOrientation ori)
    {
        cameraOrientation = ori;
        currentPiercingPosition = poses;
        //Debug.Log("camera pos : " + poses+"/"+ori);
        Vector3 pos;
        if (poses == PiercingPositions.EAR)
        {
            if (ori == CameraOrientation.LEFT)
            {
                pos = targetObject.position + positionOffsetSideLeft;
            }
            else
            {
                pos = targetObject.position + positionOffsetSideRight;
            }
        }
        else
        {
            if (cameraOrientation == CameraOrientation.LEFT)
            {
                pos = targetObject.position + positionOffsetFrontLeft;
            }
            else if (cameraOrientation == CameraOrientation.RIGHT)
            {
                pos = targetObject.position + positionOffsetFrontRight;
            }
            else if (cameraOrientation == CameraOrientation.MIDDLE)
            {
                pos = targetObject.position + positionOffsetFrontCenter;
            }
            else
            {
                pos = targetObject.position + positionOffsetFrontCenter;
            }
            if (currentPiercingPosition == PiercingPositions.BELLY)
            {
                pos = targetObject.position + positionOffsetBellyButton;
            }
        }

        currentOffset = pos;

    }
    public PiercingPositions GetCurrentPiercingPosition()
    {
        return currentPiercingPosition;
    }
}
