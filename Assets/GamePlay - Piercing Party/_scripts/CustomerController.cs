﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerController : MonoBehaviour
{
    //public ObjectCollectionHuda data;
    public float speed = 1f;
    public Vector3 startPosition;
    public Vector3 endPosition;
    public Animator customerAnimator;
    public Transform roomCamera;
    public SceneController sceneController;
    public CameraDollyController dollyCam;
    public UIController uiController;

    [Header ("Piercing Slots")]
    public int piercingSlotIndex = 0;
    public FakeBoneObject currentFakeBone;
    public BonnieFier currentBonniefier;

    public List<GameObject> customerModels;
    [Header("do not put any value here, set fake bone at sceneController")]
    public List<FakeBoneObject> fakeBones;
    

    private Animator currentPiercingAnimator;
    private MoveInPingPong currentMovingPingPong;
    private string anim1;
    private string anim2;

    private string IN = "in";
    private string OUT = "out";
    private string LAY = "lay";

    int modelsCount = 0;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(1.5f);
    WaitForSeconds WAITTWO = new WaitForSeconds(0.8f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(1f);

    private void Awake()
    {
        InitializePiercingSlots();
    }
    void Start()
    {
        modelsCount = customerModels.Count;
        
        startPosition = transform.localPosition;
        
        //GoAndSit();
        //SetPiercingSlot(0);
    }

    public void GoAndSit(bool isManualLevel, int _level)
    {
        uiController.RingCollectionOnOff(false);
        sceneController.inputController.DisableInput();
        StopAllCoroutines();
        StartCoroutine(GoToSitRoutine(isManualLevel, _level));
        //AnalyticsController.LogEvent_LevelStarted(sceneController.levelCount);
    }
    IEnumerator GoToSitRoutine(bool isManualLevel, int _level)
    {
        currentFakeBone.customerModel.EnableRenderer();
        yield return ENDOFFRAME;
        customerAnimator.SetTrigger(IN);
        float step = 0f;
        while (step <1f)
        {
            transform.localPosition = Vector3.Lerp(startPosition, endPosition, step );
            step += Time.deltaTime * speed;
            yield return ENDOFFRAME;
        }
        float rot = 0f;
        float yVal = 0f;
        if (dollyCam.GetCurrentPiercingPosition() == PiercingPositions.BELLY)
        {
            SetCustomerToLayDown();
        }
        while (rot < 1f)
        {
            yVal = Mathf.Lerp(90, 180, rot);
            transform.eulerAngles = new Vector3(0, yVal, 0);
            rot += Time.deltaTime * speed;
            yield return ENDOFFRAME;
        }
        sceneController.SetSeatClose();
        yield return WAITONE;
        
        dollyCam.ZoomToGamePlay(isManualLevel, _level);
        yield return WAITTWO;
        currentFakeBone.customerModel.DisableRenderer();
        ShowTargetPoint();
        

        // turn on ring ## selection scene ## here
        uiController.RingCollectionOnOff(true);
    }
    public void EnableCustomerSkinnedMesh() { currentFakeBone.customerModel.EnableRenderer(); }

    public void GetUpAndWalk()
    {
        StartCoroutine(GetUpAndWalkRoutine());
    }
    IEnumerator GetUpAndWalkRoutine()
    {
        customerAnimator.SetTrigger(OUT);
        yield return WAITTHREE;
        sceneController.SetSeatFar();
        float rot = 0f;
        float yVal = 0f;
        while (rot < 1f)
        {
            yVal = Mathf.Lerp(180, 270, rot);
            transform.eulerAngles = new Vector3(0, yVal, 0);
            rot += Time.deltaTime * speed;
            yield return ENDOFFRAME;
        }

        float step = 0f;
        while (step < 1f)
        {
            transform.localPosition = Vector3.Lerp(endPosition, startPosition, step);
            step += Time.deltaTime * speed;
            yield return ENDOFFRAME;
        }

        
    }
    public void SetOrnamentToSlotPhaseOne()
    {
        //currentFakeBone.SetOrnamentToSlotPhaseOne();
    }
    public void SetOrnamentToSlotPhaseTwo()
    {
        //currentFakeBone.SetOrnamentToSlotPhaseTwo();
    }

    public void Reset()
    {
        transform.eulerAngles = new Vector3(0f, 90f, 0f);
        transform.localPosition = startPosition;
    }
    public FakeBoneObject GetCurrentFakeBone()
    {
        return currentFakeBone;
    }

    // going next level this
    public void SetPiercingSlot(int index)
    {
        piercingSlotIndex = index;
        if (index < fakeBones.Count)
        {
            StopAllCoroutines();
            StartCoroutine(SetPiercingSlotRoutine(index));
        }
    }
    IEnumerator SetPiercingSlotRoutine(int indd) {
        AllModelsOn();
        currentFakeBone = fakeBones[indd];
        currentFakeBone.GetPiercingSlot().ResetSlot();
        GameObject gg = currentFakeBone.customerModel.gameObject;
        AllModelsOff();
        gg.SetActive(true);
        customerAnimator = currentFakeBone.GetModelAnimator();
        currentBonniefier = currentFakeBone.GetBonniefier();
        sceneController.currentBonniefier = currentBonniefier;
        dollyCam.NextLevelShit(currentFakeBone.GetPiercingPosition(), currentFakeBone.GetCameraOrientation());
        yield return ENDOFFRAME;
    }

    public void ShowTargetPoint()
    {
        currentFakeBone.ShowTargetPoint();
    }
    public CameraDollyController GetCamera()
    {
        return dollyCam;
    }
    void AllModelsOn()
    {
        //Debug.Log("all on!!");
        for (int i = 0; i < modelsCount; i++)
        {
            customerModels[i].SetActive(true);
        }
    }
    void AllModelsOff()
    {
        //Debug.Log("all off!!");
        for (int i = 0; i < modelsCount; i++)
        {
            customerModels[i].SetActive(false);
        }
    }

    public void InitializePiercingSlots()
    {
        fakeBones = new List<FakeBoneObject>();
        fakeBones = sceneController.GetFakeBoneObjects();
        //for (int i = 0; i < max; i++)
        //{
        //    fakeBones.Add(temp[i].piercingRingPrefab.GetComponent<FakeBoneObject>());
        //}

    }
    public void SetCustomerToLayDown()
    {
        customerAnimator.SetTrigger(LAY);
    }
}
