﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerModel : MonoBehaviour
{
    public Animator _animator;
    public Transform _transform;
    public SkinnedMeshRenderer _renderer;
    public HeadRenderers headGameObject;
    public GameObject apronGameObject;
    SceneController sceneController;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _transform = GetComponent<Transform>();
        //_renderer = GetComponent<SkinnedMeshRenderer>();
    }
    private void Start()
    {
        sceneController = SceneController.GetController();
    }

    public Animator GetModelAnimator() { return _animator; }
    public Transform GetModelTransform() { return _transform; }
    public SkinnedMeshRenderer GetRenderer() { return _renderer; }

    public void DisableRenderer() { _renderer.enabled = false; headGameObject.AllVisible(); CheckApronRequirement(); }
    public void EnableRenderer() { _renderer.enabled = true; headGameObject.AllInvisible(); apronGameObject.SetActive(false); }


    void CheckApronRequirement()
    {
        if (sceneController.dollyCamera.GetCurrentPiercingPosition() == PiercingPositions.BELLY)
        {
            apronGameObject.SetActive(false);
        }
        else
        {
            apronGameObject.SetActive(true);
        }
    }
    public void SeatPositionLay()
    {
        sceneController.SetSeatlayDown();
    }
}

public enum PiercingPositions
{
    EAR,
    NOSE,
    LIPS,
    BROW,
    BELLY,
    BULL
}
public enum CameraOrientation
{
    RIGHT,
    LEFT,
    MIDDLE
}


