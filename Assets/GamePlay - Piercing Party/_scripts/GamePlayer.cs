﻿/*
    Collection of classes and structs here
 */


using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GamePlayer 
{
    public string name;
    public int id;
    public int levelsCompleted;
    public int totalStars;
    public int lastPlayedLevel;
    public int[] ringHolders;
    public bool handTutorialShown = false;
}

[System.Serializable]
public struct PiercingRing
{
    public PiercingPositions position;
    public CameraOrientation side;
    public int cost;
    public GameObject pin;
    public GameObject jewel;

    public PiercingRing(PiercingPositions _position, CameraOrientation _orientation, int _cost, GameObject _pin, GameObject _jewel)
    {
        position = _position;
        side = _orientation;
        cost = _cost;
        pin = _pin;
        jewel = _jewel;
    }
}
[System.Serializable]
public struct PiercingPosition
{
    public List<PiercingRing> EarRings;
    public List<PiercingRing> NoseRings;
    public List<PiercingRing> LipRings;
    public List<PiercingRing> EyebrowRings;
    public List<PiercingRing> BellyRings;

    //public PiercingPosition(List<PiercingRing> _EarRings,List<PiercingRing> _NoseRings,List<PiercingRing> _LipRings,List<PiercingRing> _EyebrowRings, List<PiercingRing> _BellyRings )
    //{
    //    EarRings = _EarRings;
    //    NoseRings = _NoseRings;
    //    LipRings = _LipRings;
    //    EyebrowRings = _EyebrowRings;
    //    BellyRings = _BellyRings;
    //}
}
