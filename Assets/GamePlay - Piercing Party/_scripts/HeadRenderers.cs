﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadRenderers : MonoBehaviour
{
    public List<MeshRenderer> renderers;


    int count = 0;
    private void Start()
    {
        count = renderers.Count;
    }

    public void AllVisible()
    {
        count = renderers.Count;
        if (count >0)
        {
            for (int i = 0; i < count; i++)
            {
                renderers[i].enabled = true;
            }
        }
    }
    public void AllInvisible()
    {
        count = renderers.Count;
        if (count >0)
        {
            for (int i = 0; i < count; i++)
            {
                renderers[i].enabled = false;
            }
        }
    }
}
