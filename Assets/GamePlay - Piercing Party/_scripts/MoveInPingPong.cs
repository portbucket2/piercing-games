﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveInPingPong : MonoBehaviour
{
    public bool move = false;
    public PiercingSlot slot;
    float t = 0;
    Vector3 startPositon;
    private Animator animator;
    readonly string DEFAULT = "default";
    readonly string MOVE = "move";
    readonly string BACK = "back";
    SceneController sceneController;
    private void Start()
    {
        animator = GetComponent<Animator>();
        sceneController = SceneController.GetController();
    }
    private void Update()
    {
        if (move)
        {
            move = false;
            MovePingPong();
        }
    }
    public void MovePingPong()
    {
        // move to piercing position
        animator.SetTrigger(MOVE);
        sceneController.PlayPopParticle(slot.targetPointer.transform.position);
    }
    public void MoveBack()
    {
        // move to piercing position
        animator.SetTrigger(BACK);
        sceneController.PlayPopParticle(slot.targetPointer.transform.position);
    }

    public void StopMoving()
    {
        // reset animator to original
        animator.SetTrigger(DEFAULT);
    }
}
