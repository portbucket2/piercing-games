﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RotationAxis
{
    None,
    Xaxis,
    Yaxis,
    Zaxis
}

public class MoveableObject : MonoBehaviour
{
    public RotationAxis rotationAxis;
    public float rotationAmount = 10f;
    public bool isPin = false;
    public Vector2 angleThreshhold;
    public float piercingThreshold = 0.03f;

    [Header("Keep Empty")]
    public CapsuleCollider col;

    private Vector3 initialPosition = new Vector3(0f, 0f, 0f);
    private Vector3 initialRotation = new Vector3(0f, 0f, 90f);

    private FakeBoneObject fakeBone;
    private float jewelDistance;
    private float currentScale;
    private bool scalingDone = false;
    SceneController sceneController;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    private void Awake()
    {
        initialPosition = transform.localPosition;
        initialRotation = transform.eulerAngles;
    }

    void Start()
    {
        sceneController = SceneController.GetController();
        currentScale = transform.localScale.x;
        //if (isPin)
        //{
        //    jewelDistance = transform.parent.GetComponent<RingHolder>().GetJewelDistance();
        //}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetPosition(Vector3 _position)
    {
        transform.position = _position;// transform.InverseTransformPoint( _position);
        //ScaleAnimationStop();
        PhysicsRingCheck();
    }
    public void SetInitialPosition()
    {
        transform.localPosition = initialPosition;
    }
    public void PushToPierce(float movement)
    {
        if (fakeBone)
        {
            fakeBone = GetComponent<FakeBoneObject>();
        }
        float distance = Vector3.Distance(transform.localPosition, initialPosition);
        if (distance < piercingThreshold)
        {
            if (movement > 0)
            {
                transform.position = transform.position + transform.up * Time.deltaTime * piercingThreshold;
            }
            else
            {
                transform.position = transform.position + transform.up * Time.deltaTime * piercingThreshold;
            }
            
        }
        else
        {
            fakeBone = GetComponent<FakeBoneObject>();
            fakeBone.SkinPiercingMovement();
            fakeBone.sceneController.currentBonniefier.Reset();
            fakeBone.ResetToPositionForNeedle();
            sceneController.inputController.StopHoveringMovement();
            Debug.Log("pierced______########");
        }

    }
    public void PushToPin(float movement)
    {
        float distance = Vector3.Distance(transform.localPosition, initialPosition);
        if (distance < piercingThreshold)
        {
            if (movement > 0)
            {
                transform.position = transform.position + transform.up * Time.deltaTime * piercingThreshold;
            }
            else
            {
                transform.position = transform.position + transform.up * Time.deltaTime * piercingThreshold;
            }
            //transform.position = transform.position + transform.up * Time.deltaTime;
        }
        else
        {
            //transform.SetParent(fakeBone.piercingSlot.currentRingHolder.transform);

            fakeBone = GetComponent<FakeBoneObject>();
            fakeBone.SkinPiercingMovement();
            fakeBone.sceneController.currentBonniefier.Reset();
            fakeBone.piercingSlot.currentRingHolder.MovePinToInitialPosition();
            fakeBone.ResetToPositionForPin();
            sceneController.inputController.StopHoveringMovement();
            Debug.Log("pinned______########");
        }

    }
    //float CalculateRadius()
    //{
    //    float highestDistance = 0;
    //    Mesh mesh = GetComponent<MeshFilter>().mesh;
    //    Vector3[] vertices = mesh.vertices;
    //    int max = vertices.Length;
    //    for (int i = 0; i < max; i++)
    //    {
    //        float distance = Vector3.Distance(transform.position, transform.TransformPoint(vertices[i]));
    //        if (distance > highestDistance)
    //        {
    //            highestDistance = distance;
    //        }
    //    }
    //    Debug.Log("highest distance:  " + jewelDistance);
    //    return highestDistance;
    //}
    public void SetOnNeddleBottom()
    {
        //Debug.Log("highest distance:  " + jewelDistance);
        //float radius = jewelDistance;

        //transform.localPosition = new Vector3(0,0f,0f);
        StartCoroutine(MoveRoutine(transform,transform.localPosition, Vector3.zero));
        if (rotationAxis != RotationAxis.None)
        {
            Vector3 temp = Vector3.zero;
            if (rotationAxis == RotationAxis.Xaxis)
            {
                temp = new Vector3(rotationAmount, transform.localEulerAngles.y, transform.localEulerAngles.z);
            }
            else if (rotationAxis == RotationAxis.Yaxis)
            {
                temp = new Vector3(transform.localEulerAngles.x, rotationAmount, transform.localEulerAngles.z);
            }
            else if (rotationAxis == RotationAxis.Zaxis)
            {
                temp = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, rotationAmount);
            }
            StartCoroutine(RotateRoutine(transform, transform.localEulerAngles, temp));
        }
    }
    IEnumerator MoveRoutine(Transform tran, Vector3 start, Vector3 end)
    {
        float step = 0;
        while (step < 1)
        {
            tran.localPosition = Vector3.Lerp(start, end, step);
            step += Time.deltaTime * 3f;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        tran.localPosition = end;
        //boneObject.SkinPiercingMovement(_revers);
    }
    IEnumerator RotateRoutine(Transform tran, Vector3 start, Vector3 end)
    {
        float step = 0;
        while (step < 1)
        {
            tran.localEulerAngles = Vector3.Lerp(start, end, step);
            step += Time.deltaTime * 3f;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        tran.localEulerAngles = end;
        //boneObject.SkinPiercingMovement(_revers);
    }
    public void ScaleAnimationStart()
    {
        scalingDone = false;
        currentScale = transform.localScale.x;
        Debug.Log("current scale: " + currentScale);
        StopAllCoroutines();
        StartCoroutine(ScaleUpDownRoutine(currentScale));
    }
    IEnumerator ScaleUpDownRoutine(float initScale)
    {
        yield return ENDOFFRAME;
        float bigScale = initScale + initScale / 2;
        Vector3 nomalScale = new Vector3(initScale, initScale, initScale);
        Vector3 enlargeScale = new Vector3(bigScale, bigScale, bigScale);
        while (initScale>0)
        {
            transform.localScale = Vector3.Lerp(nomalScale, enlargeScale, Mathf.PingPong(Time.time,1f) );
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;

    }
    public void ScaleAnimationStop()
    {
        if (!scalingDone)
        {
            scalingDone = true;
            Debug.Log("stop scale animation");
            StopAllCoroutines();
            transform.localScale = new Vector3(currentScale, currentScale, currentScale);
        }
    }
    void PhysicsRingCheck()
    {
        if (col)
        {
            col.enabled = false;
        }
    }
}
