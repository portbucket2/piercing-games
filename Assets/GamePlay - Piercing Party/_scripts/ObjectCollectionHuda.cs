﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollectionHuda : MonoBehaviour
{
    public GameObject tempObj;
    public List<ObjectName> objects;

}
[System.Serializable]
public struct ObjectName
{
    public int levelIndex;
    public string name;
    public GameObject piercingRingPrefab;
    public Sprite piercingSprite;
    public bool ismale;
    public int cost;
    public PiercingPositions position;
}
