﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiercingSlot : MonoBehaviour
{
    public string positionName;
    public PiercingPositions myPosition;
    public CameraOrientation myOrientation;
    public CustomerModel customerModel;
    public Animator piercingAnimator;
    public CustomerController customer;
    public GameObject neddle;
    public ParticleSystem targetPointer;

    private UIController uiController;
    private CameraDollyController dollyCam;
    private AnimationEventHandler handeler;
    private Animator currentPiercingAnimator;
    private string anim1;
    private string anim2;
    private SceneController sceneController;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(3.5f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(2f);
    private void Awake()
    {

        currentPiercingAnimator = piercingAnimator;
    }

    void Start()
    {
        handeler = piercingAnimator.GetComponent<AnimationEventHandler>();
        sceneController = SceneController.GetController();
        uiController = customer.uiController;
        dollyCam = customer.dollyCam;
        SelectPiercingScenerio();
    }
    void SelectPiercingScenerio()
    {
        if (myPosition == PiercingPositions.EAR)
        {
            
            anim1 = "ear1";
            anim2 = "ear2";
        }
        else if (myPosition == PiercingPositions.NOSE)
        {
            anim1 = "nose1";
            anim2 = "nose2";
        }
        else if (myPosition == PiercingPositions.LIPS)
        {
            anim1 = "lip1";
            anim2 = "lip2";
        }
        else if (myPosition == PiercingPositions.BROW)
        {
            anim1 = "brow1";
            anim2 = "brow2";
        }
        else if (myPosition == PiercingPositions.BULL)
        {
            anim1 = "bull1";
            anim2 = "bull2";
        }
        //Debug.Log("Piercing Position: " + myPosition);
    }
    public void ResetSlot()
    {
        currentPiercingAnimator.enabled = false;
        currentPiercingAnimator.gameObject.SetActive(false);
    }

    
    
    public void StartTargetPointer()
    {
        targetPointer.Play();
    }
    public Animator GetModelAnimator()
    {
        return customerModel.GetModelAnimator();
    }
    public PiercingPositions GetPiercingPosition()
    {
        return myPosition;
    }
    public CameraOrientation GetCameraOrientation()
    {
        //Debug.Log("Piercing Orientation: " + myOrientation);
        return myOrientation;
    }
    public Vector3 GetPiercingPoint()
    {
        return targetPointer.transform.position;
    }
}
