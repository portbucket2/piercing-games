﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingPrefab : MonoBehaviour
{
    public float speed;
    private Vector3 startPosition;
    private bool isFollowing = false;
    private JellyVertex jellyVertex;
    private Vector3 temp;
    bool isPositive = true;
    private Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
        //startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFollowing)
        {
            //FollowJelly();
        }
    }
    void FollowJelly()
    {
        /*float step = 0.2f;
        while (step > 0f)
        {
            step -= Time.deltaTime * speed;

            float t = 0;
            //t = Mathf.Lerp(transform.localPosition.x -0.4f, transform.localPosition.x + 0.4f, Mathf.PingPong(Time.deltaTime * speed, 1));
            //Vector3 jejo = transform.TransformPoint(jellyVertex.GetCurrentVertexVelocity());
            if (isPositive)
            {
                temp = new Vector3(transform.localPosition.x + step, transform.localPosition.y, transform.localPosition.z);
                isPositive = false;
            }
            else
            {
                temp = new Vector3(transform.localPosition.x - step, transform.localPosition.y, transform.localPosition.z);
                isPositive = true;
            }
            //temp = new Vector3(transform.position.x +jejo.x, transform.position.y, transform.position.z);
            //Debug.Log("VertextVelocity: " + jejo);
            transform.localPosition = temp;
        }*/
        //--------------------
        jellyVertex.ApplyPressureToVertex(transform, transform.position, 0.000010f);
        UpdatePosition();
    }
    public void FollowJellyVertex(JellyVertex jelly, Vector3 slotPosition)
    {
        animator.SetTrigger("move");
        //jellyVertex = jelly;
        //startPosition = slotPosition;
        //StartCoroutine(FollowRoutine());
    }
    IEnumerator FollowRoutine()
    {
        
        float step = 0;
        while (step<1)
        {
            isFollowing = true;
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        isFollowing = false;
        transform.localPosition = startPosition;
    }
    private void UpdatePosition()
    {
        
        jellyVertex.UpdateVelocity(0.050f);
        jellyVertex.Settle(1);

        jellyVertex.currentVertextPosition += jellyVertex.currentVelocity * Time.deltaTime;
        transform.localPosition = jellyVertex.currentVertextPosition;
        
       
    }
}
