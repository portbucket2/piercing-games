﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    public bool isTestScene = false;
    public TestSceneUi testScene;
    public static SceneController _sceneController;

    public DataManager dataManager;
    public ObjectCollectionHuda gameData;
    public int levelCount = 0;
    public int totalStars = 0;
    public int lastPlayedLevel = 0;
    public Animator characterAnimator;
    public CustomerController customer;
    public CameraDollyController dollyCamera;
    public UIController uiController;
    public ParticleSystem popParticle;
    public float popParticlePositionOffset;
    public ParticleSystem missParticle;
    public ParticleSystem happyParticle;

    public Camera mainCamera;
    public BonnieFier currentBonniefier;
    public Transform directionalLight;
    public TouchInputController inputController;
    public SeatController seatController;

    public List<FakeBoneObject> boneObjects;

    public List<RingHolder> ringHolders;

    private int unlockedStageIndex = 0;
    private int boneNumber = 0;
    private int ringNumber = 0;

    private WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    private WaitForSeconds WAITONE = new WaitForSeconds(0.2f);
    private WaitForSeconds WAITTWO = new WaitForSeconds(1f);
    private WaitForSeconds WAITTHREE = new WaitForSeconds(3f);

    private readonly string OUT = "out";
    private int[] ringHoldersSave;
    private bool handTutorialShown = false;
    private void Awake()
    {
        _sceneController = this;
    }
    public static SceneController GetController()
    {
        return _sceneController;
    }

    void Start()
    {


        ringHoldersSave = new int[1];
        ringHoldersSave[0] = -1;
        unlockedStageIndex = 21;
        dataManager.LoadData();

        totalStars = dataManager.GetGamePlayer.totalStars;
        lastPlayedLevel = dataManager.GetGamePlayer.lastPlayedLevel;
        levelCount = lastPlayedLevel;
        ringHoldersSave = dataManager.GetGamePlayer.ringHolders;
        handTutorialShown = dataManager.GetGamePlayer.handTutorialShown;
        SetInventory(totalStars);

        customer.SetPiercingSlot(levelCount);

        BaseLoader.GetIterationIndex(ref levelCount);
        LoadFromLevelLoader(true, levelCount);
        RingHoldersSet();
        //SetRingHoldersList();
    }

    public void TestSceneCommands(FakeBoneObject fakeRuni)
    {
        boneObjects = new List<FakeBoneObject>();
        boneObjects.Add(fakeRuni);
        levelCount = 0;
        totalStars = 100;
        customer.InitializePiercingSlots();
        customer.SetPiercingSlot(levelCount);
    }
    void SetInventory(int total)
    {
        uiController.SetInventory(total);
    }
    public void StartPiercingGameplay(bool isManualLevel, int _level)
    {
        //piercing gameplay starts here
        uiController.PainMeterVisible(true);
        //customer.ShowTargetPoint();
        //currentBonniefier.boneObject.SetFakeBoneObject();
        CurrentPiercingSlotSetup(isManualLevel, _level);
        //inputController.EnableInput();
    }
    public void TurnOffUICam()
    {

    }
    
    public void EndPiercingGameplay()
    {
        StartCoroutine(PhaseTwoRoutine());
    }
    IEnumerator PhaseTwoRoutine()
    {
        customer.GetCurrentFakeBone().GetPiercingSlot().PiercingFixed();
        yield return WAITTWO;
        customer.EnableCustomerSkinnedMesh();
        dollyCamera.ZoomOutOfGamePlay();

        yield return WAITTWO;
        characterAnimator.SetTrigger(OUT);
        //add coustomer goin out of here
        customer.GetUpAndWalk();
        uiController.PainMeterInLove();
        // here start end of level
        yield return WAITTHREE;
        uiController.PainMeterVisible(false);
        BaseLoader.ReportStarCount(3);
        BaseLoader.RequestCompletion(true);
        //uiController.NextLevelPanel();
        //if (isTestScene)
        //{
        //    testScene.ResetPanelState(true);
        //}
        
    }
    public void NextLevel(int stars)
    {
        //AddStars(stars);
        levelCount++;

        if (levelCount > boneObjects.Count-1)
        {
            levelCount = 0;
        }
        //-------level check^^
        //Debug.Log("level count: " + levelCount);
        //GameManagerForPlugins.SetTotalLevelCompletedSoFar(levelCount);
        customer.SetPiercingSlot(levelCount);
        SaveLevel();
    }
    public void AddStars(int earned)
    {
        totalStars += earned;
        SetInventory(totalStars);
        SaveLevel();
    }

    public bool AddStarsAndCheckIfNewUnlocked(int earnedstars, ObjectCollectionHuda objectcollection,out int tempindex)
    {
        tempindex=-1;
        bool unlocked;
        
        for (int i = 0; i < objectcollection.objects.Count; i++)
        {
            if (objectcollection.objects[i].cost > totalStars)
            {
                tempindex = i;

                break;
            }
        }
        AddStars(earnedstars);
        unlocked = tempindex == -1 ? false : !(objectcollection.objects[tempindex].cost > totalStars); 
        for (int i = 0; i < objectcollection.objects.Count; i++)
        {
            if (objectcollection.objects[i].cost > totalStars)
            {
                tempindex = i;

                break;
            }
        }

        return unlocked;
    }
    public int GetCurrentLevel()
    {
        return levelCount;
    }

    public void PlayMissParticle(Vector3 pos)
    {
        Vector3 dir = (mainCamera.transform.position - pos).normalized;
        missParticle.transform.position = pos + (dir * popParticlePositionOffset);
        missParticle.Play();
    }
    public void PlayHappyParticle(Vector3 pos)
    {
        Vector3 dir = (mainCamera.transform.position - pos).normalized;
        happyParticle.transform.position = pos + (dir * (popParticlePositionOffset * 2f));
        happyParticle.Play();
    }
    public void PlayPopParticle(Vector3 position)
    {
        Vector3 dir = (mainCamera.transform.position - position).normalized;
        popParticle.transform.position = position + (dir * popParticlePositionOffset);
        popParticle.Play();
    }
    void SaveLevel()
    {
        GamePlayer gg = new GamePlayer();
        if (dataManager.GetGamePlayer.levelsCompleted < levelCount)
        {
            gg.levelsCompleted = levelCount;
        }
        else
        {
            gg.levelsCompleted = dataManager.GetGamePlayer.levelsCompleted;
        }
        lastPlayedLevel = levelCount;
        gg.totalStars = totalStars;
        gg.lastPlayedLevel = levelCount;

        RingHoldersSave();


        gg.ringHolders = ringHoldersSave;
        gg.handTutorialShown = handTutorialShown;

        dataManager.SetGameplayerData(gg);
    }
    
    void RingHoldersSave()
    {
        int max = boneObjects.Count;
        ringHoldersSave = new int[max];
        for (int i = 0; i < max; i++)
        {
            if (boneObjects[i].piercingSlot.currentRingHolder)
            {
                ringHoldersSave[i] = boneObjects[i].piercingSlot.currentRingHolder.index;
            }
            else
            {
                ringHoldersSave[i] = -1;
            }
            //Debug.Log("Ring Holdrs Save: "+ i +" = " + ringHoldersSave[i]);
        }
    }
    void RingHoldersSet()
    {
        int max = boneObjects.Count;
        Debug.Log("RingHolders Length: " + max);
        for (int i = 0; i < max; i++)
        {
            if (ringHoldersSave[i] >= 0)
            {
                boneObjects[i].piercingSlot.SetSavedRingObject(ringHolders[ringHoldersSave[i]]);
                //Debug.Log("Ring Holdrs Set: " + i + " = " + ringHoldersSave[i]);
            }
        }
    }

    public int GetNextLevelCost(int earned = 0)
    {
        List<ObjectName> objj = new List<ObjectName>();
        objj = gameData.objects;
        int max = objj.Count;
        int tempCount = 0;
        for (int i = 0; i < max; i++)
        {
            //Debug.Log("next Level name: " + objj[i].name);
            //Debug.Log("next Level cost: " + objj[i].cost);
            if (totalStars + earned < objj[i].cost) 
            {
                tempCount = i;
                break;
            }
            else
                tempCount = -1;
        }
        int costNext = 0;
        if (tempCount < 0)
            costNext = 63;
        else
            costNext = objj[tempCount].cost;
        
        Debug.Log("next Level cost: " + costNext);
        return costNext;
    }
    public Sprite GetNextLevelImage()
    {
        List<ObjectName> objj = new List<ObjectName>();
        objj = gameData.objects;
        int max = objj.Count;
        int tempCount = 0;
        for (int i = 0; i < max; i++)
        {
            //Debug.Log("next Level name: " + objj[i].name);
            //Debug.Log("next Level cost: " + objj[i].cost);
            if (totalStars < objj[i].cost)
            {
                tempCount = i;
                break;
            }
            else
                tempCount = 0;
        }
        int costNext = objj[tempCount].cost;
        //Debug.Log("next Level cost: " + costNext);
        return objj[tempCount].piercingSprite;
    }
    public void ResetAllSaveData()
    {
        GamePlayer gg = new GamePlayer();
        dataManager.SetGameplayerData(gg);
    }

    public void RingSelectFromMenu(int level)
    {
        uiController.DisablePiercingSelectionPanel();
        uiController.SetGameOngoingStatus(true);
        ringNumber = level;
        
        currentBonniefier.GetBoneObject().piercingSlot.SetRingObject(ringHolders[ringNumber]);
        inputController.SetPiercingStage(PiercingStages.FloatingPin);
        StartCoroutine(EnableInptAfterTime());
        Show3DHand(false);
        //StartPiercingGameplay();
    }
    IEnumerator EnableInptAfterTime()
    {
        yield return WAITONE;
        inputController.EnableInput();
    }

    void BonnieFie()
    {
        currentBonniefier.SetBonnifier(boneObjects[boneNumber]);
        currentBonniefier.StartBonnifing(ringHolders[ringNumber]);
        boneObjects[boneNumber].SkinPiercingMovement();
    }
    void AllBoneOff()
    {
        for (int i = 0; i < boneObjects.Count; i++)
        {
            boneObjects[i].HideNeedle();
            //boneObjects[i].gameObject.SetActive(false);
        }
    }
    
    public void SetLightSide()
    {
        directionalLight.eulerAngles = new Vector3(directionalLight.eulerAngles.x, -73f, directionalLight.eulerAngles.z);
    }
    public void SetLightFront()
    {
        directionalLight.eulerAngles = new Vector3(directionalLight.eulerAngles.x, 0f, directionalLight.eulerAngles.z);
    }
    void CurrentPiercingSlotSetup(bool setLevelManual, int newLevel = 0)
    {
        if (setLevelManual)
        {
            boneNumber = newLevel;
        }else
            boneNumber = levelCount;
        //GameManagerForPlugins.SetTotalLevelCompletedSoFar(levelCount);
        AllBoneOff();
        boneObjects[boneNumber].gameObject.SetActive(true);
        boneObjects[boneNumber].SetFakeBoneObject();
        boneObjects[boneNumber].ShowTargetPoint();
        currentBonniefier.SetBonnifier(boneObjects[boneNumber]);
        currentBonniefier.GetVertices();
        inputController.SetCurrentPiercingSlot(boneObjects[boneNumber].piercingSlot);
        //dollyCam.ZoomToGamePlay(boneObjects[boneNumber]);
        //if (boneObjects[boneNumber].position == PiercingPositions.EAR)
        //{
        //    SetLightSide();
        //}
        //else
        //{
        //    SetLightFront();
        //}
    }
    public void PositionTargetPointer()
    {
        boneObjects[boneNumber].ShowTargetPoint();
    }
    public void PositionNeddle()
    {
        boneObjects[boneNumber].ShowNeddle();
    }
    public void TestBonniefy()
    {
        //boneNumber = 0;
        //ringNumber = 0;
        BonnieFie();
    }
    public void SetInputPiercingStage(PiercingStages stage)
    {
        inputController.SetPiercingStage(stage);
    }
    public int GetBoneNumber() { return boneNumber; }
    public int GetRingNumber() { return ringNumber; }
    public void ShowLevelComplete()
    {
        uiController.SetGameOngoingStatus(false);
        // show level complete
    }
    public Camera GetMainCamera()
    {
        return mainCamera;
    }
    public List<FakeBoneObject> GetFakeBoneObjects()
    {
        return boneObjects;
    }
    void SetRingHoldersList()
    {
        List<ObjectName> temp = gameData.objects;
        ringHolders = new List<RingHolder>();
        int max = temp.Count;
        for (int i = 0; i < max; i++)
        {
            ringHolders.Add(temp[i].piercingRingPrefab.GetComponent<RingHolder>());
        }
    }

    public void ChangeStatusOfHingeJoint(bool status)
    {
        int max = boneObjects.Count;
        for (int i = 0; i < max; i++)
        {
            if (boneObjects[i].piercingSlot.currentRingHolder)
            {
                if (boneObjects[i].piercingSlot.currentRingHolder.jewel.GetComponent<MoveableObject>().col)
                {
                    boneObjects[i].piercingSlot.currentRingHolder.jewel.GetComponent<MoveableObject>().col.GetComponent<Rigidbody>().isKinematic = status;
                }
            }
        }
    }

    public void SetSeatClose()
    {
        seatController.MoveToSecodaryPostion();
    }
    public void SetSeatFar()
    {
        seatController.MoveToInitialPostion();
    }
    public void SetSeatlayDown()
    {
        seatController.MoveToLayPostion();
    }
    public void Show3DHand(bool show)
    {
        uiController.pierceselection3D.Controltutorial(show);
        handTutorialShown = true;
    }
    public bool HandTutorialShown()
    {
        return handTutorialShown;
    }

    public void LoadFromLevelLoader(bool isManualLevel, int _level)
    {
        uiController.TapToStartPlay(isManualLevel, _level);
    }
}
