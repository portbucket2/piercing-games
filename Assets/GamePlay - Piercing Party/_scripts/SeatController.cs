﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeatController : MonoBehaviour
{
    public float moveSpeed = 3f;
    public Vector3 moveOffset;

    private Vector3 initialPosition;
    private Vector3 secondaryPosition;
    private Vector3 tertiaryPosition;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(0.1f);
    void Start()
    {
        initialPosition = transform.localPosition;
        secondaryPosition = transform.localPosition + moveOffset;
        tertiaryPosition = transform.localPosition + moveOffset*2f;
    }


    public void MoveToSecodaryPostion()
    {
        StartCoroutine(MoveRoutine(initialPosition, secondaryPosition));
    }
    public void MoveToLayPostion()
    {
        StartCoroutine(MoveRoutine(initialPosition, tertiaryPosition));
    }
    public void MoveToInitialPostion()
    {
        StartCoroutine(MoveRoutine(secondaryPosition, initialPosition));
    }

    IEnumerator MoveRoutine(Vector3 start, Vector3 end)
    {
        float step = 0;
        while (step < 1)
        {
            transform.localPosition = Vector3.Lerp(start, end, step);
            step += Time.deltaTime * moveSpeed;
            yield return ENDOFFRAME;
        }
        yield return ENDOFFRAME;
        transform.localPosition = end;
        //boneObject.SkinPiercingMovement(_revers);
    }
}
