﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowBonesScript : MonoBehaviour
{
    void LateUpdate()
    {
        FindChildBone(transform);
    }

    void FindChildBone(Transform gg)
    {
        int count = gg.childCount;
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                FindChildBone(gg.GetChild(i));
                Debug.DrawLine(gg.position, gg.GetChild(i).position, Color.red);
            }
            
        }
    }
}
