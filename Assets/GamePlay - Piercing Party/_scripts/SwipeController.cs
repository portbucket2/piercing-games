﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeController : MonoBehaviour
{
    UIRingCollectionPanelController piercecollection;
    UIRingCollectionPanelController3D piercecollection3d;
    public Camera camcam;
    public float swipedistance = 5;
    public float swipetime = 0.1f;

    Vector3 mousestartpos;
    float mousestarttime;
    // Start is called before the first frame update
    void Start()
    {
        piercecollection = GetComponent<UIRingCollectionPanelController>();
        piercecollection3d = GetComponent<UIRingCollectionPanelController3D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mousestartpos = Input.mousePosition;
            mousestarttime = Time.time;
        }

        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("button pressed ");
            if (Time.time - mousestarttime < swipetime)
            {
                Debug.Log("quick enough");
                float horzSwipe = (Input.mousePosition - mousestartpos).x;
                if (Mathf.Abs(horzSwipe) > swipedistance)
                {
                    Debug.Log("for swipe");

                    if (piercecollection) piercecollection.ChangePage(horzSwipe < 0);
                    else if (piercecollection3d) piercecollection3d.ChangeBox(horzSwipe < 0);
                }
                else
                {
                    Debug.Log("for selection");
                    RaycastHit hit;
                    if (Physics.Raycast(camcam.ScreenPointToRay(Input.mousePosition), out hit, LayerMask.NameToLayer("PierceBoxRingSelection")))
                    {
                        Debug.Log("and also found button");
                        UIPiercingRing3D pierc = hit.collider.gameObject.GetComponent<UIPiercingRing3D>();
                        if(pierc)pierc.StageSelect();
                    }
                }
            }
        }
    }
}
