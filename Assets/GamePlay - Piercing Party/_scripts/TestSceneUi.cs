﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
[System.Serializable]
public struct BoneNames
{ 
    public FakeBoneObject earTopLeft;
    public FakeBoneObject earTopRight;
    public FakeBoneObject earMiddle;
    public FakeBoneObject earBottom;
    public FakeBoneObject noseLeft;
    public FakeBoneObject noseRight;
    public FakeBoneObject noseMiddle;
    public FakeBoneObject lipCenter;
    public FakeBoneObject browLeft;
    public FakeBoneObject browRight;
    public FakeBoneObject bellyButton;
}

public class TestSceneUi : MonoBehaviour
{
    
    public GameObject characterPanel;
    public GameObject positionPanel;
    public GameObject resetPanel;


    public Button resetButton;

    public Button buttonMale01;
    public Button buttonMale02;
    public Button buttonFemale01;
    public Button buttonFemale02;

    public Button button_earTopLeft;
    public Button button_earTopRight;
    public Button button_earMiddle;
    public Button button_earBottom;
    public Button button_noseLeft;
    public Button button_noseRight;
    public Button button_noseMiddle;
    public Button button_lipCenter;
    public Button button_browLeft;
    public Button button_browRight;
    public Button button_belly;

    public BoneNames male01;
    public BoneNames male02;
    public BoneNames female01;
    public BoneNames female02;

    [Header("No Input Required")]
    public FakeBoneObject currentFakeBoneObject;

    SceneController sceneController;
    void Start()
    {
        sceneController = SceneController.GetController();
        ButtonInit();
    }

    void ButtonInit()
    {
        resetButton.onClick.AddListener(delegate { SceneManager.LoadScene("PiercingSegmentedTestScene");});
        buttonMale01.onClick.AddListener(delegate { SetAllToMale01Button(); CharacterPanelState(false); });
        buttonMale02.onClick.AddListener(delegate { SetAllToMale02Button(); CharacterPanelState(false); });
        buttonFemale01.onClick.AddListener(delegate { SetAllToFemale01Button(); CharacterPanelState(false); });
        buttonFemale02.onClick.AddListener(delegate { SetAllToFemale02Button(); CharacterPanelState(false); });
    }
    public void ResetPanelState(bool state) { resetPanel.SetActive(state); }
    void CharacterPanelState(bool state) { characterPanel.SetActive(state); }
    void PositionPanelState(bool state) { positionPanel.SetActive(state); sceneController.TestSceneCommands(currentFakeBoneObject); }

    void SetAllToMale01Button()
    {
        button_earTopLeft.onClick.AddListener(delegate { currentFakeBoneObject = male01.earTopLeft; PositionPanelState(false); });
        button_earTopRight.onClick.AddListener(delegate { currentFakeBoneObject = male01.earTopRight; PositionPanelState(false); });
        button_earMiddle.onClick.AddListener(delegate { currentFakeBoneObject = male01.earMiddle; PositionPanelState(false); });
        button_earBottom.onClick.AddListener(delegate { currentFakeBoneObject = male01.earBottom; PositionPanelState(false); });
        button_noseLeft.onClick.AddListener(delegate { currentFakeBoneObject = male01.noseLeft; PositionPanelState(false); });
        button_noseRight.onClick.AddListener(delegate { currentFakeBoneObject = male01.noseRight; PositionPanelState(false); });
        button_noseMiddle.onClick.AddListener(delegate { currentFakeBoneObject = male01.noseMiddle; PositionPanelState(false); });
        button_lipCenter.onClick.AddListener(delegate { currentFakeBoneObject = male01.lipCenter; PositionPanelState(false); });
        button_browLeft.onClick.AddListener(delegate { currentFakeBoneObject = male01.browLeft; PositionPanelState(false); });
        button_browRight.onClick.AddListener(delegate { currentFakeBoneObject = male01.browRight; PositionPanelState(false); });
        button_belly.onClick.AddListener(delegate { currentFakeBoneObject = male01.browRight; PositionPanelState(false); });
    }
    void SetAllToMale02Button()
    {
        button_earTopLeft.onClick.AddListener(delegate { currentFakeBoneObject = male02.earTopLeft; PositionPanelState(false); });
        button_earTopRight.onClick.AddListener(delegate { currentFakeBoneObject = male02.earTopRight; PositionPanelState(false); });
        button_earMiddle.onClick.AddListener(delegate { currentFakeBoneObject = male02.earMiddle; PositionPanelState(false); });
        button_earBottom.onClick.AddListener(delegate { currentFakeBoneObject = male02.earBottom; PositionPanelState(false); });
        button_noseLeft.onClick.AddListener(delegate { currentFakeBoneObject = male02.noseLeft; PositionPanelState(false); });
        button_noseRight.onClick.AddListener(delegate { currentFakeBoneObject = male02.noseRight; PositionPanelState(false); });
        button_noseMiddle.onClick.AddListener(delegate { currentFakeBoneObject = male02.noseMiddle; PositionPanelState(false); });
        button_lipCenter.onClick.AddListener(delegate { currentFakeBoneObject = male02.lipCenter; PositionPanelState(false); });
        button_browLeft.onClick.AddListener(delegate { currentFakeBoneObject = male02.browLeft; PositionPanelState(false); });
        button_browRight.onClick.AddListener(delegate { currentFakeBoneObject = male02.browRight; PositionPanelState(false); });
        button_belly.onClick.AddListener(delegate { currentFakeBoneObject = male02.browRight; PositionPanelState(false); });
    }
    void SetAllToFemale01Button()
    {
        button_earTopLeft.onClick.AddListener(delegate { currentFakeBoneObject = female01.earTopLeft; PositionPanelState(false); });
        button_earTopRight.onClick.AddListener(delegate { currentFakeBoneObject = female01.earTopRight; PositionPanelState(false); });
        button_earMiddle.onClick.AddListener(delegate { currentFakeBoneObject = female01.earMiddle; PositionPanelState(false); });
        button_earBottom.onClick.AddListener(delegate { currentFakeBoneObject = female01.earBottom; PositionPanelState(false); });
        button_noseLeft.onClick.AddListener(delegate { currentFakeBoneObject = female01.noseLeft; PositionPanelState(false); });
        button_noseRight.onClick.AddListener(delegate { currentFakeBoneObject = female01.noseRight; PositionPanelState(false); });
        button_noseMiddle.onClick.AddListener(delegate { currentFakeBoneObject = female01.noseMiddle; PositionPanelState(false); });
        button_lipCenter.onClick.AddListener(delegate { currentFakeBoneObject = female01.lipCenter; PositionPanelState(false); });
        button_browLeft.onClick.AddListener(delegate { currentFakeBoneObject = female01.browLeft; PositionPanelState(false); });
        button_browRight.onClick.AddListener(delegate { currentFakeBoneObject = female01.browRight; PositionPanelState(false); });
        button_belly.onClick.AddListener(delegate { currentFakeBoneObject = female01.bellyButton; PositionPanelState(false); });
    }
    void SetAllToFemale02Button()
    {
        button_earTopLeft.onClick.AddListener(delegate { currentFakeBoneObject = female02.earTopLeft; PositionPanelState(false); });
        button_earTopRight.onClick.AddListener(delegate { currentFakeBoneObject = female02.earTopRight; PositionPanelState(false); });
        button_earMiddle.onClick.AddListener(delegate { currentFakeBoneObject = female02.earMiddle; PositionPanelState(false); });
        button_earBottom.onClick.AddListener(delegate { currentFakeBoneObject = female02.earBottom; PositionPanelState(false); });
        button_noseLeft.onClick.AddListener(delegate { currentFakeBoneObject = female02.noseLeft; PositionPanelState(false); });
        button_noseRight.onClick.AddListener(delegate { currentFakeBoneObject = female02.noseRight; PositionPanelState(false); });
        button_noseMiddle.onClick.AddListener(delegate { currentFakeBoneObject = female02.noseMiddle; PositionPanelState(false); });
        button_lipCenter.onClick.AddListener(delegate { currentFakeBoneObject = female02.lipCenter; PositionPanelState(false); });
        button_browLeft.onClick.AddListener(delegate { currentFakeBoneObject = female02.browLeft; PositionPanelState(false); });
        button_browRight.onClick.AddListener(delegate { currentFakeBoneObject = female02.browRight; PositionPanelState(false); });
        button_belly.onClick.AddListener(delegate { currentFakeBoneObject = female02.bellyButton; PositionPanelState(false); });
    }
}
