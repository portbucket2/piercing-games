﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PiercingStages
{
    NeddleFloat,
    PushToPierce,
    RingSelect,
    FloatingPin,
    PushPinIn,
    FloatingJewel,
    LevelComplete
}

public class TouchInputController : MonoBehaviour
{
    public Camera mainCamera;
    public CameraDollyController dollyCam;
    public SceneController sceneController;
    public bool isFreeMoveOn = true;
    public float speed;
    public float floatDistance;
    public PiercingStages currentPiercingStage;

    float rotX;
    float rotY;

    public MoveableObject currentMoveableObject;
    public DermalPiercingSlot currentPiercingSlot;

    private Vector3 _dir;
    private Vector3 _position;
    private Vector3 _dirToCam;
    private Vector3 oldHit;
    private Vector3 newHit;
    [SerializeField]
    private bool isInputEnabled = false;
    private UIController uiController;

    private float inActivityCounter = 5f;
    void Start()
    {
        //currentMoveableObject.SetPosition(currentMoveableObject.transform.position + Vector3.down * 0.2f);
        sceneController = SceneController.GetController();
        uiController = sceneController.uiController;
    }
        
    void Update()
    {
        if (Application.isEditor)
        {
            #region Editor mouse input

            inActivityCounter -= Time.deltaTime;
            if (inActivityCounter < 0 && uiController.GetGameOngoingStatus())
            {
                uiController.TutorialPanelStatus(true);
            }
            if (Input.GetMouseButtonDown(0))
            {
                isFreeMoveOn = true;
                inActivityCounter = 5f;
                uiController.TutorialPanelStatus(false);
                Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100))
                {
                    oldHit = hit.point;
                }
                //currentMoveableObject.ScaleAnimationStop();
            }
            if (Input.GetMouseButton(0))
            {
                inActivityCounter = 5f;
                if (isFreeMoveOn && isInputEnabled)
                {
                    Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, 100))
                    {
                        //Debug.DrawLine(ray.origin, hit.point, Color.green);
                        newHit = hit.point;
                        //--------------------------------------------------------------------------------------------------------------
                        if (currentPiercingStage == PiercingStages.NeddleFloat)
                        {
                            Vector3 objectPostion = currentMoveableObject.transform.position;
                            Vector3 objectDirection = currentMoveableObject.transform.position - mainCamera.transform.position;
                            Ray ray2 = new Ray(objectPostion, objectDirection);
                            RaycastHit hit2;
                            if (Physics.Raycast(ray2, out hit2, 100))
                            {
                                //Debug.DrawLine(ray2.origin, hit2.point, Color.red);
                                //_dir = (dollyCam.transform.position - hit.point).normalized;
                                //_position = hit.point + (_dir * floatDistance) + (transform.up * floatDistance);
                                _dirToCam = (dollyCam.transform.position - hit2.point).normalized;
                                _dir = newHit - oldHit;
                                _position = currentMoveableObject.transform.position;
                                Vector3 screenPoint = mainCamera.WorldToViewportPoint(_position);
                                bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                                if (onScreen)
                                    _position += _dir ;
                                else
                                {
                                    _position = hit.point + _dirToCam * 0.1f;
                                    isFreeMoveOn = false;
                                }

                                oldHit = newHit;
                                float distance = Vector3.Distance(hit2.point, currentPiercingSlot.transform.position);
                                if (distance < 0.1f)
                                {
                                    isFreeMoveOn = false;
                                    //Debug.Log("reached!!!!!!!!!!!!!!!");
                                    currentMoveableObject.SetInitialPosition();
                                    currentMoveableObject.ScaleAnimationStop();
                                    SetCurrentMoveableObject(sceneController.currentBonniefier.GetBoneObject().GetComponent<MoveableObject>());
                                    SetPiercingStage(PiercingStages.PushToPierce);
                                    sceneController.TestBonniefy();
                                    // change current movable object and bonniefy
                                }
                                else
                                {
                                    currentMoveableObject.SetPosition(_position);
                                }
                            }
                        }//--------------------------------------------------------------------------------------------------------------
                        else if (currentPiercingStage == PiercingStages.PushToPierce)
                        {
                            uiController.PainMeterRunning(true);
                            rotY = Input.GetAxis("Mouse Y") * speed * Mathf.Deg2Rad;
                            currentMoveableObject.PushToPierce(rotY);
                        }//--------------------------------------------------------------------------------------------------------------
                        else if (currentPiercingStage == PiercingStages.RingSelect)
                        {
                            //select piercing here
                        }//--------------------------------------------------------------------------------------------------------------
                        else if (currentPiercingStage == PiercingStages.FloatingPin)
                        {
                            Vector3 objectPostion = currentMoveableObject.transform.position;
                            Vector3 objectDirection = currentMoveableObject.transform.position - mainCamera.transform.position;
                            Ray ray2 = new Ray(objectPostion, objectDirection);
                            RaycastHit hit2;
                            if (Physics.Raycast(ray2, out hit2, 100))
                            {
                                // floating pin function
                                uiController.PainMeterRunning(false);
                                //Debug.DrawLine(ray.origin, hit.point);
                                //_dir = (dollyCam.transform.position - hit.point).normalized;
                                //_position = hit.point + (_dir * floatDistance) + (transform.up * floatDistance);
                                _dir = newHit - oldHit;
                                _position = currentMoveableObject.transform.position;
                                Vector3 screenPoint = mainCamera.WorldToViewportPoint(_position);
                                bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                                if (onScreen)
                                    _position += _dir;
                                else
                                {
                                    _position = hit.point + _dirToCam * 0.1f;
                                    isFreeMoveOn = false;
                                }
                                oldHit = newHit;

                                float distance = Vector3.Distance(hit2.point, currentPiercingSlot.transform.position);
                                if (distance < 0.1f)
                                {
                                    isFreeMoveOn = false;
                                    //Debug.Log("pinned!!!!!!!!!!!!!!!");
                                    currentMoveableObject.ScaleAnimationStop();
                                    currentMoveableObject.transform.SetParent(sceneController.currentBonniefier.GetBoneObject().piercingSlot.neddleBottom);
                                    //currentMoveableObject.transform.localPosition = Vector3.zero;
                                    currentMoveableObject.SetOnNeddleBottom();
                                    SetPiercingStage(PiercingStages.PushPinIn);
                                    SetCurrentMoveableObject(sceneController.boneObjects[sceneController.GetBoneNumber()].GetComponent<MoveableObject>());
                                    //SetCurrentMoveableObject(currentMoveableObject.transform.parent.GetComponent<RingHolder>().jewel.GetComponent<MoveableObject>());


                                    sceneController.TestBonniefy();
                                    // change current movable object and bonniefy
                                }
                                else
                                {
                                    currentMoveableObject.SetPosition(_position);
                                }
                            }
                        }//--------------------------------------------------------------------------------------------------------------
                        else if (currentPiercingStage == PiercingStages.PushPinIn)
                        {
                            uiController.PainMeterRunning(true);
                            rotY = Input.GetAxis("Mouse Y") * speed * Mathf.Deg2Rad;
                            currentMoveableObject.PushToPin(rotY);
                        }//--------------------------------------------------------------------------------------------------------------
                        else if (currentPiercingStage == PiercingStages.FloatingJewel)
                        {
                            Vector3 objectPostion = currentMoveableObject.transform.position;
                            Vector3 objectDirection = currentMoveableObject.transform.position - mainCamera.transform.position;
                            Ray ray2 = new Ray(objectPostion, objectDirection);
                            RaycastHit hit2;
                            if (Physics.Raycast(ray2, out hit2, 100))
                            {
                                // floating jewel function
                                uiController.PainMeterRunning(false);
                                //Debug.DrawLine(ray.origin, hit.point);
                                //_dir = (dollyCam.transform.position - hit.point).normalized;
                                //_position = hit.point + (_dir * floatDistance) + (transform.up * floatDistance);
                                _dir = newHit - oldHit;
                                _position = currentMoveableObject.transform.position;
                                Vector3 screenPoint = mainCamera.WorldToViewportPoint(_position);
                                bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                                if (onScreen)
                                    _position += _dir;
                                else
                                {
                                    _position = hit.point + _dirToCam * 0.1f;
                                    isFreeMoveOn = false;
                                }
                                oldHit = newHit;

                                float distance = Vector3.Distance(hit2.point, currentPiercingSlot.transform.position);
                                if (distance < 0.1f)
                                {
                                    isFreeMoveOn = false;
                                    //Debug.Log("jeweled!!!!!!!!!!!!!!!");
                                    currentMoveableObject.transform.parent.GetComponent<RingHolder>().MoveJewelToInitialPosition();
                                    currentMoveableObject.transform.parent.parent.GetComponent<DermalPiercingSlot>().TargetParticleOff();
                                    sceneController.ShowLevelComplete();
                                    sceneController.PlayHappyParticle(currentMoveableObject.transform.parent.parent.GetComponent<DermalPiercingSlot>().transform.position);
                                    SetPiercingStage(PiercingStages.LevelComplete);
                                    DisableInput();
                                    sceneController.EndPiercingGameplay();

                                    currentMoveableObject.ScaleAnimationStop();
                                }
                                else
                                {
                                    currentMoveableObject.SetPosition(_position);
                                }
                            }
                        }//--------------------------------------------------------------------------------------------------------------
                        else if (currentPiercingStage == PiercingStages.LevelComplete)
                        {

                        }
                    }

                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                uiController.PainMeterRunning(false);
            }

            #endregion

        }
        else
        {
            #region Editor touch input
            inActivityCounter -= Time.deltaTime;
            if (inActivityCounter < 0 && uiController.GetGameOngoingStatus())
            {
                uiController.TutorialPanelStatus(true);
            }
            if (Input.touchCount >  0)
            {
                Touch touch = Input.GetTouch(0);
                
                
                if (touch.phase == TouchPhase.Began)
                {
                    isFreeMoveOn = true;
                    inActivityCounter = 5f;
                    uiController.TutorialPanelStatus(false);
                    Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, 100))
                    {
                        oldHit = hit.point;
                    }
                    //currentMoveableObject.ScaleAnimationStop();
                }
                if (touch.phase == TouchPhase.Moved)
                {
                    inActivityCounter = 5f;
                    if (isFreeMoveOn && isInputEnabled)
                    {
                        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit, 100))
                        {
                            //Debug.DrawLine(ray.origin, hit.point, Color.green);
                            newHit = hit.point;
                            //--------------------------------------------------------------------------------------------------------------
                            if (currentPiercingStage == PiercingStages.NeddleFloat)
                            {
                                Vector3 objectPostion = currentMoveableObject.transform.position;
                                Vector3 objectDirection = currentMoveableObject.transform.position - mainCamera.transform.position;
                                Ray ray2 = new Ray(objectPostion, objectDirection);
                                RaycastHit hit2;
                                if (Physics.Raycast(ray2, out hit2, 100))
                                {
                                    //Debug.DrawLine(ray2.origin, hit2.point, Color.red);
                                    //_dir = (dollyCam.transform.position - hit.point).normalized;
                                    //_position = hit.point + (_dir * floatDistance) + (transform.up * floatDistance);
                                    _dirToCam = (dollyCam.transform.position - hit2.point).normalized;
                                    _dir = newHit - oldHit;
                                    _position = currentMoveableObject.transform.position;
                                    Vector3 screenPoint = mainCamera.WorldToViewportPoint(_position);
                                    bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                                    if (onScreen)
                                        _position += _dir;
                                    else
                                    {
                                        _position = hit.point + _dirToCam * 0.1f;
                                        isFreeMoveOn = false;
                                    }

                                    oldHit = newHit;
                                    float distance = Vector3.Distance(hit2.point, currentPiercingSlot.transform.position);
                                    if (distance < 0.1f)
                                    {
                                        isFreeMoveOn = false;
                                        //Debug.Log("reached!!!!!!!!!!!!!!!");
                                        currentMoveableObject.SetInitialPosition();
                                        currentMoveableObject.ScaleAnimationStop();
                                        SetCurrentMoveableObject(sceneController.currentBonniefier.GetBoneObject().GetComponent<MoveableObject>());
                                        SetPiercingStage(PiercingStages.PushToPierce);
                                        sceneController.TestBonniefy();
                                        // change current movable object and bonniefy
                                    }
                                    else
                                    {
                                        currentMoveableObject.SetPosition(_position);
                                    }
                                }
                            }//--------------------------------------------------------------------------------------------------------------
                            else if (currentPiercingStage == PiercingStages.PushToPierce)
                            {
                                uiController.PainMeterRunning(true);
                                rotY = Input.GetAxis("Mouse Y") * speed * Mathf.Deg2Rad;
                                currentMoveableObject.PushToPierce(rotY);
                            }//--------------------------------------------------------------------------------------------------------------
                            else if (currentPiercingStage == PiercingStages.RingSelect)
                            {
                                //select piercing here
                            }//--------------------------------------------------------------------------------------------------------------
                            else if (currentPiercingStage == PiercingStages.FloatingPin)
                            {
                                Vector3 objectPostion = currentMoveableObject.transform.position;
                                Vector3 objectDirection = currentMoveableObject.transform.position - mainCamera.transform.position;
                                Ray ray2 = new Ray(objectPostion, objectDirection);
                                RaycastHit hit2;
                                if (Physics.Raycast(ray2, out hit2, 100))
                                {
                                    // floating pin function
                                    uiController.PainMeterRunning(false);
                                    //Debug.DrawLine(ray.origin, hit.point);
                                    //_dir = (dollyCam.transform.position - hit.point).normalized;
                                    //_position = hit.point + (_dir * floatDistance) + (transform.up * floatDistance);
                                    _dir = newHit - oldHit;
                                    _position = currentMoveableObject.transform.position;
                                    Vector3 screenPoint = mainCamera.WorldToViewportPoint(_position);
                                    bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                                    if (onScreen)
                                        _position += _dir;
                                    else
                                    {
                                        _position = hit.point + _dirToCam * 0.1f;
                                        isFreeMoveOn = false;
                                    }
                                    oldHit = newHit;

                                    float distance = Vector3.Distance(hit2.point, currentPiercingSlot.transform.position);
                                    if (distance < 0.1f)
                                    {
                                        isFreeMoveOn = false;
                                        //Debug.Log("pinned!!!!!!!!!!!!!!!");
                                        currentMoveableObject.ScaleAnimationStop();
                                        currentMoveableObject.transform.SetParent(sceneController.currentBonniefier.GetBoneObject().piercingSlot.neddleBottom);
                                        //currentMoveableObject.transform.localPosition = Vector3.zero;
                                        currentMoveableObject.SetOnNeddleBottom();
                                        SetPiercingStage(PiercingStages.PushPinIn);
                                        SetCurrentMoveableObject(sceneController.boneObjects[sceneController.GetBoneNumber()].GetComponent<MoveableObject>());
                                        //SetCurrentMoveableObject(currentMoveableObject.transform.parent.GetComponent<RingHolder>().jewel.GetComponent<MoveableObject>());


                                        sceneController.TestBonniefy();
                                        // change current movable object and bonniefy
                                    }
                                    else
                                    {
                                        currentMoveableObject.SetPosition(_position);
                                    }
                                }
                            }//--------------------------------------------------------------------------------------------------------------
                            else if (currentPiercingStage == PiercingStages.PushPinIn)
                            {
                                uiController.PainMeterRunning(true);
                                rotY = Input.GetAxis("Mouse Y") * speed * Mathf.Deg2Rad;
                                currentMoveableObject.PushToPin(rotY);
                            }//--------------------------------------------------------------------------------------------------------------
                            else if (currentPiercingStage == PiercingStages.FloatingJewel)
                            {
                                Vector3 objectPostion = currentMoveableObject.transform.position;
                                Vector3 objectDirection = currentMoveableObject.transform.position - mainCamera.transform.position;
                                Ray ray2 = new Ray(objectPostion, objectDirection);
                                RaycastHit hit2;
                                if (Physics.Raycast(ray2, out hit2, 100))
                                {
                                    // floating jewel function
                                    uiController.PainMeterRunning(false);
                                    //Debug.DrawLine(ray.origin, hit.point);
                                    //_dir = (dollyCam.transform.position - hit.point).normalized;
                                    //_position = hit.point + (_dir * floatDistance) + (transform.up * floatDistance);
                                    _dir = newHit - oldHit;
                                    _position = currentMoveableObject.transform.position;
                                    Vector3 screenPoint = mainCamera.WorldToViewportPoint(_position);
                                    bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                                    if (onScreen)
                                        _position += _dir;
                                    else
                                    {
                                        _position = hit.point + _dirToCam * 0.1f;
                                        isFreeMoveOn = false;
                                    }
                                    oldHit = newHit;

                                    float distance = Vector3.Distance(hit2.point, currentPiercingSlot.transform.position);
                                    if (distance < 0.1f)
                                    {
                                        isFreeMoveOn = false;
                                        //Debug.Log("jeweled!!!!!!!!!!!!!!!");
                                        currentMoveableObject.transform.parent.GetComponent<RingHolder>().MoveJewelToInitialPosition();
                                        currentMoveableObject.transform.parent.parent.GetComponent<DermalPiercingSlot>().TargetParticleOff();
                                        sceneController.ShowLevelComplete();
                                        sceneController.PlayHappyParticle(currentMoveableObject.transform.parent.parent.GetComponent<DermalPiercingSlot>().transform.position);
                                        SetPiercingStage(PiercingStages.LevelComplete);
                                        DisableInput();
                                        sceneController.EndPiercingGameplay();

                                        currentMoveableObject.ScaleAnimationStop();
                                    }
                                    else
                                    {
                                        currentMoveableObject.SetPosition(_position);
                                    }
                                }
                            }//--------------------------------------------------------------------------------------------------------------
                            else if (currentPiercingStage == PiercingStages.LevelComplete)
                            {

                            }
                        }

                    }
                }
                if (touch.phase == TouchPhase.Ended)
                {
                    uiController.PainMeterRunning(false);
                }
            }
            #endregion
        }
    }

    public void SetPiercingStage(PiercingStages stage)
    {
        currentPiercingStage = stage;
    }
    public void SetCurrentMoveableObject(MoveableObject obaja, bool scale = false)
    {
        currentMoveableObject = obaja;
        //currentMoveableObject.SetPosition(currentMoveableObject.transform.position + Vector3.down * 0.2f);

        if (scale)
        {
            Debug.Log("scaling start!!");
            currentMoveableObject.ScaleAnimationStart();
        }
    }
    public void SetCurrentPiercingSlot(DermalPiercingSlot slot)
    {
        currentPiercingSlot = slot; 
        currentMoveableObject = slot.neddle.GetComponent<MoveableObject>();
        currentMoveableObject.SetPosition(currentMoveableObject.transform.position + Vector3.down * 4f);
        TouchInputInit();
    }
    public void EnableInput() { isInputEnabled = true; }
    public void DisableInput() { isInputEnabled = false; }

    public void TouchInputInit()
    {
        SetPiercingStage(PiercingStages.NeddleFloat);
    }
    public void StopHoveringMovement()
    {
        uiController.PainMeterRunning(false);
        uiController.SetGameOngoingStatus(false);
        isFreeMoveOn = false;
    }
}

