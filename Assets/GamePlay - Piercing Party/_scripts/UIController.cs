﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class UIController : MonoBehaviour
{
    public Camera uiCamera;
    public Camera gameCamera;
    public Transform targetPosition;

    //public Button selectButton;
    public Button ResetButton;
    public Button StartToPlayButton;
    public GameObject TaptoStartPanel;
    public Button TapToStartPiercingButton;

    public CustomerController customer;

    public Image step1Image;
    public Image step2Image;
    public CameraDollyController dollyCam;
    [Header("main panel collection")]
    public TextMeshProUGUI totalStarText;
    public Button ringCollectionButton;
    public UIRingCollectionPanelController ringCollectionController;
    public GameObject gameStepPanel;
    public GameObject nextLevelPanel;
    public Button nextLevelButton;
    public Button watchVideoAdButton;
    public Button claimRewardButton;
    public Button pierceRewardButton;
    public Button bonusStarsButton;
    public GameObject claimRewardpanel;
    public GameObject pierceRewardpanel;
    public GameObject retryPanel;
    public GameObject persistentPanel;
    public GameObject selectPiercingPanel;
    public UIRingCollectionPanelController selectPiercingPanelController;
    public UIPainMeterController painMeter;

    public UIRingCollectionPanelController3D pierceselection3D;

    public Image RewardPirceImage;
    public ObjectCollectionHuda objectcollection;
    public GameObject tutorialPanel;

    private bool isVerticalMovementOn = false;
    private float xPoint = 0;
    private float yPoint = 0;
    private SceneController sceneController;

    private int foulcount = 0;
    private int levelEarnings = 0;
    bool isHomescene = true;
    bool isGameOngoing = false;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTHIRTY = new WaitForSeconds(30f);
    public static string currentPlacement { get; private set; }
    private void Awake()
    {
        
    }



    void Start()
    {
        sceneController = SceneController.GetController();

        ButtonCollection();
        SetProgressLevel(0f);
    }

    void ButtonCollection()
    {
        
        ResetButton.onClick.AddListener(delegate
        {
            ResetAll();
        });
        nextLevelButton.onClick.AddListener(delegate
        {
            NextLevel();
        });
        StartToPlayButton.onClick.AddListener(delegate
        {
            TapToStartPlay(false, 0);
        });
        TapToStartPiercingButton.onClick.AddListener(delegate
        {
            TapToStartPiercing();
        });
        ringCollectionButton.onClick.AddListener(delegate
        {
            //ringCollectionController.gameObject.SetActive(true);
            //ringCollectionController.UpdateGameProgress();
            pierceselection3D.TurnOnPanel(true);
            TaptoStartPanel.SetActive(false);
            persistentPanel.SetActive(false);
            sceneController.inputController.DisableInput();
        });
        watchVideoAdButton.onClick.AddListener(delegate
        {
            WatchVideoAd();
        });
        claimRewardButton.onClick.AddListener(delegate
        {
            ClaimVideoReward();
        });
        bonusStarsButton.onClick.AddListener(delegate
        {
            GetBonusStars();
        });
        pierceRewardButton.onClick.AddListener(delegate
        {
            pierceRewardpanel.SetActive(false);
        });

    }

    
    public void SetGameOngoingStatus(bool isOnGoing)
    {
        isGameOngoing = isOnGoing;
    }
    public bool GetGameOngoingStatus()
    {
        return isGameOngoing;
    }

    public void PainmeterActivationControls(bool show)
    {
        painMeter.gameObject.SetActive(show);
    }
    
    bool CalculateAccuracyPass()
    {
        bool temp = true;
        // define to play happy particle or not

        return temp;
    }

    
    void NextLevel()
    {
        //if (sceneController.GetCurrentLevel() < customer.fakeBones.Count - 1)
        //{
        //    Debug.Log("all level complete!!!");
        //}
        //AnalyticsController.LogEvent_LevelCompleted(sceneController.levelCount);
        //sceneController.NextLevel(levelEarnings); //_______________________________ send value of earned star
        //sceneController.TurnOffUICam();
        //nextLevelPanel.SetActive(false);
        //customer.Reset();
        //customer.GoAndSit();
        //foulcount = 0;
        //SetGameOngoingStatus(true);
    }
    public void NextLevelPanel()
    {
        //sceneController.TurnOnUICam();
        nextLevelPanel.SetActive(true);
        nextLevelButton.interactable = false;
        int unlockindex;
        //sceneController.AddStars(3);
        bool unlocknew = sceneController.AddStarsAndCheckIfNewUnlocked(CalculateStarResult(), objectcollection, out unlockindex);
        
        nextLevelPanel.GetComponent<UIStageCompletePanelManager>().SetStageResult(levelEarnings, unlockindex<0?unlockindex: objectcollection.objects[unlockindex].cost, unlockindex < 0 ? unlockindex : objectcollection.objects[unlockindex-1].cost, unlockindex != -1 ? objectcollection.objects[unlockindex].piercingSprite : RewardPirceImage.sprite,()=> {
            if (unlocknew)
            {
                ActivateRewardPanel(unlockindex - 1);
            }
            nextLevelButton.interactable = true;
        });
        //nextLevelPanel.GetComponent<UIStageCompletePanelManager>().SetStageResult(CalculateStarResult(), RewardPirceImage.sprite);
        bonusStarsButton.interactable = true;
        RingCollectionOnOff(false);
        TurnOffRingCollectionPanel();
        //Portbliss.Ad.AdvertisementWrapper.AdIteration((success) =>
        //{


        //}, false, true);
    }

    void ActivateRewardPanel(int spriteindex)
    {
        pierceRewardpanel.SetActive(true);
        RewardPirceImage.sprite = objectcollection.objects[spriteindex].piercingSprite;
        //nextLevelPanel.GetComponent<UIStageCompletePanelManager>().nextpierce.sprite = objectcollection.objects[spriteindex+1].piercingSprite; ;
    }

    int CalculateStarResult()
    {
        int temp = 0;
        if (foulcount == 0)
            temp = 3;
        else if (foulcount == 1)
            temp = 2;
        else if (foulcount >= 2)
            temp = 1;
        levelEarnings = temp;
        return temp;
    }


    public void SetProgressLevel(float value)
    {
        if (value > 1)
            value = 1;

        StartCoroutine(SetProgressSmooth(value));
    }
    IEnumerator SetProgressSmooth(float val)
    {
        //float step = 0;
        //float temp = 0;
        //while (step<1)
        //{
        //    temp = Mathf.Lerp(progressImage.fillAmount, val, step);
        //    step += Time.deltaTime * progressImageSpeed;
        //    progressImage.fillAmount = temp;
        //    yield return ENDOFFRAME;
        //}
        //progressImage.fillAmount = val;
        if (val == 0)
        {
            step1Image.enabled = false;
            step2Image.enabled = false;
        }
        else
        {
            if (val < 0.6f)
            {
                step1Image.enabled = true;
                step2Image.enabled = false;
            }
            else
            {
                step1Image.enabled = true;
                step2Image.enabled = true;
            }
        }
        yield return ENDOFFRAME;
    }
    void ResetAll()
    {
        //SceneManager.LoadScene("PiercingSceneRak");
        sceneController.ResetAllSaveData();
    }
    public void TurnOnHomePanel()
    {
        persistentPanel.SetActive(true);
        if (isHomescene)
        {
            TaptoStartPanel.SetActive(true);
        }
    }
    public void RingCollectionOnOff(bool isOn)
    {
        ringCollectionButton.interactable = isOn;
    }

    void WatchVideoAd()
    {
        currentPlacement = "HomeScreen";
        watchVideoAdButton.interactable = false;
        if (Application.isEditor)
        {
            VideoAdComplete();
        }
        else
        {
            //Portbliss.Ad.AdController.ShowRewardedVideoAd((success) =>
            //{
            //    if (success)
            //    {
            //        VideoAdComplete();
            //    }
            //    else
            //    {
            //        StartCoroutine(RewardedAdTimer());
            //    }
            //});
        }
    }
    public void VideoAdComplete()
    {
        claimRewardpanel.SetActive(true);
        for (int i = 0; i < objectcollection.objects.Count; i++)
        {
            if (objectcollection.objects[i].cost > sceneController.totalStars)
            {
                RewardPirceImage.sprite = objectcollection.objects[i-1].piercingSprite;

                break;
            }
        }
        
        TaptoStartPanel.SetActive(false);
        StartCoroutine(RewardedAdTimer());
    }
    IEnumerator RewardedAdTimer()
    {
        yield return WAITTHIRTY;
        watchVideoAdButton.interactable = true;
    }
    void ClaimVideoReward()
    {
        //sceneController.AddStars(3);
        claimRewardpanel.SetActive(false);
        int unlockindex;
        bool unlocknew = sceneController.AddStarsAndCheckIfNewUnlocked(3, objectcollection, out unlockindex);
        if (unlocknew)
        {
            ActivateRewardPanel(unlockindex-1);
        }
        //nextLevelPanel.GetComponent<UIStageCompletePanelManager>().SetStageResult(3, objectcollection.objects[unlockindex].cost, objectcollection.objects[unlockindex - 1].cost, unlockindex != -1 ? objectcollection.objects[unlockindex].piercingSprite : RewardPirceImage.sprite);
        
        if (isHomescene)
        {
            TaptoStartPanel.SetActive(true);
        }
    }
    void GetBonusStars()
    {
        currentPlacement = "EndOfLevel";
        bonusStarsButton.interactable = false;
        //Portbliss.Ad.AdController.ShowRewardedVideoAd((success) =>
        //{
        //    if (success)
        //    {
        //        VideoAdCompleteEndOfLevel();
        //    }
        //});
    }
    void VideoAdCompleteEndOfLevel()
    {
        int unlockindex;
        //sceneController.AddStars(3);
        bool unlocknew = sceneController.AddStarsAndCheckIfNewUnlocked(3, objectcollection, out unlockindex);
        nextLevelButton.interactable = false;
        nextLevelPanel.GetComponent<UIStageCompletePanelManager>().SetStageResult(3, unlockindex < 0 ? unlockindex : objectcollection.objects[unlockindex].cost, unlockindex < 0 ? unlockindex : objectcollection.objects[unlockindex - 1].cost, unlockindex !=-1? objectcollection.objects[unlockindex].piercingSprite : RewardPirceImage.sprite,()=> {
            if (unlocknew)
            {
                ActivateRewardPanel(unlockindex - 1);
            }
            nextLevelButton.interactable = true;
        });
    }
    public void SetInventory(int stars)
    {
        totalStarText.text = stars.ToString();
    }
    public void ShowStepPanel()
    {
        gameStepPanel.SetActive(true);
    }
    public void RemoveStepPanel()
    {
        gameStepPanel.SetActive(false);
    }

    public void ResetAfterMiss()
    {
        
        StartCoroutine(ResetAfterMissRoutine());
    }
    IEnumerator ResetAfterMissRoutine()
    {
        retryPanel.SetActive(true);
        sceneController.PlayMissParticle(customer.GetCurrentFakeBone().GetPiercingSlot().GetTargetParticle().position);
        yield return WAITONE;
        retryPanel.SetActive(false);
        //yield return WAITONE;
        //sceneController.PlayMissParticle(customer.GetCurrentPiercingSlot().targetPointer.transform.position);
    }
    public void TapToStartPlay(bool isManualLevel, int _level)
    {
        customer.SetPiercingSlot(sceneController.levelCount);
        customer.GoAndSit(isManualLevel, _level);
        TaptoStartPanel.SetActive(false);
        isHomescene = false;
        SetGameOngoingStatus(true);
    }
    public void TurnOffRingCollectionPanel()
    {
        ringCollectionController.TurnOffPanel();
    }
    public bool IsHomeScene()
    {
        return isHomescene;
    }
    public void EnablePiercingSelectionPanel() {
        sceneController.inputController.DisableInput();
        //selectPiercingPanel.SetActive(true);
        //selectPiercingPanelController.UpdateGameProgress();
        pierceselection3D.TurnOnPanel(false);
        if (!sceneController.HandTutorialShown())
        {
            pierceselection3D.Controltutorial(true);
        }
    }
    public void DisablePiercingSelectionPanel()
    {
        selectPiercingPanel.SetActive(false);
        pierceselection3D.TurnOffPanel();

    }

public void PainMeterRunning(bool _running)
    {
        painMeter.PainMerterRunning(_running);
    }
    public void PainMeterVisible(bool visible)
    {
        painMeter.gameObject.SetActive(visible);
    }
    public void PainMeterInLove()
    {
        painMeter.InLove();
    }
    void TapToStartPiercing()
    {
        EnablePiercingSelectionPanel();
        StartPiercingButtonStatus(false);
    }
    public void StartPiercingButtonStatus(bool isOn)
    {
        TapToStartPiercingButton.gameObject.SetActive(isOn);
    }

    public void TutorialPanelStatus(bool turnOn)
    {
        tutorialPanel.SetActive(turnOn);
    }
    
}
