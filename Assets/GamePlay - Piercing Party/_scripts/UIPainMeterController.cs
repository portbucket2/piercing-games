﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPainMeterController : MonoBehaviour
{
    public Image emojiImage;
    public Image fillImage;

    public float fillSpeed;

    public Sprite happySprite;
    public Sprite inLoveSprite;
    public Sprite sadSprite;
    public Sprite angrySprite;

    public Color happyColor;
    public Color sadColor;
    public Color angryColor;

    private Color currentColor = Color.green;

    public bool isFilling = false;
    private float fillValue = 0;
    private bool isInLove = false;
    

    // Update is called once per frame
    void Update()
    {
        if (isFilling)
        {
            isInLove = false;
            fillValue += Time.deltaTime * fillSpeed;
            if (fillValue>1){ fillValue = 1f;  LevelFailed(); }
            fillImage.fillAmount = fillValue;
            if (fillValue > 0f && fillValue <=0.13f)
            {
                SetHappyFace();
            }else if (fillValue > 0.13f && fillValue <= 0.66f)
            {
                SetSadFace();
            }
            else if (fillValue > 0.66f )
            {
                SetAngryFace();
            }
            fillImage.color = currentColor;
        }
        else
        {
            fillValue -= Time.deltaTime * fillSpeed;
            if (fillValue < 0) { fillValue = 0f; }
            fillImage.fillAmount = fillValue;
            if (!isInLove)
            {
                if (fillValue > 0f && fillValue <= 0.13f)
                {
                    SetHappyFace();
                }
                else if (fillValue > 0.13f && fillValue <= 0.66f)
                {
                    SetSadFace();
                }
                else if (fillValue > 0.66f)
                {
                    SetAngryFace();
                }
            }
            else
            {
                SetInLoveFace();
            }
            fillImage.color = currentColor;
        }
    }

    void SetHappyFace() { emojiImage.sprite = happySprite; currentColor = happyColor; }
    void SetSadFace() { emojiImage.sprite = sadSprite; currentColor = sadColor; }
    void SetAngryFace() { emojiImage.sprite = angrySprite; currentColor = angryColor; }
    void SetInLoveFace() { emojiImage.sprite = inLoveSprite; currentColor = happyColor; }

    void LevelFailed()
    {
        Debug.Log("level failed");
    }

    public void PainMerterRunning(bool running)
    {
        isFilling = running;
    }
    public void InLove()
    {
        isFilling = false;
        isInLove = true;
        SetInLoveFace();
    }
}
