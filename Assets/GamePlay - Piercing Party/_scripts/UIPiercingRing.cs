﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPiercingRing : MonoBehaviour
{
    public bool onlyForShow = false;
    public Image buttonImage;
    public Image ringImage;
    public Image genderImage;
    public Image positionImage;
    [Header("Required sprite")]
    public Sprite maleSprite;
    public Sprite feMaleSprite;
    public Sprite noseSprite;
    public Sprite earSprite;
    public Sprite lipsSprite;
    public Sprite browSprite;
    public Sprite bellySprite;
    public Sprite lockedSprite;
    public Sprite unLockedSprite;
    public Sprite currentLevelSprite;

    [Header ("Internal value autoFetch")]
    public bool islocked = true;
    public int levelIndex;
    public int cost;

    private Button button;

    UIRingCollectionPanelController ringCollectionController;
    SceneController sceneController;
    private void Awake()
    {
        sceneController = SceneController.GetController();
        button = GetComponent<Button>();
    }
    void Start()
    {
        button.onClick.AddListener(delegate {
            StageSelect();
	    });

    }

    void CheckLockState()
    {
        int starCount = 0;
        starCount = sceneController.totalStars; // get total stars
        if (starCount >= cost)
            islocked = false;
        else
            islocked = true;

        
        //genderImage.gameObject.SetActive(!islocked);

        if (islocked)
        {
            buttonImage.sprite = lockedSprite;
            button.interactable = false;

            ringImage.gameObject.SetActive(false);
            positionImage.gameObject.SetActive(false);
        }
        else
        {
            buttonImage.sprite = unLockedSprite;
            if (onlyForShow)
                button.interactable = false;
            else
                button.interactable = true;
            ringImage.gameObject.SetActive(true);
            positionImage.gameObject.SetActive(true);
        }

        //if (levelIndex == sceneController.levelCount) 
        //{
        //    buttonImage.sprite = currentLevelSprite;
        //}

    }

    public void SetPierceBoxValue(int _levelIndex, int _cost, Sprite _ringImage, bool _isMale, PiercingPositions _position, UIRingCollectionPanelController controller)
    {
        ringCollectionController = controller;
        levelIndex = _levelIndex;
        cost = _cost;
        ringImage.sprite = _ringImage;
        SetGenderImage(_isMale);
        SetPositionImage(_position);
        CheckLockState();
    }

    void SetGenderImage(bool isMale)
    {
        if (isMale)
            genderImage.sprite = maleSprite;
        else
            genderImage.sprite = feMaleSprite;
    }
    void SetPositionImage(PiercingPositions pos)
    {
        if(pos == PiercingPositions.EAR)
        {
            positionImage.sprite = earSprite;
        }
        else if (pos == PiercingPositions.NOSE)
        {
            positionImage.sprite = noseSprite;
        }
        else if (pos == PiercingPositions.LIPS)
        {
            positionImage.sprite = lipsSprite;
        }
        else if (pos == PiercingPositions.BROW)
        {
            positionImage.sprite = browSprite;
        }
        else if (pos == PiercingPositions.BULL)
        {
            positionImage.sprite = noseSprite;
        }
        else
        {
            positionImage.sprite = earSprite;
        }
    }
    void StageSelect()
    {
        ringCollectionController.RingSelectFromMenu(levelIndex);
    }
}
