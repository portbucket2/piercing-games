﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPiercingRing3D : MonoBehaviour
{
    public bool onlyForShow = false;
    //public Image buttonImage;
    //public Image ringImage;
    //public Image genderImage;
    public Transform canvas;
    public GameObject ringmodelparent;
    public Image positionImage;
    [Header("Required sprite")]
    //public Sprite maleSprite;
    //public Sprite feMaleSprite;
    public Sprite noseSprite;
    public Sprite earSprite;
    public Sprite lipsSprite;
    public Sprite browSprite;
    public Sprite bellySprite;
    //public Sprite lockedSprite;
    //public Sprite unLockedSprite;
    //public Sprite currentLevelSprite;

    [Header ("Internal value autoFetch")]
    public bool islocked = true;
    public int levelIndex;
    public int cost;

    private Collider thiscollider;

    UIRingCollectionPanelController3D ringCollectionController;
    SceneController sceneController;
    private void Awake()
    {
        sceneController = SceneController.GetController();
        thiscollider = GetComponent<Collider>();
    }
    void Start()
    {
        canvas.rotation = transform.parent.parent.rotation;

    }

    void CheckLockState()
    {
        int starCount = 0;
        starCount = sceneController.totalStars; // get total stars
        if (starCount >= cost)
            islocked = false;
        else
            islocked = true;

        
        //genderImage.gameObject.SetActive(!islocked);
        ///*
        if (islocked)
        {
            thiscollider.enabled = false;

            ringmodelparent.SetActive(false);
            positionImage.gameObject.SetActive(false);
        }
        else
        {
            if (onlyForShow)
                thiscollider.enabled = false;
            else
                thiscollider.enabled = true;
            ringmodelparent.SetActive(true);
            positionImage.gameObject.SetActive(true);
        }
        //*/

        //if (levelIndex == sceneController.levelCount) 
        //{
        //    buttonImage.sprite = currentLevelSprite;
        //}

    }

    public void SetPierceBoxValue(int _levelIndex, int _cost,GameObject modelprefab, PiercingPositions _position, UIRingCollectionPanelController3D controller, bool onlyShow)
    {
        onlyForShow = onlyShow;
        ringCollectionController = controller;
        levelIndex = _levelIndex;
        cost = _cost;
        for (int i = 0; i < ringmodelparent.transform.childCount; i++)
        {
            GameObject temp = ringmodelparent.transform.GetChild(i).gameObject;
            Destroy(temp) ;
        }
        GameObject GO = Instantiate(modelprefab, ringmodelparent.transform) ;
        if (GO)
        {
            Debug.Log("Succeded instantiation of " + GO.name);
            foreach (Rigidbody rbd in GO.GetComponentsInChildren<Rigidbody>())
            {
                rbd.isKinematic = true;
            }
            //GO.transform.SetParent(ringmodelparent.transform);
            //GO.transform.localPosition = Vector3.zero+0.002f*Vector3.forward;
            //GO.transform.localRotation = Quaternion.identity;
            GO.transform.localScale *= 20;
        }
        //else Debug.Log("Failed instantiation of " + modelprefab.name);
        SetPositionImage(_position);
        CheckLockState();
    }

    
    void SetPositionImage(PiercingPositions pos)
    {
        if(pos == PiercingPositions.EAR)
        {
            positionImage.sprite = earSprite;
        }
        else if (pos == PiercingPositions.NOSE)
        {
            positionImage.sprite = noseSprite;
        }
        else if (pos == PiercingPositions.LIPS)
        {
            positionImage.sprite = lipsSprite;
        }
        else if (pos == PiercingPositions.BROW)
        {
            positionImage.sprite = browSprite;
        }
        else if (pos == PiercingPositions.BULL)
        {
            positionImage.sprite = noseSprite;
        }
        else
        {
            positionImage.sprite = earSprite;
        }
    }
    public void StageSelect()
    {
        Debug.Log("selected stage");
        ringCollectionController.RingSelectFromMenu(levelIndex);
    }
}
