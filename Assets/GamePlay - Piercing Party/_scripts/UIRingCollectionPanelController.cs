﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIRingCollectionPanelController : MonoBehaviour
{
    public ObjectCollectionHuda data;
    public UIController uiController;
    public Button closeButton;
    public Button nextButton;
    public Button previousButton;
    public Slider nextPiercingSlider;

    public GameObject page0;
    public GameObject page1;
    public GameObject page2;

    public GameObject indicator0;
    public GameObject indicator1;
    public GameObject indicator2;
    public Slider slider;
    public TextMeshProUGUI nextValueText;
    public TextMeshProUGUI previousTargetText;
    public TextMeshProUGUI nextTargetText;

    public List<UIPiercingRing> piercingRings;

    public RectTransform Fillbar;

    private int pageCount = 0;
    SceneController sceneController;
    private void Awake()
    {
        sceneController = SceneController.GetController();
    }

    void Start()
    {
        ButtonCollection();
        CloseAllPages();
        TurnOnPage(0);
    }


    void SetPiercingRingBox()
    {
        int max = data.objects.Count;
        if (piercingRings.Count != max)
            Debug.LogError("problem, data lost!!!");
        List<ObjectName> objs = data.objects; 

        for (int i = 0; i < max; i++)
        {
            piercingRings[i].SetPierceBoxValue(objs[i].levelIndex, objs[i].cost, objs[i].piercingSprite, objs[i].ismale, objs[i].position, this);
        }
    }

    void ButtonCollection()
    {
        closeButton.onClick.AddListener(delegate {
            TurnOffPanel();
	    });
        nextButton.onClick.AddListener(delegate {
            ChangePage(true);
        });
        previousButton.onClick.AddListener(delegate {
            ChangePage(false);
        });
    }
    public void ChangePage(bool forward)
    {
        CloseAllPages();
        pageCount +=forward?1:-1;
        if (forward)
        {
            if (pageCount > 2)
                pageCount = 0;
        }
        else
        {
            if (pageCount < 0)
                pageCount = 2;
        }
        

        TurnOnPage(pageCount);
    }
    void CloseAllPages()
    {
        page0.SetActive(false);
        page1.SetActive(false);
        page2.SetActive(false);
        
        indicator0.SetActive(false);
        indicator1.SetActive(false);
        indicator2.SetActive(false);
    }
    void TurnOnPage(int count)
    {
        if (count == 0)
        {
            page0.SetActive(true);
            indicator0.SetActive(true);
        }
        else if (count == 1)
        {
            page1.SetActive(true);
            indicator1.SetActive(true);
        }
        else if (count == 2)
        {
            page2.SetActive(true);
            indicator2.SetActive(true);
        }
    }
    public List<UIPiercingRing> GetPiercingRingsList()
    {
        return piercingRings;
    }
    public void UpdateGameProgress()
    {
        SetPiercingRingBox();
        SetSlider();
    }

    void SetSlider()
    {
        int costNext = sceneController.GetNextLevelCost();
        int totalstars = sceneController.totalStars;
        int leftVal = GetPreviousTargetValue();

        slider.minValue = leftVal;
        slider.maxValue = costNext;
        slider.value = totalstars;
        fillbarfill(Mathf.Clamp01((totalstars - leftVal) / (costNext - leftVal)));
        nextValueText.text = totalstars + "/" + costNext;

        previousTargetText.text = leftVal.ToString();
        nextTargetText.text = costNext.ToString();
    }
    public void RingSelectFromMenu(int level)
    {
        sceneController.RingSelectFromMenu(level);
    }
    public void TurnOffPanel()
    {
        uiController.TurnOnHomePanel();
        gameObject.SetActive(false);
        sceneController.inputController.EnableInput();
    }
    int GetPreviousTargetValue()
    {
        int temp = 0;
        int totalStars = sceneController.totalStars;
        if (totalStars < 21)
            temp = 0;
        else if (totalStars >= 21 && totalStars < 42)
            temp = 21;
        else if (totalStars >= 42 && totalStars < 63)
            temp = 42;
        else
            temp = 42;
        return temp;
    }

    void fillbarfill(float fraction)
    {
        Fillbar.offsetMax = new Vector2(-(1 - fraction) * Fillbar.parent.GetComponent<RectTransform>().sizeDelta.x, 0);
    }
}
