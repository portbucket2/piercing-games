﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIRingCollectionPanelController3D : MonoBehaviour
{
    public ObjectCollectionHuda data;
    public UIController uiController;
    public Button closeButton;

    public GameObject OncreenUIPart;
    public GameObject TutorialGameobject;

    public GameObject indicator0;
    public GameObject indicator1;
    public GameObject indicator2;
    public TextMeshProUGUI nextValueText;
    public TextMeshProUGUI previousTargetText;
    public TextMeshProUGUI nextTargetText;

    public List<UIPiercingRing3D> piercingRings;

    public RectTransform Fillbar;
    public float boxdistance = 1;
    bool panelmoving = false;
    public float movetime = 0.5f;
    public int curpos;
    int poscount;

    private int pageCount = 0;
    SceneController sceneController;
    private void Awake()
    {
        sceneController = SceneController.GetController();
        curpos = 0;
        poscount = transform.childCount;
        for (int i = 0; i < poscount; i++)
        {
            transform.GetChild(i).localPosition = new Vector3(i*boxdistance,transform.GetChild(i).localPosition.y, transform.GetChild(i).localPosition.z);
        }
    }

    void Start()
    {
        ButtonCollection();
    }

    private void OnEnable()
    {
        
    }

    public void Controltutorial(bool show)
    {
        TutorialGameobject.SetActive(show);
    }


    void SetPiercingRingBox(bool forShow)
    {
        int max = data.objects.Count;
        if (piercingRings.Count != max)
            Debug.LogError("problem, data lost!!!");
        List<ObjectName> objs = data.objects; 

        for (int i = 0; i < max; i++)
        {
            piercingRings[i].SetPierceBoxValue(objs[i].levelIndex, objs[i].cost, objs[i].piercingRingPrefab,  objs[i].position, this, forShow);
        }
    }

    public void ChangeBox(bool forward)
    {
        
        if (!panelmoving) StartCoroutine(Movepanel(forward));
    }

    

    IEnumerator Movepanel(bool forward)
    {
        panelmoving = true;

        float t = 0;

        float pos = - curpos * boxdistance;
        transform.localPosition = new Vector3(pos, transform.localPosition.y, transform.localPosition.z);

        bool flag = false;
        while (t < movetime)
        {
            transform.localPosition = new Vector3(pos+(forward?-1:1)*(t/movetime)*boxdistance, transform.localPosition.y, transform.localPosition.z);

            if (!flag)
            {
                if (t > movetime / 2)
                {
                    flag = true;

                    if (curpos == 0 && !forward)
                    {
                        curpos = poscount ;
                        pos = -curpos * boxdistance;
                    }

                    if (curpos == (poscount - 1) && forward)
                    {
                        curpos = -1;
                        pos = -curpos * boxdistance;
                    }
                }
            }

            t += Time.deltaTime;
            yield return null;
        }

        if (!flag)
        {
            if (t > movetime / 2)
            {
                flag = true;

                if (curpos == 0 && !forward)
                {
                    curpos = poscount;
                    pos = -curpos * boxdistance;
                }

                if (curpos == (poscount - 1) && forward)
                {
                    curpos = -1;
                    pos = -curpos * boxdistance;
                }
            }
        }
        curpos += forward ? 1 : -1;
        pos = -curpos * boxdistance;
        transform.localPosition = new Vector3(pos , transform.localPosition.y, transform.localPosition.z);

        indicator0.SetActive(curpos == 0);
        indicator1.SetActive(curpos == 1);
        indicator2.SetActive(curpos == 2);

        panelmoving = false;

    } 

    void ButtonCollection()
    {
        closeButton.onClick.AddListener(delegate {
            TurnOffPanel();
	    });
        
    }
    
    
    
    public List<UIPiercingRing3D> GetPiercingRingsList()
    {
        return piercingRings;
    }
    public void UpdateGameProgress(bool forShow)
    {
        SetPiercingRingBox(forShow);
        SetSlider();
    }

    void SetSlider()
    {
        int costNext = sceneController.GetNextLevelCost();
        int totalstars = sceneController.totalStars;
        int leftVal = GetPreviousTargetValue();

        fillbarfill(Mathf.Clamp01((totalstars - leftVal) / (costNext - leftVal)));
        nextValueText.text = totalstars + "/" + costNext;

        previousTargetText.text = leftVal.ToString();
        nextTargetText.text = costNext.ToString();
    }
    public void RingSelectFromMenu(int level)
    {
        sceneController.RingSelectFromMenu(level);
    }
    public void TurnOffPanel()
    {
        uiController.TurnOnHomePanel();
        uiController.PainmeterActivationControls(true);
        gameObject.SetActive(false);
        OncreenUIPart.SetActive(false);
        sceneController.inputController.EnableInput();
    }
    public void TurnOnPanel(bool fullcontrol)
    {
        uiController.PainmeterActivationControls(false);
        closeButton.gameObject.SetActive(fullcontrol);
        gameObject.SetActive(true);
        OncreenUIPart.SetActive(true);
        UpdateGameProgress(fullcontrol);
    }
    int GetPreviousTargetValue()
    {
        int temp = 0;
        int totalStars = sceneController.totalStars;
        if (totalStars < 21)
            temp = 0;
        else if (totalStars >= 21 && totalStars < 42)
            temp = 21;
        else if (totalStars >= 42 && totalStars < 63)
            temp = 42;
        else
            temp = 42;
        return temp;
    }

    void fillbarfill(float fraction)
    {
        Fillbar.offsetMax = new Vector2(-(1 - fraction) * Fillbar.parent.GetComponent<RectTransform>().sizeDelta.x, 0);
    }
}
