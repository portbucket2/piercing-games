﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIStageCompletePanelManager : MonoBehaviour
{
    public Slider slider;
    public RectTransform Fillbar;
    public TextMeshProUGUI nextValueText;
    public TextMeshProUGUI feedbackText;
    public TextMeshProUGUI previousTargetText;
    public TextMeshProUGUI nextTargetText;
    public GameObject star1;
    public GameObject star2;
    public GameObject star3;
    public float fillSpeed = 2f;

    public Image nextpierce;

    readonly string BAD = "NICE"; 
    readonly string GOOD = "GREAT"; 
    readonly string PERFECT = "PERFECT";

    SceneController sceneController;
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    private void Awake()
    {
        sceneController = SceneController.GetController();
    }

    void Start()
    {
        
    }

    public void SetStageResult(int result,int nextcost,int prevcost,Sprite pierceimage,System.Action delayedaction)
    {
        nextpierce.sprite = pierceimage;

        int costNext = nextcost;//sceneController.GetNextLevelCost(result) ;
        int totalstars = sceneController.totalStars;// + result;
        int leftVal = prevcost;//GetPreviousTargetValue();

        slider.minValue = leftVal;
        slider.maxValue = costNext;
        StartCoroutine(SliderAnimate(totalstars));
        StartCoroutine(FillBarAnimate(Mathf.Clamp01(((float)(totalstars - leftVal)) / (costNext - leftVal))));
        nextValueText.text = totalstars + "/" + costNext;
        if (prevcost < 0 || nextcost < 0) nextValueText.text= "MAX";
        previousTargetText.text = leftVal.ToString();
        nextTargetText.text = costNext.ToString();

        StartCoroutine(popstars(result, delayedaction));
        if (result == 1)
        {
            feedbackText.text = BAD;
        }else if (result == 2)
        {
            feedbackText.text = GOOD;
        }
        else if (result == 3)
        {
            feedbackText.text = PERFECT;
        }
    }
    public float starscalexcess = 0.25f;
    public float starpoptransitiontime = 1;
    IEnumerator StarPop(int starid)
    {
        Transform star = starid == 1 ? star1.transform : (starid == 2 ? star2.transform : star3.transform);

        star.localScale = Vector3.zero;

        float scalefactor = Mathf.Sqrt(1 + (1 / starscalexcess)) - 1;

        float actualtime = ((2 + 2 * scalefactor) / (2 + scalefactor)) * starpoptransitiontime;
        float actualscale = 1 + starscalexcess;

        float time = 0;

        while (time < starpoptransitiontime)
        {
            float scale = (actualscale / (actualtime * actualtime)) * 4 * time * (actualtime - time);
            star.localScale = scale * Vector3.one;
            time += Time.deltaTime;
            yield return null;
        }

        star.localScale =  Vector3.one;
    }

    IEnumerator popstars(int num, System.Action afterfillup)
    {
        int startnum = 1;
        star1.SetActive(false);
        star2.SetActive(false);
        star3.SetActive(false);
        while (startnum <= num)
        {
            GameObject star = startnum == 1 ? star1 : (startnum == 2 ? star2 : star3);

            star.SetActive(true);

            yield return StartCoroutine(StarPop(startnum));

            startnum++;
        }
        afterfillup?.Invoke();
    }

    IEnumerator SliderAnimate(float val)
    {
        float step = 0;
        float temp = 0;
        slider.value = 0;
        while (step < 1)
        {
            temp = Mathf.Lerp(slider.value, val, step);
            step += Time.deltaTime * fillSpeed;
            slider.value = temp;
            yield return ENDOFFRAME;
        }
        slider.value = val;
    }

    IEnumerator FillBarAnimate(float val)
    {
        float step = 0;
        //float temp = 0;
        //slider.value = 0;
        while (step < val)
        {
            //temp = Mathf.Lerp(slider.value, val, step);
            step += Time.deltaTime * fillSpeed;
            //slider.value = temp;
            fillbarfill(step);
            yield return ENDOFFRAME;
        }
        //slider.value = val;
        fillbarfill(val);
        
    }
    void fillbarfill(float fraction)
    {
        Fillbar.offsetMax =new Vector2( -(1 - fraction) * Fillbar.parent.GetComponent<RectTransform>().sizeDelta.x,0);
    }
    int GetPreviousTargetValue()
    {
        int temp = 0;
        int totalStars = sceneController.totalStars;
        if (totalStars < 21)
            temp = 0;
        else if (totalStars >= 21 && totalStars < 42)
            temp = 21;
        else if (totalStars >= 42 && totalStars < 63)
            temp = 42;
        else
            temp = 42;
        return temp;
    }
}
