﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellyVertex 
{
    public int vertexIndex;
    public Vector3 initialVertextPosition;
    public Vector3 currentVertextPosition;

    public Vector3 currentVelocity;

    public JellyVertex(int _vertexIndex, Vector3 _initialVertextPosition,Vector3 _currentVertextPosition,Vector3 _currentVelocity)
    {
        vertexIndex = _vertexIndex;
        initialVertextPosition = _initialVertextPosition;
        currentVertextPosition = _currentVertextPosition;
        currentVelocity = _currentVelocity;
    }

    public Vector3 GetCurrentDisplacement()
    {
        return currentVertextPosition - initialVertextPosition;
    }

    public void UpdateVelocity(float _bounceSpeed)
    {
        currentVelocity = currentVelocity - GetCurrentDisplacement() * _bounceSpeed * Time.deltaTime;
    }

    public void Settle(float _stiffness)
    {
        currentVelocity *= 1f - _stiffness * Time.deltaTime;
    }
    public void ApplyPressureToVertex(Transform _transform, Vector3 _position, float _pressure)
    {
        Vector3 distanceVerticePoint = currentVertextPosition - _transform.InverseTransformPoint(_position);
        float adaptedPressure = _pressure / (1f + distanceVerticePoint.sqrMagnitude);
        float velocity = adaptedPressure * Time.deltaTime;
        currentVelocity += -distanceVerticePoint.normalized * velocity;
    }
    public Vector3 GetCurrentVertexVelocity()
    {
        return currentVelocity;
    }
}
