﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jellyfier : MonoBehaviour
{
    public Camera mainCamera;

    public float bounceSpeed;
    public float fallForce;
    public float stiffness;

    private MeshFilter meshFilter;
    private Mesh mesh;

    JellyVertex[] jellyVertices;
    Vector3[] currentMeshVertices;

    private void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        mesh = meshFilter.mesh;

        GetVertices();
    }


    void Update()
    {
        //mouse click on object check
        if (Input.GetMouseButtonUp(0))
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.DrawLine(ray.origin, hit.point);
                ApplyPressureToPoint(hit.point, fallForce);
            }
                
        }
        //---------------------------
        UpdateVertices();
    }
    private void OnCollisionEnter(Collision collision)
    {
        ContactPoint[] collisionPoints = collision.contacts;
        for (int i = 0; i < collisionPoints.Length; i++)
        {
            Vector3 inputPoint = collisionPoints[i].point + (collisionPoints[i].point * 0.1f);
            ApplyPressureToPoint(inputPoint, fallForce);
        }
    }
    public void ApplyPressureToPoint(Vector3 _point, float _pressure)
    {
        int max = jellyVertices.Length;
        for (int i = 0; i < max; i++)
        {
            jellyVertices[i].ApplyPressureToVertex(transform, _point, _pressure);
        }
    }
    private void GetVertices()
    {
        int max = mesh.vertices.Length;
        jellyVertices = new JellyVertex[max];
        currentMeshVertices = new Vector3[max];
        for (int i = 0; i < max; i++)
        {
            jellyVertices[i] = new JellyVertex(i,mesh.vertices[i],mesh.vertices[i],Vector3.zero);
            currentMeshVertices[i] = mesh.vertices[i];
        }

    }

    private void UpdateVertices()
    {
        int max = jellyVertices.Length;
        for (int i = 0; i < max; i++)
        {
            jellyVertices[i].UpdateVelocity(bounceSpeed);
            jellyVertices[i].Settle(stiffness);

            jellyVertices[i].currentVertextPosition += jellyVertices[i].currentVelocity * Time.deltaTime;
            currentMeshVertices[i] = jellyVertices[i].currentVertextPosition;
        }

        mesh.vertices = currentMeshVertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }
    public JellyVertex GetOneJellyVertex()
    {
        return jellyVertices[1];
    }
}
