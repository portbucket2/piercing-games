﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerControllerGemEx : MonoBehaviour
{
    public Transform hammer;
    public ParticleSystem popParticle;
    private float moveTime = 0.12f;
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    Vector3 hammerStartRot;
    private Vector3 targetPos;
    void Start()
    {
        hammerStartRot = hammer.eulerAngles;
    }
    public void HammerFollow(Vector3 pos)
    {
        transform.position = pos;
    }
    public void HammerFall(Vector3 target)
    {
        StopAllCoroutines();
        targetPos = target;
        StartCoroutine(FallRoutine());
    }
    IEnumerator FallRoutine()
    {
        float timeSpan = moveTime;
        float timeElapsed = 0;
        Vector3 startPos = transform.position;
        Vector3 hammerEndRot = new Vector3(-75, hammer.eulerAngles.y, hammer.eulerAngles.z);
        float xAngle = 0;
        while (timeElapsed < timeSpan)
        {
            transform.position = Vector3.Lerp(startPos, targetPos, Mathf.Pow((timeElapsed / timeSpan), 0.28f));
            xAngle = Mathf.Lerp(0, -75, Mathf.Pow((timeElapsed / timeSpan), 0.28f));
            hammer.eulerAngles = new Vector3(xAngle, hammer.eulerAngles.y, hammer.eulerAngles.z);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        popParticle.Play();
        yield return ENDOFFRAME;

        float step = 0;
        while (step < 1)
        {
            xAngle = Mathf.Lerp(-75, 0, step);
            hammer.eulerAngles = new Vector3(xAngle, hammer.eulerAngles.y, hammer.eulerAngles.z);
            step += Time.deltaTime*2f;
            yield return null;
        }
        yield return ENDOFFRAME;
    }
}
