﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputControllerGemEx : MonoBehaviour
{
    public bool isInputEnabled = true;
    public Camera mainCamera;
    public HammerControllerGemEx hammerController;
    private Vector3 rayAddition;
    // Start is called before the first frame update
    void Start()
    {
        float temp = Screen.width / 10;
        Debug.Log("screen width: " + temp);
        rayAddition = new Vector3(0f, temp, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (isInputEnabled)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                //RaycastHit hit;
                //if (Physics.Raycast(ray, out hit, 100))
                //{
                    
                //}
            }
            if (Input.GetMouseButton(0))
            {
                //Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition + rayAddition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100))
                {
                    Debug.DrawLine(ray.origin, hit.point, Color.red);
                    hammerController.HammerFollow(hit.point);
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition + rayAddition);
                RaycastHit hit;
                int layerMask = CreateLayerMask(false, 25);
                if (Physics.Raycast(ray, out hit, 100, layerMask))
                {
                    
                    Debug.DrawLine(ray.origin,hit.point,Color.green, 20f);
                    //Debug.Log("input receive: " + hit.transform.name);
                    if (hit.transform.GetComponent<MeshCollider>())
                    {
                        hit.transform.GetComponent<OreFragmentScript>().SeparateThrustes();
                        hammerController.HammerFall(hit.point);
                    }
                    
                }
            }
        }
    }

    public int CreateLayerMask(bool aExclude, params int[] aLayers)
    {
        int v = 0;
        foreach (var L in aLayers)
            v |= 1 << L;
        if (aExclude)
            v = ~v;
        return v;
    }


}
