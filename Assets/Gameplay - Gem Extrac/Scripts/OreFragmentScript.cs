﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreFragmentScript : MonoBehaviour
{
    public Vector3 startRot;
    private OreScripts oreScripts;
    private Rigidbody rb;
    private MeshCollider mc;
    void Awake()
    {
        rb = transform.GetComponent<Rigidbody>();
        mc = transform.GetComponent<MeshCollider>();
        oreScripts = transform.parent.GetComponent<OreScripts>();
    }
    

    public void SeparateThrustes()
    {
        rb.isKinematic = false;
        mc.isTrigger = false;
        transform.parent = null;

        oreScripts.UpdateHealth();
    }
}
