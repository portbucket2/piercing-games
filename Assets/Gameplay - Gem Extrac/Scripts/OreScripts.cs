﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreScripts : MonoBehaviour
{
    public float health = 1;
    [Range(0, 1)]
    public float minimumHealth = 0.2f;
    public OreFragmentScript gem;

    private float childCount = 0;
    private float childCountTotal;

    SceneControllerGemEx sceneController;

    void Start()
    {
        childCountTotal = transform.childCount;
        sceneController = SceneControllerGemEx.getController();
    }

    // Update is called once per frame
    public void UpdateHealth()
    {
        childCount = transform.childCount;
        float temp = childCount / childCountTotal;
        health = temp;
        if (health < minimumHealth)
        {
            Debug.Log("show Gem");

            for (int i = 0; i < childCount; i++)
            {
                transform.GetChild(i).GetComponent<OreFragmentScript>().SeparateThrustes();
            }
            //gem.SeparateThrustes();
            sceneController.EndLevel();
        }
    }
}
