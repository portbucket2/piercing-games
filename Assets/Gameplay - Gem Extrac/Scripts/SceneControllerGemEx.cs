﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneControllerGemEx : MonoBehaviour
{
    public static SceneControllerGemEx _sceneController;
    public int levelCount = 0;
    public OreScripts currentOre;
    public Camera mainCamera;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    private void Awake()
    {
        _sceneController = this;
    }
    void Start()
    {
        BaseLoader.GetIterationIndex(ref levelCount);
        StartLevel(levelCount);
    }
    public static SceneControllerGemEx getController()
    {
        return _sceneController;
    }

    void StartLevel(int _level)
    {
        // start level number
        // spwan ore
        // set curent ore as spwaned ore
    }
    public void EndLevel()
    {
        DisplayGemOnCam();
        Invoke("EndLevelAfter", 3f);
    }
    void EndLevelAfter()
    {
        
        BaseLoader.ReportStarCount(3);
        BaseLoader.RequestCompletion(true);
    }
    public void DisplayGemOnCam()
    {
        StartCoroutine(DisplayGemRoutine());
    }
    IEnumerator DisplayGemRoutine()
    {
        Transform gem = currentOre.gem.transform;
        gem.GetComponent<Rigidbody>().isKinematic = true;
        float timeSpan = 1f;
        float timeElapsed = 0;
        Vector3 startPos = gem.position;
        Vector3 targetPos = new Vector3(0, 8.6f, -8.6f);
        Vector3 GemEndRot = new Vector3(0f, 1f, 0f);
        Vector3 startAngle = gem.eulerAngles;
        Vector3 targetAngle = new Vector3(0f, 0f, 90f);

        yield return WAITONE;

        while (timeElapsed < timeSpan)
        {
            gem.position = Vector3.Lerp(startPos, targetPos, Mathf.Pow((timeElapsed / timeSpan), 0.28f));
            gem.eulerAngles = Vector3.Lerp(startAngle, targetAngle, Mathf.Pow((timeElapsed / timeSpan), 0.28f));
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        yield return ENDOFFRAME;
        
        while (true)
        {
            gem.eulerAngles += GemEndRot * Time.deltaTime;
            yield return null;
        }
    }
}
