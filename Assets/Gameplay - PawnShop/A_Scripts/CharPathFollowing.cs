﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharPathFollowing : MonoBehaviour
{
    public GameObject targetPoint;
    public float moveSpeed;
    public bool moving;

    public bool turnRightThief;

    public Vector3 characterRootPos;
    public Quaternion charRootRot;
    public GameObject initialTarget;
    // Start is called before the first frame update
    void Start()
    {
        turnRightThief = false;
        characterRootPos = transform.position;
        charRootRot = transform.rotation;
        //initialTarget = targetPoint;
    }

    // Update is called once per frame
    void Update()
    {
        if (targetPoint && moving)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPoint.transform.position, Time.deltaTime * moveSpeed);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetPoint.transform.rotation, Time.deltaTime * moveSpeed*50);
        }

        if (turnRightThief)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetPoint.transform.rotation, Time.deltaTime * moveSpeed * 50);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "pathPoint")
        {
            if (other.GetComponent<PathPoint>().NextPathPoint)
            {
                targetPoint = other.GetComponent<PathPoint>().NextPathPoint.gameObject;
            }
            
        }

        if (other.tag == "charStation")
        {

            GameStatesControl.instance.ReachedDeskState();
            //if (other.GetComponent<PathPoint>().NextPathPoint)
            //{
            //    
            //    //targetPoint = other.GetComponent<PathPoint>().NextPathPoint.gameObject;
            //}

        }

        if(other.tag == "exit")
        {
            //GetBackCharToRoot();
        }
    }

    public void GetBackCharToRoot()
    {
        //moving = false;
        transform.localPosition = Vector3.zero;
        transform.rotation = charRootRot;

        targetPoint = initialTarget;

        GetComponent<CharacterAnimController>().ResetItemOnHand();

        
    }
}
