﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollarBills : MonoBehaviour
{
    public static DollarBills instance;
    public Transform dollarTar;
    public Transform ScannerPos;
    public Transform RootPos;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (dollarTar)
        {
            transform.position = Vector3.MoveTowards(transform.position, dollarTar.position, Time.deltaTime*5);
            transform.rotation = Quaternion.Lerp(transform.rotation, dollarTar.rotation, Time.deltaTime * 30);

            if(transform.position == dollarTar.position)
            {
                transform.rotation=  dollarTar.rotation;
                transform.SetParent(dollarTar);

                dollarTar = null;
                
            }
        }
    }

    public void DollarForSellerOnScanner()
    {
        dollarTar = ScannerPos;
        transform.SetParent(ScannerAttributes.instance.gameObject.transform);
        DollarOnHandDelayed();
    }
    public void DollarForMeOnScanner()
    {
        dollarTar = ScannerPos;
        transform.SetParent(ScannerAttributes.instance.gameObject.transform);
        //DollarOnHandDelayed();
        Invoke("DollarBackToRoot", 1f);
    }
    public void DollarBackToRootInstant()
    {
        transform.position = RootPos.position;
        transform.rotation = RootPos.rotation;

        dollarTar = null;
        transform.SetParent(ScannerAttributes.instance.gameObject.transform);
    }
    public void DollarBackToRoot()
    {
        

        dollarTar = RootPos;
        transform.SetParent(ScannerAttributes.instance.gameObject.transform);
    }

    public void DollarIntoHand()
    {
        //dollarTar = hand;
        dollarTar = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().moneyHolder.transform;
    }
    public void DollarIntoHandInstant()
    {
        dollarTar = null;
        Transform hand = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().moneyHolder.transform;
        transform.position = hand.position;
        transform.rotation = hand.rotation;

        transform.SetParent(hand);
    }

    public void DollarOnHandDelayed()
    {
        Invoke("DollarIntoHand", 2f);
    }
}
