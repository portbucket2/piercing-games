﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimControl : MonoBehaviour
{
    Animator doorAnim;
    public static DoorAnimControl instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        doorAnim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenDoor()
    {
        doorAnim.SetTrigger("openDoor");
    }
}
