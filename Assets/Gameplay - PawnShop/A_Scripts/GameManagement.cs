﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement : MonoBehaviour
{
    
    public enum State
    {
        idle, walkToDesk, reachedDesk, scanning , Decision , Bidding , result , selling, soldResult
    }

    public int levelIndex;
    public int dollarsPP;
    public int thiefPPref;

    public State state;

    public static GameManagement instance;

    public ParticleSystem particleDollars;
    public ParticleSystem particleSold;

    
    private void Awake()
    {
        instance = this;

        levelIndex = PlayerPrefs.GetInt("levelIndex", 0);
        dollarsPP = PlayerPrefs.GetInt("dollarsPP", 1000);
        thiefPPref = PlayerPrefs.GetInt("thiefPPref", 0);


    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeStateTo(int s)
    {
        state = (State)s;
        UiManage.instance.GetRightPanel(s);
    }

    public void LevelStarted()
    {
        
        Debug.Log("Level Started" + (levelIndex + 1));
        AnalyticsController.LogEvent_LevelStarted((levelIndex + 1));

        UiManage.instance.UpdateAllValueUi();
    }

    public void LevelAccomplished()
    {
        Debug.Log("Level Accomplished" + (levelIndex + 1));
        AnalyticsController.LogEvent_LevelCompleted((levelIndex + 1));
    }

    public void NextLevel()
    {
        levelIndex += 1;

        PlayerPrefs.SetInt("levelIndex", levelIndex);
    }

    public void DollarsPPSave()
    {
        dollarsPP = AccountsControl.instance.currentBalance;
        PlayerPrefs.SetInt("dollarsPP", dollarsPP);
    }
}
