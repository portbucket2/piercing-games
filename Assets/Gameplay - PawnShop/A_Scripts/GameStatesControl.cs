﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStatesControl : MonoBehaviour
{
    public GameObject currentCharacter;
    

    public bool rejectedCurrent;

    public bool Hasthief;

    public bool BuyerMode;


    //public Text targetDemo;


    
    //public Animator cureentCharAnim;

    public static GameStatesControl instance;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        SellerCharactersMamanger.instance.GetASeller();
        IdleState();
        int levelCount = 0;
        BaseLoader.GetIterationIndex(ref levelCount);

        Invoke("WalkToDeskState",0.5f);

    }

    // Update is called once per frame
    void Update()
    {
        //targetDemo.text = "" + currentCharacter.GetComponent<CharPathFollowing>().targetPoint;
        if (Input.GetKeyDown(KeyCode.A))
        {
            SellItemToBuyer();
        }
    }


    public void IdleState()
    {
        GameManagement.instance.ChangeStateTo(0);
        currentCharacter.GetComponent<CharacterAnimController>().GoIdle();
        ScreenAttributes.instance.AppearPanelIdle();
    }

    public void WalkToDeskState()
    {
        GameManagement.instance.ChangeStateTo(1);
        currentCharacter.GetComponent<CharPathFollowing>().moving = true;
        currentCharacter.GetComponent<CharacterAnimController>().GoWalk();

        ScreenAttributes.instance.AppearPanelIdle();

        //DoorAnimControl.instance.OpenDoor();
        Invoke("OpenDoorGameState", 0.7f);
        //OpenDoorGameState();

        GameManagement.instance.LevelStarted();
    }
    public void ReachedDeskState()
    {
        if (BuyerMode)
        {
            DecisionState();
            currentCharacter.GetComponent<CharPathFollowing>().moving = false;
        }
        else
        {
            GameManagement.instance.ChangeStateTo(2);
            currentCharacter.GetComponent<CharPathFollowing>().moving = false;
            currentCharacter.GetComponent<CharacterAnimController>().ItemOnTable();


            ScannerAttributes.instance.GrabItemDelayed();
        }
        

       
    }

    public void ScanningState()
    {
        GameManagement.instance.ChangeStateTo(3);
    }

    public void DecisionState()
    {
        GameManagement.instance.ChangeStateTo(4);
        currentCharacter.GetComponent<charHUD>().AppearSpeechBubble();
        currentCharacter.GetComponent<CharacterAnimController>().GoSayprice();

        if (Hasthief)
        {
            UiManage.instance.rightLeftUpSwipeAnim.SetBool("thiefMode", true);
        }
        else
        {
            UiManage.instance.rightLeftUpSwipeAnim.SetBool("thiefMode", false);
        }
        
    }

    public void ResultStatePositive()
    {
        if (BuyerMode)
        {
            GetStoaragePanel();
        }
        else
        {
            rejectedCurrent = false;

            UiManage.instance.ResultDealText.text = "GREAT DEAL !!!";
            GameManagement.instance.ChangeStateTo(6);
            currentCharacter.GetComponent<charHUD>().RatingHudEnable();
            currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointRight;

            currentCharacter.GetComponent<CharacterAnimController>().GoHappy();

            GameManagement.instance.particleDollars.Play();

            AccountsControl.instance.SpendBoughtValue();

            ScannerAttributes.instance.BoughtItem = true;
            DollarBills.instance.DollarForSellerOnScanner();
            Invoke("GoAwayChar", 2f);
            //GoAwayChar();

            //Invoke("GetSellingState",3f);
            Invoke("GetStoaragePanel", 3f);

            if (currentCharacter.GetComponent<CharacterAnimController>().Thief)
            {
                WantedBorad.instance.NextThiefPP();
            }
               
        }
        
    }
    public void ResultStatenegative()
    {
        //if (currentCharacter.GetComponent<CharacterAnimController>().Thief)
        //{
        //    
        //
        //    UiManage.instance.ResultDealText.text = "THIEF !!!";
        //    GameManagement.instance.ChangeStateTo(6);
        //    currentCharacter.GetComponent<charHUD>().CloseHud();
        //    currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointLeft;
        //    currentCharacter.GetComponent<CharacterAnimController>().GoArrested();
        //
        //    PoliceLights.instance.EnablePoliceLights();
        //
        //    Invoke("GoAwayThief", 2f);
        //}
        //else
        //{
        //    rejectedCurrent = true;
        //
        //    UiManage.instance.ResultDealText.text = "REJECTED !";
        //    GameManagement.instance.ChangeStateTo(6);
        //    currentCharacter.GetComponent<charHUD>().CloseHud();
        //    currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointLeft;
        //    //GoAwayChar();
        //    currentCharacter.GetComponent<CharacterAnimController>().GoSad();
        //
        //    Invoke("GoAwayChar", 2f);
        //}
        if (currentCharacter.GetComponent<CharacterAnimController>().Thief)
        {
            WantedBorad.instance.NextThiefPP();
        }
        rejectedCurrent = true;

        UiManage.instance.ResultDealText.text = "REJECTED !";
        GameManagement.instance.ChangeStateTo(6);
        currentCharacter.GetComponent<charHUD>().RatingHudReject();
        currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointLeft;
        //GoAwayChar();
        currentCharacter.GetComponent<CharacterAnimController>().GoSad();

        Invoke("GoAwayChar", 2f);

        Invoke("NextSellerButton", 3f);
    }

    public void ResultStateThiefToPolice()
    {
        if (currentCharacter.GetComponent<CharacterAnimController>().Thief)
        {


            UiManage.instance.ResultDealText.text = "THIEF !!!";
            GameManagement.instance.ChangeStateTo(6);
            currentCharacter.GetComponent<charHUD>().CloseHud();
            currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointLeft;
            currentCharacter.GetComponent<CharacterAnimController>().GoArrested();

            PoliceLights.instance.EnablePoliceLights();

            Invoke("GoAwayThief", 2f);

            //Invoke("NextSellerButton", 3f);
            
        }
        
    }


    public void GoAwayChar()
    {
        if (rejectedCurrent)
        {
            currentCharacter.GetComponent<CharacterAnimController>().rejected = true;
        }
        currentCharacter.GetComponent<CharPathFollowing>().moving = true;
        currentCharacter.GetComponent<CharacterAnimController>().GoWalk();

        Debug.Log("Away");
        SellerCharactersMamanger.instance. GetNextSellerIndex();
    }
    public void GoAwayThief()
    {
        
        currentCharacter.GetComponent<CharPathFollowing>().moving = true;
        RewardBoardUi.instance.RewardBoardAppear();

        Debug.Log("thief Away");
        SellerCharactersMamanger.instance.GetNextSellerIndex();
        //currentCharacter.GetComponent<CharacterAnimController>().GoWalk();
    }

    public void GetSellingState()
    {
        //GameManagement.instance.ChangeStateTo(7);

        UiManage.instance.SellingPanelCorrectedEnable();
        SellingPanel.instance.ResetOfferIndex();
        SellingPanel.instance.UpdateSellingPanelValues();

        if(!( StoragePanelManageMent.instance.storageState == StoragePanelManageMent.StorageState.idle))
        {
            //currentCharacter.GetComponent<CharacterAnimController>().itemSold = true;
            ScreenAttributes.instance.AppearPanelIdle();
        }
        

        

        //Destroy(ScannerAttributes.instance.currentItem);

    }

    public void GetSoldState()
    {
        //GameManagement.instance.ChangeStateTo(8);
        UiManage.instance.SellingPanelCorrectedDisable();
        
        if (GameManagement.instance.state == GameManagement.State.result)
        {
            Invoke("NextSellerButton", 1.5f);
        }
        
        //else
        //{
        //    GameManagement.instance.ChangeStateTo(0);
        //}
        AccountsControl.instance.GetProfitIntoBalance();
        GameManagement.instance.particleSold.Play();

    }

    public void NextSellerButton()
    {
        //GameManagement.instance.LevelAccomplished();
        //GameManagement.instance.NextLevel();
        //--------------------------------------------------------------------------------------------------------Level End Position--------------

        BaseLoader.ReportStarCount(3);
        BaseLoader.RequestCompletion(true);

        // do it for normal game cycle

        //SellerCharactersMamanger.instance.GetNextSeller();
        WalkToDeskState();



    }

    public void GetStoaragePanel()
    {
        if (BuyerMode)
        {
            StoragePanelManageMent.instance.storageState = StoragePanelManageMent.StorageState.itemQuery;
            StoragePanelManageMent.instance.TransitionIn();

            //currentCharacter.GetComponent<CharacterAnimController>().itemSold = true;
        }
        else
        {
            StoragePanelManageMent.instance.storageState = StoragePanelManageMent.StorageState.itemAdded;
            StoragePanelManageMent.instance.TransitionIn();

            currentCharacter.GetComponent<CharacterAnimController>().itemSold = true;
        }
        

        ScreenAttributes.instance.AppearPanelIdle();
    }

    public void SellItemToBuyer()
    {
        rejectedCurrent = false;

        UiManage.instance.ResultDealText.text = "GREAT DEAL !!!";
        GameManagement.instance.ChangeStateTo(6);
        currentCharacter.GetComponent<charHUD>().RatingHudEnable();
        currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointRight;

        currentCharacter.GetComponent<CharacterAnimController>().GoHappy();

        GameManagement.instance.particleDollars.Play();

        //AccountsControl.instance.SpendBoughtValue();
        //AccountsControl.instance.GetProfitIntoBalance();
        AccountsControl.instance.BalanceInceaseBy(GameStatesControl.instance.currentCharacter.GetComponent<charHUD>().BuyValueHud);

        //ScannerAttributes.instance.BoughtItem = true;
        Invoke("GoAwayChar", 2.5f);
        Invoke("NextSellerButton", 4f);

        UniversalItem.instance.SendUniversalItemToSeller();

        //GameStatesControl.instance.NextSellerButton();
    }

    public void OpenDoorGameState()
    {
        DoorAnimControl.instance.OpenDoor();
    }
}
