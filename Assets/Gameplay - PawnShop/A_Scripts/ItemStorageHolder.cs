﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemStorageHolder : MonoBehaviour
{

    public string title;
    public GameObject itemObj;
    public GameObject itemPos;
    public GameObject itemSlotOutScreen;
    public float buyValue;
    public string SellValueCurrent;

    public Text titleBarText;

    public bool IncomingItem;
    public bool RemovingItem;

    public int ItemStorageHolderIndex;

    public GameObject highlightGlow;

    public int queryIndex;

    // Start is called before the first frame update
    void Start()
    {
        queryIndex = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (itemObj)
        {
            if(itemObj.transform.position != itemPos.transform.position && IncomingItem) 
            {
                itemObj.transform.position = Vector3.MoveTowards(itemObj.transform.position, itemPos.transform.position, Time.deltaTime*8);
                if (itemObj.transform.position == itemPos.transform.position)
                {
                    IncomingItem = false;
                }
            }
            

            if(RemovingItem && itemObj.transform.position != itemSlotOutScreen.transform.position && RemovingItem)
            {
                itemObj.transform.position = Vector3.MoveTowards(itemObj.transform.position, itemSlotOutScreen.transform.position, Time.deltaTime * 8);
                if (itemObj.transform.position == itemSlotOutScreen.transform.position)
                {
                    RemovingItem = false;
                    StorageManager.instance.RemoveFromStorage(ItemStorageHolderIndex);
                }
            }
        }
    }

    public void ButtonCheck()
    {
        Debug.Log("Ok");
        if (itemObj)
        {
            if(StoragePanelManageMent.instance.storageState == StoragePanelManageMent.StorageState.itemQuery)
            {
                if(queryIndex != -1)
                {
                    StoragePanelManageMent.instance.StoragePanelInfoUpdate(ItemStorageHolderIndex);
                    StorageManager.instance.HighlightSelected(ItemStorageHolderIndex);

                    StoragePanelManageMent.instance.queryIndex = queryIndex;
                    StoragePanelManageMent.instance.QueryBoardInfoUpdate();
                    StoragePanelManageMent.instance.PolishButtonOrNot();
                    //Debug.Log("Okk");
                }
            }
            else
            {
                StoragePanelManageMent.instance.StoragePanelInfoUpdate(ItemStorageHolderIndex);
                StorageManager.instance.HighlightSelected(ItemStorageHolderIndex);
                StoragePanelManageMent.instance.PolishButtonOrNot();
            }
            SellingPanel.instance.UpdateSellingPanelValues();
            StorageShelfSliding.instance.ShelfSnapByItemSlot(ItemStorageHolderIndex);
        }
        
    }

    public void GetItemInfo(int i)
    {
        if(i < StorageManager.instance.itemsIds.Count )
        {
            titleBarText.text = StorageManager.instance.itemsOnShelf[i].GetComponent<ItemToSell>().title;
            //buyValue = StorageManager.instance.itemsOnShelf[i].GetComponent<ItemToSell>().buyValue;
            itemObj = StorageManager.instance.itemsOnShelf[i];
            StorageManager.instance.itemsOnShelf[i].transform.position = itemPos.transform.position;
            StorageManager.instance.itemsOnShelf[i].transform.rotation = itemPos.transform.rotation;
        }
        else
        {
            itemObj = null;
            titleBarText.text = "N / A";
        }

        //titleBarText.text = "N / A";
    }
    public void GetItemFromOutScreen()
    {
        itemObj.transform.position = itemSlotOutScreen.transform.position;
        IncomingItem = true;
    }

    public void RemoveItemStart()
    {
        RemovingItem = true;
    }

    public void HighlightIt()
    {
        highlightGlow.SetActive(true);
    }
    public void DisableHighlight()
    {
        highlightGlow.SetActive(false);
    }
}
