﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceLights : MonoBehaviour
{
    public static PoliceLights instance;
    public GameObject LightsHolder;


    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        DisablePoliceLights();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnablePoliceLights()
    {
        LightsHolder.SetActive(true);
        Invoke("DisablePoliceLights", 3f);
    }

    public void DisablePoliceLights()
    {
        LightsHolder.SetActive(false);
    }
}
