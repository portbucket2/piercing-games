﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolishSyestem : MonoBehaviour
{
    public GameObject HandTool;
    public Transform centerPos;
    public Transform sidePos;
    public bool polishDone;
    Transform tarPos;

    public float polishValue;

    public GameObject polishPanelPolishedUi;
    public GameObject polishPanelPolishingUi;
    public ParticleSystem sparkleParticle;

    public PolishItemUniversal polishItem;

    public static PolishSyestem instance;

    //public bool polishEnabled;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        polishValue = 1.1f;
        polishDone = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && !polishDone )
        {
            tarPos = centerPos;
            polishValue -= Time.deltaTime * 0.2f;
            if(polishValue <= 0)
            {
                polishValue = 0;
                polishDone = true;
                //polishEnabled = false;
                PolishDoneFun();
            }

            polishItem.polishMatAffect( polishValue);
        }
        else
        {
            tarPos = sidePos;
        }
        if (tarPos)
        {
            HandTool.transform.position = Vector3.Lerp(HandTool.transform.position, tarPos.position+(new Vector3(0,polishValue,0)), Time.deltaTime * 20);
        }
        
    }
    public void PolishInitalize()
    {
        polishItem.PolishItemChoose(StoragePanelManageMent.instance.selectedItemRootIndex);
        polishValue = 1.1f;
        polishPanelPolishedUi.SetActive(false);
        polishPanelPolishingUi.SetActive(true);
        polishDone = false;
        polishItem.polishMatAffect(polishValue);
        sparkleParticle.Stop();

        

    }

    void PolishDoneFun()
    {
        polishPanelPolishedUi.SetActive(true);
        polishPanelPolishingUi.SetActive(false);
        StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>().itemCond = ItemToSell.ItemCondition.Excellent;
        StorageManager.instance.itemSlots[StoragePanelManageMent.instance.selectedSlotIndex].ButtonCheck();
        sparkleParticle.Play();
    }
}
