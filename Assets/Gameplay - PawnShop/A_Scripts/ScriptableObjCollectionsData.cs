﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new scriptable Data", menuName = "item Data Pawn")]
public class ScriptableObjCollectionsData : ScriptableObject
{
    
    public GameObject[] itemPrefabs;
    public Material[] nonPolishedMats;
    public Material[] polishedMats;

    public Texture2D[] texNonPolished;
    public Texture2D[] texPolished;
    public Sprite[] itemPPs;

    public Sprite[] charPPs;

    

}
