﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellingPanel : MonoBehaviour
{
    public Text offerSerialtext;
    public Text[] sellingValueText;
    public Text SellingItemTitle;
    public Text boughtValueText;
    public Text profitvalueText;
    public Text[] OfferCommentText;

    public Image SellingItemImage;

    public static SellingPanel instance;

    public int offerindex;

    public float multi;
    float sellValue;
    public int[] sellvalues;

    public GameObject[] offerBoards;
    public Transform midPos;
    public Transform leftPos;
    public Transform rightPos;



    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //UpdateSellingPanelValues();

    }

    // Update is called once per frame
    void Update()
    {
        OfferBoardsPositioning();
    }

    public void UpdateSellingPanelValues()
    {
        //SellOfferBoradValues(0);
        //SellOfferBoradValues(1);
        //SellOfferBoradValues(2);

        //boughtValueText.text = "$ " + AccountsControl.instance.currentBoughtvalue ;
        SellingItemTitle.text = StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>().title;
        boughtValueText.text = "$ " + StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>().buyValue;
        sellValue = (float)StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>().EstValue;
        sellvalues[0] = (int)sellValue;
        sellingValueText[0].text = "$ " + (int)sellValue;

        offerSerialtext.text = "" + (offerindex + 1) + " / " + 3;
        profitvalueText.text = "$ "+ (sellValue - StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>().buyValue) ;
        AccountsControl.instance.lastSoldvalue = (int)sellValue;

        LoadItemImage();

    }

    public void NextOffer()
    {
        offerindex += 1;
        if(offerindex <= 2)
        {
            //UpdateSellingPanelValues();
            offerSerialtext.text = "" + (offerindex + 1) + " / " + 3;
            profitvalueText.text = "$ " + (sellvalues[offerindex] - AccountsControl.instance.currentBoughtvalue);
            AccountsControl.instance.lastSoldvalue = sellvalues[offerindex];
        }
    }

    public void ResetOfferIndex()
    {
        offerindex = 0;
        UpdateSellingPanelValues();
    }

    void SellOfferBoradValues(int i)
    {
        multi = (Random.Range(0.7f, 1.5f));
        //sellValue = ((float)AccountsControl.instance.currentEstValue * multi);
        sellValue = ((float)StorageManager.instance.itemsOnShelf [StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>(). EstValue * multi);
        sellvalues[i] = (int)sellValue;
        sellingValueText[i].text = "$ " + sellvalues[i] ;
        if (multi < 0.9f)
        {
            OfferCommentText[i].text = "POOR";
        }
        else if (multi > 1.1f)
        {
            OfferCommentText[i].text = "GOOD";
        }
        else
        {
            OfferCommentText[i].text = "AVERAGE";
        }
    }

    void OfferBoardsPositioning()
    {
        if(offerindex == 0)
        {
            offerBoards[0].transform.position = Vector3.Lerp(offerBoards[0].transform.position, midPos.position, Time.deltaTime * 10);
            offerBoards[1].transform.position = Vector3.Lerp(offerBoards[1].transform.position,rightPos.position, Time.deltaTime * 10);
            offerBoards[2].transform.position = Vector3.Lerp(offerBoards[2].transform.position,rightPos.position, Time.deltaTime * 10);
        }
        else if (offerindex == 1)
        {
            offerBoards[0].transform.position = Vector3.Lerp(offerBoards[0].transform.position, leftPos.position, Time.deltaTime * 10);
            offerBoards[1].transform.position = Vector3.Lerp(offerBoards[1].transform.position, midPos.position, Time.deltaTime * 10);
            offerBoards[2].transform.position = Vector3.Lerp(offerBoards[2].transform.position, rightPos.position, Time.deltaTime * 10);
        }
        else
        {
            offerBoards[0].transform.position = Vector3.Lerp(offerBoards[0].transform.position, leftPos.position, Time.deltaTime * 10);
            offerBoards[1].transform.position = Vector3.Lerp(offerBoards[1].transform.position, leftPos.position, Time.deltaTime * 10);
            offerBoards[2].transform.position = Vector3.Lerp(offerBoards[2].transform.position, midPos.position, Time.deltaTime * 10);
        }
    }

    public void LoadItemImage()
    {
        //SellingItemImage.sprite = ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().itemImage;

        SellingItemImage.sprite = StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>().itemImage;
    }
}
