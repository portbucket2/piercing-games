﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WantedBorad : MonoBehaviour
{
    public Animator wantedBoardAnimator;

    public static WantedBorad instance;
    public Text TextRewardInPoster;

    public Image wantedPP;
    public Image wantedRewardPP;
    public List<Sprite> wantedPpSprites;
    public int thiefIndex;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        thiefIndex = GameManagement.instance.thiefPPref;
        wantedPP.sprite = wantedPpSprites[thiefIndex];
        wantedRewardPP.sprite = wantedPpSprites[thiefIndex];

        BustedAnimReset();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BustedAnimPlay()
    {
        //wantedBoardAnimator.SetTrigger("busted");

        wantedBoardAnimator.SetBool("bustedd", true);
        //thiefIndex += 1;
        //wantedPP.sprite = wantedPpSprites[thiefIndex];
    }
    public void BustedAnimReset()
    {
        //wantedBoardAnimator.SetTrigger("resetBusted");

        wantedBoardAnimator.SetBool("bustedd", false);
        RewardvalueIntoPoster();
        //wantedPP.sprite = wantedPpSprites[thiefIndex];
    }
    public void RewardvalueIntoPoster()
    {
        TextRewardInPoster.text = "$ " + RewardBoardUi.instance. rewardValue;
    }

    public void NextThiefPP()
    {
        
        thiefIndex += 1;
        if(thiefIndex >= wantedPpSprites.Count)
        {
            thiefIndex = 0;
        }
        wantedPP.sprite = wantedPpSprites[thiefIndex];
        wantedRewardPP.sprite = wantedPpSprites[thiefIndex];
        PlayerPrefs.SetInt("thiefPPref", thiefIndex);
    }
    //public void 
}
