﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class charHUD : MonoBehaviour
{
    public GameObject HUD;
    public GameObject speechBubble;
    public GameObject ratingBoard;
    public int SellValue;
    public int BuyValueHud;
    public Text SellValueText;
    public Text BuyValueText;
    public Text TextHeader;
    public GameObject BuyerBoard;
    public GameObject SellerBoard;
    public Image buyingItemImage;
    public Sprite[] imojis;
    public Image[] starFills;
    public Image imojiProjected;
    public int rating;
    public Text expressionText;
    public string[] expressionsRated;
    // Start is called before the first frame update
    void Start()
    {
        CloseHud();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AppearSpeechBubble()
    {
        
        HUD.SetActive(true);
        //speechBubble.SetActive(true);
        ratingBoard.SetActive(false);

        if (GetComponent<CharacterAnimController>().Buyer)
        {
            BuyerBoard.gameObject.SetActive(true);
            SellerBoard.gameObject.SetActive(false);
            int i = (int)GetComponentInParent<CharacterAnimController>().buyingItem;
            if (GetComponent<CharacterAnimController>().buyingItemCondition == ItemToSell.ItemCondition.Excellent)
            {
                TextHeader.text = ""+ GetComponent<CharacterAnimController>().buyingItemCondition + " " + GetComponent<CharacterAnimController>().buyingItem;
                BuyValueHud = (int)(StorageManager.instance.itemsCreated[i].EstValue * 1.2f);
                BuyValueText.text = "" + BuyValueHud;
            }
            else
            {
                TextHeader.text = "" + GetComponent<CharacterAnimController>().buyingItem;

                BuyValueHud = (int)(StorageManager.instance.itemsCreated[i].EstValue );
                BuyValueText.text = "" + BuyValueHud;
            }

            //buyingItemImage.sprite = StorageManager.instance.itemsCreated[(int)GetComponent<CharacterAnimController>().buyingItem].itemImage;
            buyingItemImage.sprite = 
                GetComponent<CharacterAnimController>().item.GetComponent<ItemToSell>().itemImagesAccToType[(int)GetComponent<CharacterAnimController>().buyingItem];


        }
        else
        {
            BuyerBoard.gameObject.SetActive(false);
            SellerBoard.gameObject.SetActive(true);
            SellValueText.text = "" + SellValue;
        }
    }

    public void CloseHud()
    {
        HUD.SetActive(false);
    }
    public void RatingHudEnable()
    {
        
        HUD.SetActive(true);
        
        ratingBoard.SetActive(true);
        BuyerBoard.gameObject.SetActive(false);
        SellerBoard.gameObject.SetActive(false);
        //speechBubble.SetActive(false);

        if (GetComponent<CharacterAnimController>().Buyer &&(GetComponent<CharacterAnimController>().buyingItemCondition == ItemToSell.ItemCondition.Excellent))
        {
            if (StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>().itemCond
                == ItemToSell.ItemCondition.Excellent)
            {
                Rateit(4);
            }
            else
            {
                Rateit(2);
            }
        }
        
        else
        {
            Rateit(4);
        }
        
        Invoke("CloseHud", 2f);
    }
    public void RatingHudReject()
    {

        HUD.SetActive(true);

        ratingBoard.SetActive(true);
        BuyerBoard.gameObject.SetActive(false);
        SellerBoard.gameObject.SetActive(false);
        //speechBubble.SetActive(false);
        Rateit(0);
        Invoke("CloseHud", 2f);
    }
    void Rateit(int rate)
    {
        imojiProjected.sprite = imojis[rate ];
        expressionText.text = expressionsRated[rate ];
        for (int i = 0; i < imojis.Length; i++)
        {
            if (i <= rate)
            {
                starFills[i].gameObject.SetActive(true);
            }
            else
            {
                starFills[i].gameObject.SetActive(false);
            }
        }

        MasterRatingUiUp.instance.MasterRateFromSingleRating(rate + 1);
    }

    

}
