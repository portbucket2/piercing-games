﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class PhotoShoot : MonoBehaviour
{
    public Camera cam;
    Texture2D snapShot;
    //public Image snapImage;
    //public SpriteRenderer snapImageSprite;
    Sprite snapSprite;
    public RenderTexture rndTex;

    public int TakeIndex;

    public object DataPathDirectoryManager { get; private set; }

    public GameObject[] SellerChar;
    //public List<ThiefStrap> straps;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < SellerChar. Length; i++)
        {
            SellerChar[i].GetComponentInChildren<ThiefStrap>().gameObject.SetActive(false);
        } 
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeSnap();
        }
    }

    public void TakeSnap()
    {
        cam.gameObject.SetActive(true);
        cam.Render();

        //RenderTexture currentActiveRT = new RenderTexture(900, 1600, 24);
        RenderTexture currentActiveRT = rndTex;
        cam.targetTexture = currentActiveRT;


        cam.Render();
        RenderTexture.active = currentActiveRT;
        Texture2D image = new Texture2D(cam.targetTexture.width, cam.targetTexture.height, TextureFormat.RGB24, false);
        image.ReadPixels(new Rect(0, 0, cam.targetTexture.width, cam.targetTexture.height), 0, 0);
        image.Apply();

        SaveScreenShot(image);

        TakeIndex += 1;
        
    }

    void SaveScreenShot(Texture2D Image)
    {
        byte[] bytes = Image.EncodeToPNG();
        string takeName = "camScreenShot" + TakeIndex;
        //string fileName = Application.persistentDataPath + "/ScreenShots/takeSnap.png";

        //var dirPath = DataPathDirectoryManager.instance.filePath + "/../SavedImages/";
        var dirPath = Application.persistentDataPath; 
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }

        string fileName = dirPath + "/takeSnap" + TakeIndex  + ".png";
        //string fileName = Application.persistentDataPath + "/takeSnap" + GameManagement.instance.levelIndex + ".png";

        //string fileName = System.IO.Directory.GetCurrentDirectory() + "/takeSnap" + GameManagement.instance.levelIndex + ".png";
        //string fileName = Application.dataPath + "/takeSnap" +GameManagement.instance.levelIndex +".png";
        System.IO.File.WriteAllBytes(fileName, bytes);
        //Debug.Log("snapTaken");
        Debug.Log(dirPath);
        Debug.Log(fileName);
    }
}
