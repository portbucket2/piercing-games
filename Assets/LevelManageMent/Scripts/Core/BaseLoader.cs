﻿using UnityEngine;
using System;


public abstract class BaseLoader : MonoBehaviour
{
    public static BaseLoader instance;
    protected abstract int iterationIndex { get; }
    protected abstract void RequestComplete(bool success);
    protected abstract void ReportStars(int count);

    public static void GetIterationIndex(ref int iteration)
    {
        if (instance)
        {
            iteration = instance.iterationIndex;
        }
    }
    public static void RequestCompletion(bool  success)
    {
        if (instance)
        {
            instance.RequestComplete(success);
        }
    }
    public static void ReportStarCount(int count)
    {
        if (instance)
        {
            instance.ReportStars(count);
        }
    }

}

