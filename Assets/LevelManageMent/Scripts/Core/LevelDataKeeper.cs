﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using System;

public class LevelDataKeeper : MonoBehaviour
{

    public bool allLevelsUnlocked = false;
    public int numberofexceptions = 40;

    //public LevelPrefabManager commonPrefab;
    public List<AreaDefinition> areaDefinitions;


    internal List<LevelArea> levelAreas = new List<LevelArea>();


    public static LevelDataKeeper instance;

    public HardData<int> randomAreaIndex;
    public HardData<int> randomLevelIndex;

    public List<HardData<int>> randomlevelcollection;
    public HardData<int> LastRandomIndex;


    public static bool startLevelReady = false;
    public void Awake()
    {
//#if ((UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR)
//        Debug.unityLogger.logEnabled = false;
//#endif

        if (instance)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            randomAreaIndex = new HardData<int>("RAI", 0);
            randomLevelIndex = new HardData<int>("RLI", 0);
            DontDestroyOnLoad(this.transform.root.gameObject);

            //int abchoice = 1;//ABManager.GetValue(ABtype.LEVEL_ORDER);
            for (int i = 0; i < areaDefinitions.Count; i++)
            {
                levelAreas.Add(new LevelArea(areaDefinitions[i], i));
            }
            for (int a = 0; a < levelAreas.Count; a++)
            {
                LevelArea area = levelAreas[a];
                area.lastUnlockedLevelIndex = new HardData<int>(string.Format("UNLOCKED_PROGRSS_{0}", a), 0);
                area.stars = new List<HardData<int>>();
                for (int l = 0; l < area.levelDefinition.Count; l++)
                {
                    area.stars.Add(new HardData<int>(string.Format("stars_{0}_{1}", a, l), 0));
                }
            }


            randomlevelcollection = new List<HardData<int>>();

            for (int i = 0; i < numberofexceptions * 2; i++)
            {
                randomlevelcollection.Add(new HardData<int>(string.Format("RANDOM_INDEX_{0}_{1}", (int)(i / 2), i % 2 == 0 ? "AREA" : "LEVEL"), -1));
            }
            LastRandomIndex = new HardData<int>("LAST_INDEX", 0);
        }



    }
    public void AddtoRandomException ( int aI, int lI )
    {
        randomlevelcollection[LastRandomIndex.value * 2].value = aI;
        randomlevelcollection[LastRandomIndex.value * 2 +1].value = lI;
    }

    public void changerandomindex ()
    {
        if ( LastRandomIndex.value >= numberofexceptions-1 )
        {
            LastRandomIndex.value = 0;
        }
        else
            LastRandomIndex.value++;
    }


    public int GetTotalStarCount()
    {

        int count = 0;
        foreach (LevelArea area in levelAreas)
        {
            foreach (HardData<int> star in area.stars)
            {
                count += star.value;
            }
        }
        return count;
    }
    public bool IsAreaUnlocked(int areaIndex)
    {
        if (allLevelsUnlocked) return true; 
        if (levelAreas[areaIndex].starToEnter > GetTotalStarCount()) return false;
        if (areaIndex == 0) return true;
        else if (levelAreas[areaIndex - 1].lastUnlockedLevelIndex.value >= (levelAreas[areaIndex - 1].levelDefinition.Count ))
        {
            return true;
        }
        else return false;
    }

    public static int GetCumulativeLevelCap ()
    {
        int cumulativeIndex = 0;
        foreach ( LevelArea lva in instance.levelAreas )
        {
            cumulativeIndex += lva.levelDefinition.Count;
        }
        return cumulativeIndex ;
    }

    #region indexing
    public static LevelDefinition FetchAppropriateDefinition(int ai, int li)
    {

        if (ai == instance.levelAreas.Count - 1)
        {
            if (li >= instance.levelAreas[ai].levelDefinition.Count)
            {
                ai = instance.randomAreaIndex.value;
                li = instance.randomLevelIndex.value;
            }
        }

        return instance.levelAreas[ai].levelDefinition[li];
    }
    public static int Last_ai
    {
        get
        {

            if (!instance) return -1;


            int lastProperAreaIndex = 0;
            for (int i = 0; i < instance.levelAreas.Count; i++)
            {
                LevelArea area = instance.levelAreas[i];
                if (!instance.IsAreaUnlocked(i))
                {
                    return lastProperAreaIndex;
                }
                else if (area.levelDefinition.Count > 0)
                {
                    lastProperAreaIndex = i;
                }
            }
            if (instance.levelAreas.Count == 0) return -1;
            else return lastProperAreaIndex;
        }
    }
    public static int Last_li
    {
        get
        {
            int ai = Last_ai;
            return instance.levelAreas[ai].lastUnlockedLevelIndex.value;
        }
    }

    public static int GetNextLevel_ai(int current_ai, int current_li)
    {
        LevelArea currentArea = instance.levelAreas[current_ai];
        if (current_li + 1 < currentArea.levelDefinition.Count)
        {
            return current_ai;
        }
        else
        {
            if (current_ai + 1 < instance.levelAreas.Count)
            {
                return current_ai + 1;
            }
            else
            {
                return current_ai;
            }
        }
    }
    public static int GetNextLevel_li(int current_ai, int current_li)
    {
        LevelArea currentArea = instance.levelAreas[current_ai];
        if (current_li + 1 < currentArea.levelDefinition.Count)
        {
            return current_li + 1;
        }
        else
        {
            if (current_ai + 1 < instance.levelAreas.Count)
            {
                return 0;
            }
            else
            {
                return current_li + 1;
            }
        }
    }
    #endregion

}


[Serializable]
public class LevelArea
{
    public int starToEnter=0;
    public List<LevelDefinition> levelDefinition;

    public LevelArea(AreaDefinition definition, int areaI)
    {
        if (definition == null) Debug.LogError("null prefab");

        starToEnter = definition.starToEnter;
        levelDefinition = new List<LevelDefinition>();
        //if (csvChoice >= definition.sequenceAndVariantData_CSV.Length || csvChoice<0)
        //{
        //    Debug.LogError("csvchoice is out of bounds");
        //    csvChoice = 0;
        //}
        //Dictionary<string,CSVRow> levelDatas = CSVReader.ReadCSVAsset(definition.generalLevelDetails_CSV,0, '|');
        CSVRow[] sequenceData = CSVReader.ReadCSVAsset(definition.sequenceAndVariantData_CSV, '|');

        for (int i = 0; i < sequenceData.Length; i++)
        {
            int iterationIndex =  int.Parse(sequenceData[i].fields[2]);
            string title  = sequenceData[i].fields[1];

            try
            {
                //CSVRow levelData = levelDatas[sequenceData[i].fields[0]];
                levelDefinition.Add(new LevelDefinition(
                        title: title,
                        sequence_LI: i,
                        sequence_AI: areaI,
                        sceneAddress: sequenceData[i].fields[0],
                        iterationIndex: iterationIndex));//  sequenceData[i],areaI,i));
            }
            catch(System.Exception e)
            {
                Debug.LogError(sequenceData[i].fields[0]);
                Debug.LogError(e.Message);
            }

        }
    }

    [NonSerialized]
    public List<HardData<int>> stars;
    [NonSerialized]
    public HardData<int> lastUnlockedLevelIndex;
}


[System.Serializable]
public class AreaDefinition
{
    public int starToEnter = 0;
    public TextAsset generalLevelDetails_CSV;
    public TextAsset sequenceAndVariantData_CSV;
}


[System.Serializable]
public class LevelDefinition 
{
    public string title;
    public string detail;

    //public string remoteSubPath;
    public string sceneAdress;
    public int iterationIndex;
    public int sequence_AI;
    public int sequence_LI;

    public float standardTiming=60;

    public HardData<int> PlayCount;

    public event Action<GameObject> onReadyToLoad;

    public bool isReadyToLoad;
    //public string resourcePath0;

    public void SetReadyToLoad(GameObject prefab)
    {
        isReadyToLoad = true;
        onReadyToLoad?.Invoke(prefab);
        onReadyToLoad = null;
    }


    public LevelDefinition() { }
    public LevelDefinition(string title, string sceneAddress, int sequence_AI, int sequence_LI, int iterationIndex)
    {
        isReadyToLoad = false;

        this.sceneAdress = sceneAddress;
        this.iterationIndex = iterationIndex;
        this.sequence_AI = sequence_AI;
        this.sequence_LI = sequence_LI;
        this.PlayCount = new HardData<int>( string.Format("NUMBER_OF_LOADS_{0}-{1}", this.sceneAdress, iterationIndex), 0 );

        this.title = title;
        //for (int i = 0; i < levelData.fields.Length; i++)
        //{
        //    switch (i)
        //    {
        //        case 1:
        //            title = levelData.fields[i];
        //            break;
        //        case 2:
        //            detail = levelData.fields[i];
        //            break;

        //        //case 3:
        //        //    standardTiming = float.Parse(levelData.fields[i]);
        //        //    break;
        //    }
        //}
    }
    public void PlayCountIncrease ()
    {
        if ( PlayCount != null )
        {
            PlayCount.value++;
        }
    }

    public void PlayCountReset ()
    {
        if ( PlayCount != null )
        {
            PlayCount.value=0;
        }
    }

    public static int Query_CumulativeNumber(int areaI, int levelI, LoadType loadType)
    {
        switch (loadType)
        {
            case LoadType.NORMAL:
                if (LevelDataKeeper.instance != null)
                {
                    int cumulativeIndex = 0;
                    for (int i = 0; i < areaI; i++)
                    {
                        cumulativeIndex += LevelDataKeeper.instance.levelAreas[i].levelDefinition.Count;
                    }
                    return cumulativeIndex + levelI + 1;
                }
                else
                {
                    return levelI + 1;
                }
            default:
                return -1;
        }
    }

    //public static int GetStarFor(LevelDefinition definition, LoadType loadType, int areaI, int levelI)
    //{
    //    switch (loadType)
    //    {
    //        case LoadType.NORMAL:
    //            return LevelLoader.instance.levelAreas[definition.sequence_AI].stars[definition.sequence_LI].value;
    //        case LoadType.DAILY:
    //            return DailyLevelManagement.instance.GetStarCount(levelI);
    //        default:
    //            return -1;
    //    }
    //}
    //public static void SetStarFor(LevelDefinition definition, LoadType loadType, int star, int areaI, int levelI)
    //{
    //    switch (loadType)
    //    {
    //        case LoadType.NORMAL:
    //            LevelLoader.instance.levelAreas[definition.sequence_AI].stars[definition.sequence_LI].value = star;
    //            break;
    //        case LoadType.DAILY:
    //            DailyLevelManagement.instance.SetStarCount(levelI,star);
    //            break;
    //    }
    //}
}
