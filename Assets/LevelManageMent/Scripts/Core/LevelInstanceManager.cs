﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInstanceManager
{
    static LevelInstanceManager instance { get { return PrimaryLoader.currentLevelInstance; } }
    public static event System.Action newPrefabLoading;
    public static event System.Action<Theme> newThemeLoading;
    public Theme theme;
    public LoadType loadType = LoadType.NORMAL;
    public LevelDefinition levelDefinition;
    public int cumulitiveLevelNo;
    public int playerAreaIndex { get; set; }
    public int playerLevelIndex { get; private set; }
    int sequence_areaIndex { get { return levelDefinition.sequence_AI; } }
    int sequence_levelIndex { get { return levelDefinition.sequence_LI; } }



    private static FRIA.HardData<int> totalCompletedLevelNoHD;
    public static int TotalCompletedLevelCount
    {
        get
        {
            if (totalCompletedLevelNoHD == null)
            {
                totalCompletedLevelNoHD = new FRIA.HardData<int>("TOTAL_COMPLETED_LEVEL_COUNT",
                    LevelDefinition.Query_CumulativeNumber(LevelDataKeeper.Last_ai, LevelDataKeeper.Last_li, LoadType.NORMAL));
            }
            return totalCompletedLevelNoHD.value;
        }
        set
        {
            if (totalCompletedLevelNoHD == null)
            {
                totalCompletedLevelNoHD = new FRIA.HardData<int>("TOTAL_COMPLETED_LEVEL_COUNT",
                    LevelDefinition.Query_CumulativeNumber(LevelDataKeeper.Last_ai, LevelDataKeeper.Last_li, LoadType.NORMAL));
            }
            totalCompletedLevelNoHD.value = value;
        }
    }

    int stars = 0;

    public string LevelTypeInfo
    {
        get
        {
            switch (loadType)
            {
                default:
                case LoadType.NORMAL:

                    if (sequence_areaIndex != playerAreaIndex || sequence_levelIndex != playerLevelIndex)
                        return "random";
                    else
                        return "normal";

            }
        }
    }

    public InLevelCanvasManager canvMan
    {
        get
        {
            return PrimaryLoader.derivedInstance.levelCanvas;
        }
    }
    int prevStarCount = 0;
    public LevelInstanceManager(LoadType loadType, int playerAreaIndex, int playerLevelIndex, LevelDefinition deifinition)
    {
        this.loadType = loadType;
        levelDefinition = deifinition;
        cumulitiveLevelNo = LevelDefinition.Query_CumulativeNumber(playerAreaIndex, playerLevelIndex, loadType);
        this.playerAreaIndex = playerAreaIndex;
        this.playerLevelIndex = playerLevelIndex;
        newThemeLoading?.Invoke(theme);
        newPrefabLoading?.Invoke();
        AnalyticsAssistant.LevelStarted(cumulitiveLevelNo, levelDefinition.title, LevelTypeInfo);
        prevStarCount = StarData;
    }


    public void OnComplete(bool success)
    {
        if (success)
        {
            Debug.Log("Complete Called");
            TotalCompletedLevelCount++;
            if (StarData >= stars)
            {
                stars = StarData;
            }
            else
            {
                StarData = stars;
            }
            if (loadType == LoadType.NORMAL)
            {
                AnalyticsAssistant.LevelCompletedAppsFlyer(LevelInstanceManager.instance.cumulitiveLevelNo);
                LevelRandomizer();
            }
            canvMan.LoadSucces();

        }
        else
        {
            canvMan.LoadFail();
        }
        canvMan.successUIController.SetStars(stars,stars-prevStarCount);
    }
    void LevelRandomizer()
    {
        if (!checkcontainment(sequence_areaIndex, sequence_levelIndex))
        {
            LevelDataKeeper.instance.AddtoRandomException(sequence_areaIndex, sequence_levelIndex);
            LevelDataKeeper.instance.changerandomindex();
        }
        if (playerLevelIndex >= LevelDataKeeper.instance.levelAreas[playerAreaIndex].lastUnlockedLevelIndex.value)
        {
            LevelDataKeeper.instance.levelAreas[playerAreaIndex].lastUnlockedLevelIndex.value = playerLevelIndex + 1;

            int arI = UnityEngine.Random.Range(0, LevelDataKeeper.instance.levelAreas.Count);
            int lvlI = UnityEngine.Random.Range(0, LevelDataKeeper.instance.levelAreas[arI].levelDefinition.Count);

            int j = 0;
            while (checkcontainment(arI, lvlI) && j < 100)
            {
                arI = UnityEngine.Random.Range(0, LevelDataKeeper.instance.levelAreas.Count);
                lvlI = UnityEngine.Random.Range(0, LevelDataKeeper.instance.levelAreas[arI].levelDefinition.Count);
                j++;
            }

            LevelDataKeeper.instance.randomAreaIndex.value = arI;
            LevelDataKeeper.instance.randomLevelIndex.value = lvlI;
        }


    }
    bool checkcontainment(int aI, int lI)
    {
        for (int i = 0; i < LevelDataKeeper.instance.numberofexceptions; i++)
        {
            if (LevelDataKeeper.instance.randomlevelcollection[i * 2].value == aI && LevelDataKeeper.instance.randomlevelcollection[i * 2 + 1].value == lI)
            {
                return true;
            }
        }

        return false;
    }


    //public void ReportEarlyCompletion(int reportedStars)
    //{
    //    int starC = GetStarData();
    //    if (starC < reportedStars)
    //    {
    //        SetStarData(reportedStars);
    //    }
    //    if (loadType == LoadType.NORMAL)
    //    {
    //        AnalyticsAssistant.LevelCompletedAppsFlyer(LevelInstanceManager.instance.cumulitiveLevelNo);
    //        LevelRandomizer();
    //    }
    //}
    //public void AnimateNextLevelButton()
    //{
    //    if (canvMan.successUIController.nextButtonAnim)
    //    {
    //        canvMan.successUIController.nextButtonAnim.SetTrigger("go");
    //    }
    //    if ( canvMan.successUIController.nextButtonAnim_2step )
    //    {
    //        canvMan.successUIController.nextButtonAnim_2step.SetTrigger ( "go" );
    //    }
    //}

    public int StarData
    {
        get
        {
            switch (loadType)
            {
                case LoadType.NORMAL:
                    return LevelDataKeeper.instance.levelAreas[sequence_areaIndex].stars[sequence_levelIndex].value;
                default:
                    return -1;
            }
        }
        set
        {
            switch (loadType)
            {
                case LoadType.NORMAL:
                    LevelDataKeeper.instance.levelAreas[sequence_areaIndex].stars[sequence_levelIndex].value = value;
                    break;
            }
        }
    }

    public void SubmitStarCount(int count, int limit = 3)
    {
        if (count > limit) count = limit;
        if (stars < count)
        {
            stars = count;
            StarData = stars;
        }
    }
    

    public void AddStar ()
    {
        stars++;
        //canvMan.successUIController.SetStars ( stars );

        if ( StarData < stars )
        {
            StarData  = stars;
        }
    }







}
public enum Theme
{
    BLUE = 0,
    RED = 1,
    GREEN = 2,
    YELLOW =3,
}
public interface IThemeScript
{
    Theme GetTheme();
}