﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using System;
using UnityEngine.SceneManagement;


public class PrimaryLoader :  BaseLoader
{
    public static PrimaryLoader derivedInstance;
    public static QuickSettings settings;
    public InLevelCanvasManager levelCanvas;

    public AutoAreaLoader areaLoader;
    public QuickSettings settingsRef;
    


    void Awake()
    {
        instance = this;
        derivedInstance = this;
        settings = settingsRef;

    }


    private void Start()
    {

        ChangeLevelObjectState(GameState.INITIAL);

        int ai = LevelDataKeeper.Last_ai;
        int li = LevelDataKeeper.Last_li;
        LevelDefinition levelDef = LevelDataKeeper.FetchAppropriateDefinition(ai,li);
        StartLevel(LoadType.NORMAL,ai, li, levelDef,false);
    }




    #region state_management
    public static GameState state;
    public void ChangeLevelObjectState(GameState state)
    {
        PrimaryLoader.state = state;
        //Debug.LogFormat("State set to {0}",state);
        switch (state)
        {
            case GameState.INITIAL:
                DisableObjects(inGameItems);
                DisableObjects(levelSelectItems);
                EnableObjects(initialItems);
                break;
            case GameState.GAME:
                DisableObjects(initialItems);
                DisableObjects(levelSelectItems);
                EnableObjects(inGameItems);
                break;
            case GameState.LEVELS:
                DisableObjects(initialItems);
                DisableObjects(inGameItems);
                EnableObjects(levelSelectItems);
                break;
            default:
                break;
        }

        //Debug.LogFormat("Doubt X: {0}",on);

    }

    public void DisableObjects(List<GameObject> gameObjects)
    {
        foreach (var item in gameObjects)
        {
            item.SetActive(false);
        }
    }
    public void EnableObjects(List<GameObject> gameObjects)
    {
        foreach (var item in gameObjects)
        {
            item.SetActive(true);
        }
    }

    public List<GameObject> inGameItems = new List<GameObject>();
    public List<GameObject> initialItems = new List<GameObject>();
    public List<GameObject> levelSelectItems = new List<GameObject>();

    #endregion


    public static LevelInstanceManager currentLevelInstance;
    public static GameObject lastLoadedLevelResource;


    static int iterationIndex_ForLastChosenLevel;
    protected override int iterationIndex { get { return iterationIndex_ForLastChosenLevel; } }
    protected override void ReportStars(int count)
    {
        currentLevelInstance.SubmitStarCount(count);
    }

    protected override void RequestComplete(bool success)
    {
        ReportStarCount(3);
        currentLevelInstance.OnComplete(success);
    }


    public static void StartLevel(LoadType loadType, int areaIndex, int levelIndex, LevelDefinition levelDefinition, bool asyncMode = true)
    {
        int cumlevelnum = LevelDefinition.Query_CumulativeNumber(areaIndex, levelIndex, loadType);
        currentLevelInstance = new LevelInstanceManager(
                loadType: loadType,
                playerAreaIndex: areaIndex,
                playerLevelIndex: levelIndex,
                deifinition: levelDefinition
                );

        iterationIndex_ForLastChosenLevel = levelDefinition.iterationIndex;

        if (asyncMode)
        {

            instance.StartCoroutine(derivedInstance.LoadRoutine(levelDefinition));
        }
        else
        {
            SceneManager.LoadScene(levelDefinition.sceneAdress);
            derivedInstance.levelCanvas.LevelStart(currentLevelInstance);
        }
    }
    IEnumerator LoadRoutine(LevelDefinition levelDefinition)
    {
        LoadingSign.SetState(true);
        yield return SceneManager.LoadSceneAsync(levelDefinition.sceneAdress);
        derivedInstance.levelCanvas.LevelStart(currentLevelInstance);
        LoadingSign.SetState(false);
    }

    public static void OnReset()
    {

        int ai = currentLevelInstance.playerAreaIndex;
        int li = currentLevelInstance.playerLevelIndex;
        LoadType lt = currentLevelInstance.loadType;

        StartLevel(lt, ai, li, currentLevelInstance.levelDefinition);
    }


    public static void OnNext()
    {
        int ai = 0;
        int li = 0;
        LevelDefinition levelDef = null;
        LoadType lt = currentLevelInstance.loadType;
        switch (currentLevelInstance.loadType)
        {
            case LoadType.NORMAL:
                {
                    ai = LevelDataKeeper.GetNextLevel_ai(currentLevelInstance.playerAreaIndex, currentLevelInstance.playerLevelIndex);
                    li = LevelDataKeeper.GetNextLevel_li(currentLevelInstance.playerAreaIndex, currentLevelInstance.playerLevelIndex);
                    levelDef = LevelDataKeeper.FetchAppropriateDefinition(ai, li);
                }
                break;
            default:
                break;
        }

        StartLevel(lt, ai, li, levelDef);
    }



    //public static bool isItRestart = false;
    public static void OnLevels()
    {
        int focusIndex = -1;
        LoadType lt = LoadType.NORMAL;
        if (derivedInstance != null)
        {
            lt = currentLevelInstance.loadType;
            focusIndex = currentLevelInstance.playerLevelIndex;
        }
        derivedInstance.areaLoader.LoadLevelList(lt, focusIndex);
        //UnLoad();
    }
}
public enum GameState
{
    INITIAL,
    GAME,
    LEVELS,
    LEVELS_DAILY,
}
public enum LoadType
{
    NORMAL,
}
