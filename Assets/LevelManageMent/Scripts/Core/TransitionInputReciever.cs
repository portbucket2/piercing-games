﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionInputReciever : MonoBehaviour
{
    public static event System.Action initialInputRecieved;
    public GameState fromState = GameState.INITIAL;
    void Update()
    {
        if (PrimaryLoader.state == fromState)
        {
            if (Input.GetMouseButtonUp(0)||(LevelDataKeeper.Last_ai == 0 && LevelDataKeeper.Last_li==0))
            {
                PrimaryLoader.derivedInstance.ChangeLevelObjectState(GameState.GAME);
                initialInputRecieved?.Invoke();
            }
        }

    }
}
