﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingSign : MonoBehaviour
{
    private static LoadingSign instance;
    public GameObject loadingObject;

    public Transform rotatingTrans;
    // Start is called before the first frame update
    public float rotateSpeed;

    void Start()
    {
        instance = this;
        loadingObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        rotatingTrans.Rotate(rotatingTrans.forward, rotateSpeed * Time.unscaledDeltaTime);
    }

    public static void SetState(bool enabled)
    {
        instance.loadingObject.SetActive(enabled);
    }
}
