﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine.UI;
//using UnityEngine;
//using UnityEngine.ResourceManagement.AsyncOperations;

//public class DLCDialogueMan : MonoBehaviour
//{

//    public GameObject rootObj;

//    public GameObject barObj;

//    public Text titleText;
//    public Text bodyText;
//    public Text buttonText;

//    public Image barImage;

//    public Button mainButton;


//    public static DLCDialogueMan instance;
//    private void Awake()
//    {
//        if (!instance)
//        {
//            instance = this;

//            rootObj.SetActive(false);
//        }
//    }

//    public void LoadFailed()
//    {
//        StopAllCoroutines();
//        rootObj.SetActive(true);
//        barObj.SetActive(false);
//        titleText.text = "Check Your Internet!";
//        bodyText.text = "Can't connect to download servers!";
//        buttonText.text = "Okay";
//        mainButton.onClick.RemoveAllListeners();
//        mainButton.onClick.AddListener(() =>
//        {
//            rootObj.SetActive(false);
//        });
//    }

//    public void LoadProcessing(AsyncOperationHandle asyncHandle, System.Action onReady)
//    {
//        rootObj.SetActive(true);
//        barObj.SetActive(true);
//        titleText.text = "Downloading Level";
//        bodyText.text = "Your level will load once the download is complete";
//        buttonText.text = "Cancel";
        
//        mainButton.onClick.RemoveAllListeners();
//        mainButton.onClick.AddListener(() =>
//        {
//            rootObj.SetActive(false);
//            barObj.SetActive(false);
//            StopAllCoroutines();
//        });

//        StartCoroutine(ProcessingRoutine(asyncHandle,onReady));
//    }

//    private IEnumerator ProcessingRoutine(AsyncOperationHandle asyncHandle, System.Action onReady)
//    {

//        while (asyncHandle.IsValid() && !asyncHandle.IsDone)
//        {
//            barImage.fillAmount = asyncHandle.PercentComplete;
//            yield return null;
//        }
//        barImage.fillAmount = 1;
//        yield return new WaitForSeconds(.25f);
//        if (asyncHandle.Status == AsyncOperationStatus.Succeeded)
//        {
//            onReady?.Invoke();
//            rootObj.SetActive(false);
//        }
//        else
//        {
//            LoadFailed();
//        }
//    }
//    //System.Action<GameObject> callback;
//    //private void OnAsyncCompleted(AsyncOperationHandle asyncHandle)
//    //{
//    //    bodyText.text = asyncHandle.Status.ToString();
//    //    if (asyncHandle.Status == AsyncOperationStatus.Succeeded)
//    //    {
//    //        callback?.Invoke((GameObject)asyncHandle.Result);
//    //        rootObj.SetActive(false);
//    //    }
//    //    else
//    //    {
//    //        LoadFailed();
//    //    }
//    //    //try
//    //    //{
//    //    //    asyncHandle.Completed -= OnAsyncCompleted;
//    //    //}
//    //    //catch(System.Exception e)
//    //    //{
//    //    //    Debug.LogError(e.Message);
//    //    //}
//    //    //StopAllCoroutines();
//    //    //callback = null;
//    //}



//}
