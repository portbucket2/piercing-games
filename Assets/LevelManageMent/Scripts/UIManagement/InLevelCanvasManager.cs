﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InLevelCanvasManager : MonoBehaviour
{
    //public static InLevelCanvasManager instance;
    public SuccessUIController successUIController;
    public GameObject failureObj;
    public GameObject minimizeObj;


    public bool AllowSkip = true;

    [Header("Fail")]
    public Button restartButton;
    public Button levelsButton;

    [Header("Ext")]
    public Button restartButtonExt;
    public Button levelsButtonExt;
    [Header("Minimize")]
    public Button miniModMinimizeButton;
    public Button miniModNextButton;
    public GameObject miniModInitialPart;
    public GameObject miniModLaterPart;

    [Header("skip")]
    public Button skipButtonExt;
    public Button skipAcceptButton;
    public Button skipCancelButton;
    public Button skipCancelButton_outer;
    public GameObject skipMenu;
    public GameObject skipObjectExt { get { return skipButtonExt.gameObject; } }

    [Header("rating")]
    public string ratingURL = "";// "market://details?id=" + Application.identifier + "&reviewId=0";
    public Button giveRating;
    public Button skipRating;
    public GameObject ratingwindow;

    [Header ( "push" )]
    public Button permitpush;
    public Button denypush;
    public GameObject pushwindow;

    [Header ( "general" )]
    public Text levelNameText;



    public static bool skipWithAdModeEnabled
    {   get
        {
            return false; //(ABManager.GetValue(ABtype.SKIP_OPTION)==1);
        }
    }
    public  void LevelStart(LevelInstanceManager levelObj)
    {
        successUIController.ForceDisable();
        failureObj.SetActive(false);
        ratingwindow.SetActive(false);
        miniModInitialPart.SetActive(false);
        miniModLaterPart.SetActive(false);
        //restartButtonExt.gameObject.SetActive(true);
        //levelsButtonExt.gameObject.SetActive(true);

        switch (levelObj.loadType)
        {
            default:
            case LoadType.NORMAL:
                levelNameText.text = string.Format("Level {0}", levelObj.cumulitiveLevelNo);
                break;
        }


        miniModNextButton.onClick.RemoveAllListeners();
        miniModNextButton.onClick.AddListener(()=> {
        
            OnSkip(levelObj);
        });

        miniModMinimizeButton.onClick.RemoveAllListeners();
        miniModMinimizeButton.onClick.AddListener(OnMini);


        //if (levelObject.levelI < LevelLoader.instance.levelAreas[levelObject.areaI].levelPrefs.Count - 1)
        //{
        //    nextButton.gameObject.SetActive(true);

        //}
        //else
        //{
        //    nextButton.gameObject.SetActive(false);
        //}


        restartButton.onClick.RemoveAllListeners();
        restartButton.onClick.AddListener(PrimaryLoader.OnReset);


        levelsButton.onClick.RemoveAllListeners();
        levelsButton.onClick.AddListener(PrimaryLoader.OnLevels);


        restartButtonExt.onClick.RemoveAllListeners();
        restartButtonExt.onClick.AddListener(PrimaryLoader.OnReset);

        levelsButtonExt.onClick.RemoveAllListeners();
        levelsButtonExt.onClick.AddListener(PrimaryLoader.OnLevels);
        //successUIController.SetStars(0);


        skipButtonExt.onClick.RemoveAllListeners();
        skipButtonExt.onClick.AddListener(() => skipMenu.SetActive(true));

        skipAcceptButton.onClick.RemoveAllListeners();
        skipAcceptButton.onClick.AddListener(() => OnSkip(levelObj));
        skipCancelButton.onClick.RemoveAllListeners();
        skipCancelButton.onClick.AddListener(()=>skipMenu.SetActive(false));
        skipCancelButton_outer.onClick.RemoveAllListeners();
        skipCancelButton_outer.onClick.AddListener(() => skipMenu.SetActive(false));
        skipObjectExt.SetActive(false);
        skipMenu.SetActive(false);
        //levelsButtonExt.gameObject.SetActive(LevelLoader.instance.testMode);
    }
    void OnSkip(LevelInstanceManager levelObj)
    {

        PrimaryLoader.OnNext();
        skipMenu.SetActive(false);

    }

    //public void Awake()
    //{
    //    instance = this;
    //}







    public event System.Action onMiniAction;
    private void OnMini()
    {
        onMiniAction?.Invoke();

        successUIController.ForceDisable();
        miniModInitialPart.SetActive(false);
        LoadContinue(true);
    }

    public void LoadContinue(bool enable)// bool animate, string text)
    {
        if (!enable)
        {

            miniModLaterPart.SetActive(false);
            skipObjectExt.SetActive(false);
        }
        else if (!skipWithAdModeEnabled)
        {
            miniModLaterPart.SetActive(true);
        }
        else
        {
            skipObjectExt.SetActive(true);
        }
    }
    public void LoadFail()
    {
        failureObj.SetActive(true);
        //restartButtonExt.gameObject.SetActive(false);
    }

    public void LoadSucces()
    {

//#if ( UNITY_ANDROID )

//        if ( LevelLoader.instance.GetLastLevelCumulativeNumber () == 3 && !MainGameManager.instance.ratingShown.value )
//        {
//            LoadRatingWindow ();
//            MainGameManager.instance.ratingShown.value = true;
//        }
//        else
//        {
//            ShowSuccessNow();
//        }
//#else
        ShowSuccessNow();
//#endif
        //restartButtonExt.gameObject.SetActive(false);
    }
    void ShowSuccessNow()
    {
        successUIController.OnShow();
        miniModInitialPart.SetActive(true);
        miniModLaterPart.SetActive(false);
    }


    public void LoadRatingWindow ()
    {
        ratingwindow.SetActive ( true );

        skipRating.onClick.AddListener ( () =>
        {
            ratingwindow.SetActive ( false );
            //ShowSuccessNow();
        } );
        giveRating.onClick.AddListener(()=>
        {
            Application.OpenURL ( "market://details?id=" + Application.identifier + "&reviewId=0" );

            //Application.OpenURL ( "itms-apps://itunes.apple.com/app/idYOUR_ID" );
            ratingwindow.SetActive ( false );
            //ShowSuccessNow();
        } );
        
    }

 
}
