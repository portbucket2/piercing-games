﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class LevelUIItemLoader : MonoBehaviour
{
    public GameObject mainObject;
    public Button mainButton;
    public GameObject lockImage;
    public GameObject nowPlayingImage;
    public GameObject playImage;
    public GameObject starModule;
    public List<GameObject> stars;
    public Text levelName;


    public Image bgImage;
    public Color mainColor;
    public Color nowColor;

    LevelDefinition levelDef;
    public void LoadNormal(int areaIndex, int levelIndex)
    {
        LevelArea levelArea = LevelDataKeeper.instance.levelAreas[areaIndex];
        levelDef = levelArea.levelDefinition[levelIndex];

        mainObject.SetActive(true);

        bool isUnlocked = LevelDataKeeper.instance.allLevelsUnlocked || LevelDataKeeper.instance.levelAreas[areaIndex].lastUnlockedLevelIndex.value >= levelIndex;
        bool isNowPlaying = (PrimaryLoader.currentLevelInstance!=null && PrimaryLoader.currentLevelInstance.levelDefinition == levelDef && PrimaryLoader.currentLevelInstance.loadType == LoadType.NORMAL);

        mainButton.interactable = isUnlocked;
        int starEarned = levelArea.stars[levelIndex].value;
        lockImage.SetActive(!isUnlocked && !isNowPlaying);
        starModule.SetActive(isUnlocked && !isNowPlaying);
        playImage.SetActive(isUnlocked && starEarned <= 0 && !isNowPlaying);
        nowPlayingImage.SetActive(isNowPlaying);
        bgImage.color = isNowPlaying ? nowColor : mainColor;
        if (isUnlocked)
        {
            for (int i = 0; i < stars.Count; i++)
            {
                stars[i].SetActive(starEarned > i);
            }
        }
        if (LevelDataKeeper.instance.allLevelsUnlocked)
        {
            levelName.text = string.Format("{0}. {1}", levelIndex + 1, levelDef.title);
        }
        else if (starEarned >= 1)
        {
            levelName.text =  levelDef.title;
        }
        else
        {
            levelName.text = string.Format("{0}", levelIndex + 1);
        }

        mainButton.onClick.RemoveAllListeners();
        mainButton.onClick.AddListener(()=> {
            if (!isNowPlaying)
            {
                PrimaryLoader.StartLevel(LoadType.NORMAL, areaIndex, levelIndex, levelDef);
            }
            PrimaryLoader.derivedInstance.ChangeLevelObjectState(GameState.GAME);
        });
    }
    public void LoadAsDummy()
    {
        mainObject.SetActive(false);
    }

   
}