﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;
public class SuccessUIController : MonoBehaviour
{
    #region editor fields
    [Header("Transition Buttons")]
    public Button levels2Button;
    public Button replayButton;
    public Button nextButton;
    public Button stepButton;
    public Button skipBoxButton;
    public Button openBoxButton;
    public Button claimBoxButton;
    //public Text openButtonText;
    public Text giftBoxTitleText;
    public Button getRewardButton;


    [Header("Completion Data")]
    public List<Animator> stars;
    public Text levelNumericNameText;
    public Text levelNameTextSuccess;
    public Text levelNameTextSuccess_2step;
    public Text triviaText;
    public Text peelLengthText;
    public Text peelLengthText2;
    public Animator peelTextAnimator;


    [Header("Flying Stars")]
    public GameObject prfeabStar;
    public RectTransform targetPoint;
    public Text flyingStarTargetText;


    [Header("State Toggles")]
    public Animator nextButtonAnim;
    public Animator nextButtonAnim_2step;
    public GameObject[] step1_Objects;
    public GameObject[] step2_Objects;
    public GameObject newToolDisplay;


    [Header("Tool Progress")]
    public Image initialProgressBar;
    public Image achievedProgressBar;
    public Image rewardedProgressBar;
    #endregion

    LevelInstanceManager levelObject;

    public Image toolToUnlockImage;

    //public void Awake()
    //{
    //    rewardADButtonOriginalParent = getRewardButton.transform.parent;
    //}
    public void ForceDisable()
    {
        this.gameObject.SetActive(false);
    }

    int savedStarCount;
    int freshStarCount;
    int textStarCount;
    public void SetStars(int starCount, int freshStars)
    {
        Debug.LogFormat("======{0}/{1}, {2}", freshStars, starCount, StarGazer.StarsAvailable);
        textStarCount = StarGazer.StarsAvailable;
        flyingStarTargetText.text = textStarCount.ToString();
        savedStarCount = starCount;
        freshStarCount = freshStars;
        StarGazer.AddStarsForLevelCompletion(freshStars);
    }
    public void DisplayStars()
    {
        for (int i = 0; i < stars.Count; i++)
        {

            stars[i].SetTrigger("reset");

        }

        FRIA.Centralizer.Add_DelayedMonoAct(this, () =>
        {
            for (int i = 0; i < stars.Count; i++)
            {

                stars[i].SetTrigger("reset");
                int index = i;
                if (savedStarCount > index) Centralizer.Add_DelayedMonoAct(this, () =>
                {
                    if (this!=null && this.gameObject !=null && this.gameObject.activeInHierarchy)
                    {

                        stars[index].SetTrigger("go");
                        if(index>=savedStarCount-freshStarCount)StartCoroutine(Flier(stars[index].transform as RectTransform, 0.5f));
                    }
                }, 0.5f * index);

            }
        }, 0);

        
    }

    IEnumerator Flier(RectTransform startPoint, float tSpan)
    {
        yield return new WaitForSeconds(0.5f);
        GameObject go = Pool.Instantiate(prfeabStar);
        go.transform.parent = this.transform;
        go.transform.position = startPoint.position;

        PooledItem pitem = go.GetComponent<PooledItem>();
        flierList.Add(pitem);
        //Debug.LogError("A");
        RectTransform trect = go.transform as RectTransform;

        float startTime = Time.time;

        Vector3 initialPos = trect.position;

        while (Time.time < startTime + tSpan)
        {
            float prog = Mathf.Clamp01((Time.time - startTime) / tSpan);

            trect.position = Vector3.Lerp(initialPos, targetPoint.position, prog);

            yield return null;
        }
        textStarCount++;
        flyingStarTargetText.text = textStarCount.ToString();
        flierList.Remove(pitem);
        //Debug.LogError("B");
        Pool.Destroy(go);
    }
    List<PooledItem> flierList = new List<PooledItem>();
    void ResetFlierProcesses()
    {
        StopAllCoroutines();
        for (int i = flierList.Count-1; i >= 0; i--)
        {

            //Debug.LogError(flierList[i].alive);
            if (flierList[i].alive)
            {
                Pool.Destroy(flierList[i].gameObject);
            }
            flierList.RemoveAt(i);
        }
    }

    static ChancedList<string> textlist;
    static string GetRandomMotivationText()
    {
        if (textlist == null)
        {
            textlist = new ChancedList<string>();
            textlist.Add("You are awesome!",100);
            textlist.Add("A true artist!",100);
            textlist.Add("Wow... I mean Wow!",20);
            textlist.Add("Perfect art doest ex...",20);
            textlist.Add("You are a natural!",100);
            textlist.Add("Now THAT is a beauty!",50);
            textlist.Add("Sure you are not a professional?",10);
            textlist.Add("You make it look easy!",150);
            textlist.Add("Hats off!!!",50);
            textlist.Add("Piercing Perfect!",30);
        }
        return textlist.Roll();
    }

    public void OnShow()
    {
        ResetFlierProcesses();
        this.gameObject.SetActive(true);

        this.levelObject = PrimaryLoader.currentLevelInstance;


        //triviaText.text = string.Format("<color=#253B71><size=60>Did you know?</size></color>\n\n{0}", levelObject.levelDefinition.detail);
        levelNameTextSuccess.text = levelObject.levelDefinition.title;
        levelNameTextSuccess_2step.text = levelObject.levelDefinition.title;
        peelLengthText.text = GetRandomMotivationText();
        //levelNumericNameText.text = "LEVEL " + levelObject.cumulitiveLevelNo.ToString();

        switch (levelObject.loadType)
        {
            default:
            case LoadType.NORMAL:
                levelNumericNameText.text = string.Format("LEVEL {0}", levelObject.cumulitiveLevelNo);
                break;
        }


        nextButton.onClick.RemoveAllListeners();
        nextButton.onClick.AddListener(OnNext);
        replayButton.onClick.RemoveAllListeners();
        replayButton.onClick.AddListener(PrimaryLoader.OnReset);
        levels2Button.onClick.RemoveAllListeners();
        levels2Button.onClick.AddListener(PrimaryLoader.OnLevels);
        stepButton.onClick.RemoveAllListeners();
        stepButton.onClick.AddListener(OnStep);



        SetStateList(step1_Objects, true);
        SetStateList(step2_Objects, false);
        OnStep();
    }
    private void OnNext()
    {
        //if (forcePromptToGetRewardComplete)
        {
            PrimaryLoader.OnNext();
        }
    }
    public void OnStep()
    {
        SetStateList(step2_Objects, true);
        SetStateList(step1_Objects, false);
        nextButtonAnim_2step.SetTrigger("go");
        DisplayStars();
    }
    //static bool forcePromptToGetRewardComplete
    //{
    //    get
    //    {
    //        if (_forcePromptForRewardButtonCompleteHD == null) _forcePromptForRewardButtonCompleteHD = new FRIA.HardData<bool>("FORCE_REWARD_COMPLETE", false);
    //        return _forcePromptForRewardButtonCompleteHD.value;
    //    }
    //    set
    //    {
    //        if (_forcePromptForRewardButtonCompleteHD == null) _forcePromptForRewardButtonCompleteHD = new FRIA.HardData<bool>("FORCE_REWARD_COMPLETE", false);
    //        _forcePromptForRewardButtonCompleteHD.value = value;
    //    }
    //}
    static FRIA.HardData<bool> _forcePromptForRewardButtonCompleteHD;

    int numsteps;
    string peeltext ( int i )
    {
        return ( (int) ( i / 10 ) ).ToString () + "." + ( (int) ( i % 10 ) ).ToString () + " inches";
    }


    private void SetStateList(GameObject[] obs, bool state)
    {
        foreach (GameObject item in obs)
        {
            item.SetActive(state);
        }
    }

}
